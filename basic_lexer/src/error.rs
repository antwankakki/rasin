use crate::tokens::Encoding;

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord)]
pub enum InternalLexerError {
    /// We started parsing a block comment but failed to find the end
    UnterminatedBlockComment,

    /// We parsed an unknown result
    UnknownChar(char),

    /// Encountered an invalid octal digit
    InvalidOctalDigit {
        /// The index of the invalid digit
        curr_idx: usize,

        // the offending char
        invalid_digit: char,
    },

    /// Encountered an invalid binary digit
    InvalidBinaryDigit {
        /// The index of the invalid digit
        invalid_char_idx: usize,

        /// The size of the potential digit (pretending its decimal)
        size: usize,
    },

    /// digit separator cant appear in the beginning of a digit sequence
    DigitSeparatorAtStart {
        /// The index of the invalid digit
        digit_sep_idx: usize,
    },

    /// two digit separators may not appear next to each other
    AdjacentDigitSeparators {
        /// The index of the invalid digit
        digit_sep_idx: usize,
    },

    /// Only a digit or a non-digit ([a-zA-Z]|_) may appear after a digit separator
    UnexpectedCharAfterDigitSeparator {
        /// The index of the invalid digit
        digit_sep_idx: usize,
    },

    /// a hexadecimal float literal requires the power to be mentioned (p|P)
    HexFloatNeedsPower {
        /// The index of the invalid digit
        end_idx: usize,
    },

    /// a float may only contain 1 decimal point
    TooManyDecimalPoints {
        /// the first decimal point idx,
        first_radix_pt_idx: usize,

        /// the second decimal point idx,
        second_radix_pt_idx: usize,
    },

    /// A power sign was mentioned but was not followed by any digits
    ExpectedExponent {
        /// where we expected an exponent
        expected_start_idx: usize,
    },

    /// An empty char literal was encountered
    EmptyCharConstant,

    /// An empty char literal was encountered
    NonTerminatedChar,

    /// A new line char was detected while parsing a char
    NewlineInChar,

    /// The char sequence specifies a hex digit starting sequence but isn't
    /// followed by any value, i.e  '\x'
    CharEscapeSeqExpectedHexDigits,

    /// Invalid hex escape sequence '\X' is invalid, only '\x'
    InvalidOctalEscapeSeqChar,

    /// Invalid hex escape sequence '\X' is invalid, only '\x'
    InvalidHexEscapeSeqChar,

    /// Invalid escape sequence
    InvalidEscapeSequence,

    /// Invalid Universal char name
    InvalidUniversalCharName {
        // Num chars expected
        size_expected: usize,

        // Num of chars found
        size_found: usize,
    },

    /// Invalid start token for an atom {
    InvalidAtomStartChar,

    /// Raw string delimiters must be at most 16 characters
    MaxRawStringDelimiterReached {
        /// The offset of the max delim for raw strings from the beginning of the string
        max_char_offset: usize,
    },

    /// Raw delimiter chars are the basic source set except for (, ), ', or ""
    InvalidRawDelimiterChar {
        /// The index of the max char
        invalid_char_idx: usize,
    },

    /// A non terminated raw string literal was encountered
    NonTerminatedRawString {
        /// Encoding of the raw string
        enc: Encoding,

        /// The start of the delimiter to look for
        delim_start: usize,

        /// The end of the delimiter to look for
        delim_end: usize,
    },

    // No more chars
    EOF,
}
