use crate::intern::InternedRep;
use std::num::NonZeroUsize;

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord)]
pub enum Encoding {
    UTF8,
    UTF16,
    UTF32,
    Wide,
    Narrow,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub enum IntegerLiteralBase {
    Binary,
    Octal,
    Decimal,
    Hexadecimal,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub enum FloatLiteralBase {
    Decimal,
    Hexadecimal,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub enum LiteralKind {
    // Character Related:
    /// Single byte character constant, 'a', '\13'.
    /// - **Type**:
    ///   * char if its representable as a single byte
    ///   * int  otherwise
    ///
    /// - **Value**:
    ///   * Representation of the char in the execution character set if it
    ///     fits in a single byte
    ///   * Implementation defined otherwise
    ///
    /// - **Notes**: Exhibits [implementation defined behavior](../../config/struct.ImplementationBehavior.html#structfield.single_byte_char_constant_value)
    NarrowChar,

    /// UTF8 Character literal, u8'a'.
    /// - **Type**:
    ///   * char8_t
    ///
    /// - **Value**:
    ///   * ISO 10646 code point value, must fit single UTF8 codepoint
    ///
    /// - **Notes**: Causes ill-formed programs if value is not in [0x0-0x7F]
    UTF8Char,

    /// UTF16 Character literal, u'貓'.
    /// - **Type**:
    ///   * char16_t
    ///
    /// - **Value**:
    ///   * ISO 10646 code point value, must fit single UTF16 codepoint
    ///
    /// - **Notes**: Causes ill-formed programs if value is not in [0x0-0xFFFF]
    UTF16Char,

    /// UTF32 Character literal - u'💩'
    /// - **Type**: char32_t
    /// - **Value**: ISO 10646 code point value of the char
    UTF32Char,

    /// Wide Character literal - u'β'
    /// - *Type*:  wchar_t
    /// - *Value*:
    ///   * Representation of the char in the execution character set if it
    ///     can fit the type (wchar_t)
    ///   * Implementation defined otherwise
    ///
    /// - *Notes*: Exhibits [implementation defined behavior](../../config/struct.ImplementationBehavior.html#structfield.wide_16bit_char_constant_value)
    WideChar,

    /// MultiByte Character literal - 'AB'
    /// - *Type*:  int
    /// - *Value*: Implementation defined behavior
    /// - *Notes*: Exhibits [implementation defined behavior](../../config/struct.ImplementationBehavior.html#structfield.multi_char_constant_value)
    MultiByteChar,

    /// Ordinary Raw String Literal - R"(Hello)"
    /// - *Type*:  array of N const char where n is the size of the string
    /// - *Value*: Initialized with the given characters in the string
    /// - *Notes*:
    ///   * Has static storage duration
    OrdinaryRawString,

    /// Wide Raw String Literal - LR"foo(Wat)foo"
    /// - *Type*:  array of N const char where n is the size of the string
    /// - *Value*: Initialized with the given characters in the string
    /// - *Notes*:
    ///   * Has static storage duration
    ///   * ToDo: Incomplete Docs
    WideRawString,

    /// UTF8 Raw String Literal - u8R"wat(foo)wat"
    /// - *Type*:  array of N const char8_t where n is the size of the string
    /// - *Value*: Initialized with the given characters in the string
    /// - *Notes*:
    ///   * Has static storage duration
    ///   * ToDo: Incomplete Docs
    UTF8RawString,

    /// UTF16 String Literal
    /// - *Type*:  array of N const char16_t where n is the size of the string
    /// - *Value*: Initialized with the given characters in the string
    /// - *Notes*:
    ///   * Has static storage duration
    ///   * ToDo: Incomplete Docs
    UTF16RawString,

    /// UTF32 String Literal
    /// - *Type*:  array of N const char32_t where n is the size of the string
    /// - *Value*: Initialized with the given characters in the string
    /// - *Notes*:
    ///   * Has static storage duration
    ///   * ToDo: Incomplete Docs
    UTF32RawString,

    /// Ordinary String Literal - "Hello"
    /// - *Type*:  array of N const char where n is the size of the string
    /// - *Value*: Initialized with the given characters in the string
    /// - *Notes*:
    ///   * Has static storage duration
    OrdinaryString,

    /// Wide String Literal - L"Wat"
    /// - *Type*:  array of N const char where n is the size of the string
    /// - *Value*: Initialized with the given characters in the string
    /// - *Notes*:
    ///   * Has static storage duration
    ///   * ToDo: Incomplete Docs
    WideString,

    /// UTF8 String Literal - u8"foo"
    /// - *Type*:  array of N const char8_t where n is the size of the string
    /// - *Value*: Initialized with the given characters in the string
    /// - *Notes*:
    ///   * Has static storage duration
    ///   * ToDo: Incomplete Docs
    UTF8String,

    /// UTF16 String Literal
    /// - *Type*:  array of N const char16_t where n is the size of the string
    /// - *Value*: Initialized with the given characters in the string
    /// - *Notes*:
    ///   * Has static storage duration
    ///   * ToDo: Incomplete Docs
    UTF16String,

    /// UTF32 String Literal
    /// - *Type*:  array of N const char32_t where n is the size of the string
    /// - *Value*: Initialized with the given characters in the string
    /// - *Notes*:
    ///   * Has static storage duration
    ///   * ToDo: Incomplete Docs
    UTF32String,

    /// Boolean
    /// - *Type*:  **bool**
    /// - *Value*: **true** or **false**
    Boolean,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub enum TokenKind<Sym: InternedRep> {
    // Control Tokens:
    /// End of file token
    EndOfFile,
    /// End of PP Directive token (EOL inside a directive)
    EndOfPPDirective,
    /// A code completion marker token
    CodeCompletionMarker,
    // Comment Styles, note that these are also considered whitespace:
    /// A single line comment "// comment"
    LineComment,
    /// A multi line comment "/* comment */"  (note that these do not nest)
    BlockComment,
    // One Char Whitespace Tokens as understood by the standard:
    /// ' ' - most common whitespace
    Space,
    /// '\t' -
    Tab,
    /// '\v'
    VerticalTab,
    /// '\n'
    NewLine,
    /// '\f'
    FormFeed,
    /// This may be an identifier, a keyword, an alternate token (and (&&),
    /// and_eq (&=)), part of a header name (<header> or "header").
    Atom(Sym),
    /// Literal
    Literal {
        /// The kind of the literal we have
        kind: LiteralKind,
        /// The index where you can start reading the literal data (after the prefix)
        start_offset: usize,
        /// The suffix to the literal, if any
        suffix_start_idx: Option<NonZeroUsize>,
    },
    // Operators/Punctuators:
    /// "&"
    Amp,
    /// "&&"
    AmpAmp,
    /// "&="
    AmpEq,
    /// "->"
    Arrow,
    /// "->*"
    ArrowStar,
    /// "^"
    Caret,
    /// "^="
    CaretEq,
    /// "\r"
    CarriageReturn,
    /// ":"
    Colon,
    /// ":"
    ColonColon,
    /// ","
    Comma,
    /// "."
    Dot,
    /// ".*"
    DotStar,
    /// "..."
    Ellipsis,
    /// "="
    Equal,
    /// "=="
    EqualEqual,
    /// "!"
    Exclaim,
    /// "!="
    ExclaimEq,
    /// ">"
    Gt,
    /// ">="
    GtEq,
    /// ">>"
    GtGt,
    /// ">>="
    GtGtEq,
    /// "#"
    Hash,
    /// "##"
    HashHash,
    /// "<"
    Lt,
    /// "<="
    LtEq,
    /// "<<"
    LtLt,
    /// "<<="
    LtLtEq,
    /// "-"
    Minus,
    /// "-="
    MinusEq,
    /// "--"
    MinusMinus,
    /// "%"
    Percent,
    /// "%="
    PercentEq,
    /// "|"
    Pipe,
    /// "|="
    PipeEq,
    /// "||"
    PipePipe,
    /// "+"
    Plus,
    /// "+="
    PlusEq,
    /// "++"
    PlusPlus,
    // Preprocessing Number
    PPNumber,
    /// "?"
    Question,
    /// ";"
    Semi,
    /// "/"
    Slash,
    /// "/="
    SlashEq,
    /// "<=>"
    Spaceship,
    /// "*"
    Star,
    /// "*="
    StarEq,
    /// "~"
    Tilde,
    // Bracket Punctuators
    /// "("
    OpenParen,
    /// ")"
    CloseParen,
    /// "{"
    OpenBrace,
    /// "}"
    CloseBrace,
    /// "["
    OpenBracket,
    /// "]"
    CloseBracket,
    /// Escaped character, only can come up as part of a char literal
    EscapedChar(char),
    /// Unknown token
    Unknown(char),
}

// Parsing Atoms
// These can be any words, pp directives, identifiers, keywords, names...
pub fn is_atom_start(c: char) -> bool {
    matches!(c,
        // ASCII Chars
        '_' | 'a'..='z' | 'A'..='Z'
        // Unicode Chars
        | '\u{00A8}'
        | '\u{00AA}'
        | '\u{00AD}'
        | '\u{00AF}'
        | '\u{2054}'
        | '\u{00B2}'..='\u{00B5}'
        | '\u{00B7}'..='\u{00BA}'
        | '\u{00BC}'..='\u{00BE}'
        | '\u{00C0}'..='\u{00D6}'
        | '\u{00D8}'..='\u{00F6}'
        | '\u{00F8}'..='\u{00FF}'
        | '\u{0100}'..='\u{02FF}'
        | '\u{0370}'..='\u{167F}'
        | '\u{1681}'..='\u{180D}'
        | '\u{180F}'..='\u{1DBF}'
        | '\u{1E00}'..='\u{1FFF}'
        | '\u{200B}'..='\u{200D}'
        | '\u{202A}'..='\u{202E}'
        | '\u{203F}'..='\u{2040}'
        | '\u{2060}'..='\u{20CF}'
        | '\u{2100}'..='\u{218F}'
        | '\u{2460}'..='\u{24FF}'
        | '\u{2776}'..='\u{2793}'
        | '\u{2C00}'..='\u{2DFF}'
        | '\u{2E80}'..='\u{2FFF}'
        | '\u{3004}'..='\u{3007}'
        | '\u{3021}'..='\u{302F}'
        | '\u{3031}'..='\u{D7FF}'
        | '\u{F900}'..='\u{FD3D}'
        | '\u{FD40}'..='\u{FDCF}'
        | '\u{FDF0}'..='\u{FE1F}'
        | '\u{FE30}'..='\u{FE44}'
        | '\u{FE47}'..='\u{FFFD}'
        | '\u{10000}'..='\u{1FFFD}'
        | '\u{20000}'..='\u{2FFFD}'
        | '\u{30000}'..='\u{3FFFD}'
        | '\u{40000}'..='\u{4FFFD}'
        | '\u{50000}'..='\u{5FFFD}'
        | '\u{60000}'..='\u{6FFFD}'
        | '\u{70000}'..='\u{7FFFD}'
        | '\u{80000}'..='\u{8FFFD}'
        | '\u{90000}'..='\u{9FFFD}'
        | '\u{A0000}'..='\u{AFFFD}'
        | '\u{B0000}'..='\u{BFFFD}'
        | '\u{C0000}'..='\u{CFFFD}'
        | '\u{D0000}'..='\u{DFFFD}'
        | '\u{E0000}'..='\u{EFFFD}'
    )
}

/// These are the characters allowed as long as they don't start the atom
/// Mainly the same set as above, except now it also accepts numbers and
/// combining characters (characters that are added to other characters in
/// unicode)
pub fn is_atom_continue(c: char) -> bool {
    matches!(c,
        // ASCII valid chars
        '_' | 'a'..='z' | 'A'..='Z' | '0'..='9'
        // Unicode chars
        | '\u{00A8}'
        | '\u{00AA}'
        | '\u{00AD}'
        | '\u{00AF}'
        | '\u{2054}'
        | '\u{00B2}'..='\u{00B5}'
        | '\u{00B7}'..='\u{00BA}'
        | '\u{00BC}'..='\u{00BE}'
        | '\u{00C0}'..='\u{00D6}'
        | '\u{00D8}'..='\u{00F6}'
        | '\u{00F8}'..='\u{00FF}'
        | '\u{0100}'..='\u{167F}'
        | '\u{1681}'..='\u{180D}'
        | '\u{180F}'..='\u{1FFF}'
        | '\u{200B}'..='\u{200D}'
        | '\u{202A}'..='\u{202E}'
        | '\u{203F}'..='\u{2040}'
        | '\u{2060}'..='\u{218F}'
        | '\u{2460}'..='\u{24FF}'
        | '\u{2776}'..='\u{2793}'
        | '\u{2C00}'..='\u{2DFF}'
        | '\u{2E80}'..='\u{2FFF}'
        | '\u{3004}'..='\u{3007}'
        | '\u{3021}'..='\u{302F}'
        | '\u{3031}'..='\u{D7FF}'
        | '\u{F900}'..='\u{FD3D}'
        | '\u{FD40}'..='\u{FDCF}'
        | '\u{FDF0}'..='\u{FE44}'
        | '\u{FE47}'..='\u{FFFD}'
        | '\u{10000}'..='\u{1FFFD}'
        | '\u{20000}'..='\u{2FFFD}'
        | '\u{30000}'..='\u{3FFFD}'
        | '\u{40000}'..='\u{4FFFD}'
        | '\u{50000}'..='\u{5FFFD}'
        | '\u{60000}'..='\u{6FFFD}'
        | '\u{70000}'..='\u{7FFFD}'
        | '\u{80000}'..='\u{8FFFD}'
        | '\u{90000}'..='\u{9FFFD}'
        | '\u{A0000}'..='\u{AFFFD}'
        | '\u{B0000}'..='\u{BFFFD}'
        | '\u{C0000}'..='\u{CFFFD}'
        | '\u{D0000}'..='\u{DFFFD}'
        | '\u{E0000}'..='\u{EFFFD}'
    )
}
pub fn get_whitespace_token<Sym: InternedRep>(c: char) -> Option<TokenKind<Sym>> {
    match c {
        ' ' => Some(TokenKind::Space),
        '\t' => Some(TokenKind::Tab),
        '\u{000B}' => Some(TokenKind::VerticalTab),
        '\u{000C}' => Some(TokenKind::CarriageReturn),
        '\u{000D}' => Some(TokenKind::VerticalTab),
        _ => None,
    }
}

pub fn is_digit(c: char) -> bool {
    c.is_ascii_digit()
}

pub fn is_hex_digit(c: char) -> bool {
    c.is_ascii_hexdigit()
}

pub fn is_octal_digit(c: char) -> bool {
    matches!(c, '0'..='7')
}

pub fn is_ordinary_escape_seq_char(c: char) -> bool {
    matches!(c, '\'' | '"' | '?' | '\\' | 'a' | 'b' | 'f' | 'n' | 'r' | 't' | 'v')
}

pub fn is_user_defined_delim_char(c: char) -> bool {
    matches!(c, '0'..='9' | 'a'..='z' | 'A'..='Z' | '.' | '_' | '{' | '}' |
                '[' | ']' | '#' | '<' | '>' | '%' | ':' | ';' | '?' | '*' |
                '+' | '-' | '/' | '^' | '&' | '|' | '~' | '!' | '=' | ',')
}

// per the definition of non digits from ISO C++ draft n4898 5.10 Identifiers
pub fn is_non_digit(c: char) -> bool {
    matches!(c, 'a'..='z' | 'A'..='Z' | '_')
}
