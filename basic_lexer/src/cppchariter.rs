//! An iterator that parses characters in a way that respects line slices.
//!
//! C++ does line slicing so we can't simply iterate over the characters in a string, this module
//! exposes an iterator over a slice that knows how to work with line slices (\n).
use std::str::CharIndices;

/// An iterator over the [`char`]s of a string slice, and their positions that
/// skips backslash-newline by default (unless using the next_raw)
#[derive(Clone, Debug)]
pub struct CppCharIt<'input> {
    /// The underlying iterator over the buffer yielding chars
    iter: CharIndices<'input>,

    /// The buffer that the char iterator is parsing. This will always refer to the beginning of the
    /// buffer, unlike iter.as_str(), which refers to where the char iterator is looking at
    /// currently
    buffer: &'input str,
}
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
/// The basic unit of lexing for the compiler. It represents a single UTF8 char along with the
/// line slices preceding it
pub struct CppChar {
    c: char,
    logical_index: usize,
    physical_index: usize,
    line_slice_count: u8,
}

impl CppChar {
    /// Given two CppChar, provide a fold function based on the idx and return a combined CppChar
    /// element
    fn fold_idx<F>(a: Option<CppChar>, b: Option<CppChar>, cmp: F) -> Option<CppChar>
    where
        F: Fn(CppChar, CppChar) -> bool,
    {
        match (a, b) {
            (Some(x), Some(y)) if cmp(x, y) => Some(CppChar {
                c: y.character(),
                logical_index: y.logical_index,
                physical_index: y.physical_index,
                line_slice_count: x.line_slice_count + y.line_slice_count,
            }),

            (Some(x), Some(y)) => Some(CppChar {
                c: x.character(),
                logical_index: y.logical_index,
                physical_index: y.physical_index,
                line_slice_count: x.line_slice_count + y.line_slice_count,
            }),

            (Some(_), None) => a,
            (None, Some(_)) => b,

            _ => None,
        }
    }

    /// Combine two CppChar elements and keep the info of the one with a smaller physical index
    pub fn fold_min_idx(a: Option<CppChar>, b: Option<CppChar>) -> Option<CppChar> {
        Self::fold_idx(a, b, |x, y| x.physical_index() >= y.physical_index())
    }

    /// Combine two CppChar elements and keep the info of the one with a greater physical index
    pub fn fold_max_idx(a: Option<CppChar>, b: Option<CppChar>) -> Option<CppChar> {
        Self::fold_idx(a, b, |x, y| y.physical_index() >= x.physical_index())
    }

    /// returns the actual character contained in this CppChar
    pub fn character(&self) -> char {
        self.c
    }

    /// The index of the character ignoring leading slices ('\\', '\n', 'a') will have a physical
    /// index of 2
    pub fn physical_index(&self) -> usize {
        self.physical_index
    }

    /// The index of the character accounting for leading slices ('\\', '\n', 'a') will have a
    /// logical index of 0
    pub fn logical_index(&self) -> usize {
        self.logical_index
    }

    /// returns whether this CppChar has any line slices in it
    pub fn sliced(&self) -> bool {
        self.line_slice_count != 0
    }

    /// returns how many line slices are part of this CppChar
    pub fn slice_count(&self) -> u8 {
        self.line_slice_count
    }
}

impl<'a> Iterator for CppCharIt<'a> {
    type Item = CppChar;

    #[inline]
    fn next(&mut self) -> Option<CppChar> {
        match self.iter.next() {
            // this is the likely case, the next char isn't a line splice so go on
            Some((idx, c)) if c != '\\' => {
                Some(CppChar { c, logical_index: idx, physical_index: idx, line_slice_count: 0 })
            }
            // the next line is potentially a line slice, check if its followed by a new line
            Some((idx, _)) => {
                let mut count = 0;
                loop {
                    let curr_count = match self.as_str().as_bytes() {
                        // first '\\' has been consumed
                        [b'\n', ..] if count == 0 => 1,
                        [b'\r', b'\n', ..] if count == 0 => 2,

                        [b'\\', b'\n', ..] => 2,
                        [b'\\', b'\r', b'\n', ..] => 2,
                        _ => 0,
                    };

                    match curr_count {
                        0 => break,
                        n => {
                            self.iter.nth(n - 1);
                        }
                    };

                    count += curr_count;
                }

                let (line_slice_count, new_idx, c) = match count {
                    0 => (0_u8, idx, '\\'),
                    n => self.iter.next().map(|v| (n as u8, v.0, v.1)).unwrap_or((
                        n as u8,
                        idx + n,
                        '\n',
                    )),
                };

                Some(CppChar { c, logical_index: idx, physical_index: new_idx, line_slice_count })
            }
            None => None,
        }
    }

    #[inline]
    fn size_hint(&self) -> (usize, Option<usize>) {
        self.iter.size_hint()
    }

    #[inline]
    fn count(self) -> usize {
        self.iter.count()
    }
}

impl<'input> CppCharIt<'input> {
    #[inline]
    /// Creates a cpp char iterator over a string slice
    pub fn new(data: &'input str) -> CppCharIt<'input> {
        CppCharIt { iter: data.char_indices(), buffer: data }
    }

    #[inline]
    /// Returns whatever string is left of the iterator, that is, the where the iterator will be
    /// looking at currently
    pub fn as_str(&mut self) -> &str {
        self.iter.as_str()
    }

    #[inline]
    /// The buffer that the char iterator is parsing. This will always refer to the beginning of the
    /// buffer
    pub fn data(&self) -> &str {
        self.buffer
    }
}
/// Get the next char we have
pub fn advance(iter: &mut CppCharIt) -> Option<CppChar> {
    iter.next()
}

/// Get the nth char relative to this char
pub fn peek_nth(iter: &CppCharIt, n: usize) -> Option<CppChar> {
    iter.clone().nth(n)
}

/// Get the next char we have
pub fn peek_next(iter: &CppCharIt) -> Option<CppChar> {
    peek_nth(iter, 0)
}

/// Skip the next n chars, returns the last index read and whether any line slices
/// were encountered
pub fn skip_n(iter: &mut CppCharIt, n: usize) -> Option<(usize, bool)> {
    std::iter::repeat_with(|| advance(iter))
        .take(n)
        .fold(None, CppChar::fold_max_idx)
        .map(|v| (v.physical_index(), v.sliced()))
}

/// we reached the end of the input currently given
pub fn is_eof(iter: &CppCharIt) -> bool {
    iter.clone().as_str().is_empty()
}

/// Eats symbols while predicate returns true or until the end of file is
/// reached.
/// Returns a tuple of 3 integers,
///     (amount of eating symbols ignoring leading line slices for the last token,
///      total amount of eaten symbols,
///      whether the token has a line slice)
pub fn eat_while<F>(iter: &mut CppCharIt, predicate: F) -> (usize, usize, bool)
where
    F: Fn(char) -> bool,
{
    match peek_next(iter) {
        Some(tk) if predicate(tk.character()) => {
            // The starting index ignoring leading slices. ('\\', '\n', 'a') will have a physical
            // start index of 2
            let physical_start_idx = tk.physical_index();

            // The starting index accounting for leading slices. ('\\', '\n', 'a') index of 0
            let logical_start_idx = tk.logical_index();

            std::iter::repeat_with(|| (advance(iter), peek_next(iter)))
                .take_while(|c| c.1.map(|v| predicate(v.character())).unwrap_or(false))
                .filter_map(|c| c.1)
                .fold(
                    (
                        tk.physical_index() - physical_start_idx + char::len_utf8(tk.character()),
                        tk.physical_index() - logical_start_idx + char::len_utf8(tk.character()),
                        tk.sliced(),
                    ),
                    |a, b| {
                        (
                            b.physical_index() - physical_start_idx + char::len_utf8(b.character()),
                            b.physical_index() - logical_start_idx + char::len_utf8(b.character()),
                            a.2 || b.sliced(),
                        )
                    },
                )
        }
        Some(_) => (0, 0, false),
        None => (0, 0, false),
    }
}

/// Eats at most max symbols while predicate returns true or until
/// the end of file is reached.
///
/// Returns amount of eaten symbols as well as whether a slice was encountered.
pub fn eat_while_max<F>(iter: &mut CppCharIt, predicate: F, max: usize) -> (usize, bool)
where
    F: Fn(char) -> bool,
{
    let mut cloned_iter = iter.clone();
    let count = std::iter::repeat_with(|| advance(&mut cloned_iter))
        .take_while(|c| c.map(|v| predicate(v.character())).unwrap_or(false))
        .take(max)
        .count();

    let start_idx = peek_next(iter).map(|v| v.logical_index()).unwrap_or(0);

    skip_n(iter, count).map(|v| (v.0 - start_idx + 1, v.1)).unwrap_or((0, false))
}
