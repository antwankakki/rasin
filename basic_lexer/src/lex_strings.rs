use crate::intern::{InternPolicy, InternedRep};

use super::cppchariter::{
    advance, eat_while, eat_while_max, is_eof, peek_next, skip_n, CppChar, CppCharIt,
};
use super::tokens::{
    is_hex_digit, is_octal_digit, is_ordinary_escape_seq_char, is_user_defined_delim_char, Encoding,
};

use super::ILError;
use super::ILResult;

use super::CppToken;
use super::LiteralKind as BLK;
use super::{try_parse_suffix, TokenKind as BTK};
use std::str::Chars;

/// x single token representing the single quote token
const SINGLE_CHAR_TOKEN: char = '\'';

/// eats a string literal token
pub fn lex_string_literal<IP: InternPolicy>(
    iter: &mut CppCharIt,
    ctx: &mut IP::Ctx,
    start_idx: usize,
    enc: Encoding,
) -> ILResult<IP::Symbol> {
    let (prefix_size, kind) = match enc {
        Encoding::UTF8 => (3, BLK::UTF8String),       // u8"
        Encoding::UTF16 => (2, BLK::UTF16String),     // u"
        Encoding::UTF32 => (2, BLK::UTF32String),     // U"
        Encoding::Wide => (2, BLK::WideString),       // L"
        Encoding::Narrow => (1, BLK::OrdinaryString), //"
    };

    // skip less than the count because the first char was consumed already
    let (_, mut sliced) = skip_n(iter, prefix_size).ok_or(ILError::EOF)?;
    let start_offset = peek_next(iter).map(|v| v.logical_index()).ok_or(ILError::EOF)?;

    loop {
        match (advance(iter), peek_next(iter)) {
            // Matched the end?!
            (Some(a), _) if a.character() == '"' => {
                let maybe_suffix = try_parse_suffix::<IP>(iter, ctx);
                sliced = sliced || maybe_suffix.map(|v| v.1.sliced).unwrap_or(false);
                let size =
                    maybe_suffix.map(|v| v.0 + v.1.size - 1).unwrap_or_else(|| a.physical_index())
                        - start_idx
                        + 1;

                return Ok(CppToken {
                    kind: BTK::Literal {
                        kind,
                        start_offset,
                        suffix_start_idx: maybe_suffix
                            .map(|v| std::num::NonZeroUsize::new(v.0))
                            .unwrap_or(None),
                    },
                    size,
                    sliced,
                });
            }

            // validate we have an escape sequence
            (Some(a), Some(b)) if a.character() == '\\' => {
                match consume_esc_seq::<IP::Symbol>(iter, start_idx, b) {
                    Ok(tk) if tk.sliced => sliced = true,
                    Ok(_) => (),
                    Err(e) => return Err(e),
                }
            }

            // Check if we got a new line
            (Some(a), _) if matches!(a.character(), '\n' | '\r') => {
                return Err(ILError::NewlineInChar)
            }

            (Some(a), _) if a.sliced() => sliced = true,

            (Some(_), _) => (),

            // EOF hit, non terminated string?
            _ => return Err(ILError::EOF),
        }
    }
}

/// lex a C++ raw string literal
/// splices here trigger undefined behavior
pub fn lex_raw_string_literal<IP: InternPolicy>(
    iter: &mut CppCharIt,
    ctx: &mut IP::Ctx,
    enc: Encoding,
) -> ILResult<IP::Symbol> {
    // mark the start of the token
    let (kind, prefix_size) = match enc {
        Encoding::Narrow => (BLK::OrdinaryRawString, 2),
        Encoding::Wide => (BLK::WideRawString, 3),
        Encoding::UTF16 => (BLK::UTF8RawString, 3),
        Encoding::UTF32 => (BLK::UTF16RawString, 3),
        Encoding::UTF8 => (BLK::UTF32RawString, 4),
    };

    // skip less than the count because the first char was consumed already
    let (start_offset, offset_sliced) = skip_n(iter, prefix_size).ok_or(ILError::EOF)?;

    // keep another copy of the iterator fixed where the delim starts to match against for later
    let mut end_delim_iter_cpp = iter.clone();
    let end_delim_iter = end_delim_iter_cpp.as_str().chars();

    // lex any potential user defined delimiter
    let (delim_size, delim_sliced) = eat_while_max(iter, is_user_defined_delim_char, 17);

    // max user defined delim is 16, issue a diagnostic if otherwise
    if delim_size > 16 {
        return Err(ILError::MaxRawStringDelimiterReached {
            max_char_offset: start_offset + delim_size,
        });
    }

    // Fix up the expected end
    let end_delim_iter = end_delim_iter.take(delim_size).chain("\"".chars());

    // The next char must be the parenthesis
    match advance(iter) {
        Some(c) if c.character() != '(' => {
            Err(ILError::InvalidRawDelimiterChar { invalid_char_idx: c.physical_index() })
        }
        _ => Ok(()),
    }?;

    let iter_match = |curr_iter: std::iter::Skip<Chars>| {
        curr_iter.take(delim_size + 1).zip(end_delim_iter.clone()).any(|b| b.0 == b.1)
    };

    let mut literal_size = 0;
    loop {
        // loop until encountering potential end sequence, only care about the size (no slicing info)
        match eat_while(iter, |c| c != ')').1 {
            // if we failed to read anything and we are at eof return an eof error
            0 if is_eof(iter) => {
                return Err(ILError::NonTerminatedRawString {
                    enc,
                    delim_start: peek_next(&end_delim_iter_cpp)
                        .map(|v| v.physical_index())
                        .unwrap(),
                    delim_end: end_delim_iter_cpp
                        .nth(delim_size)
                        .map(|v| v.physical_index())
                        .unwrap(),
                });
            }
            // if we can read the delimiter now, we must have had an empty string literal
            v if iter_match(iter.clone().as_str().chars().skip(1)) => {
                literal_size += v;
                skip_n(iter, delim_size + 2);
                break;
            }
            // otherwise add the number of characters read to the size, note that
            // we need to special case reading "nothing" which may be a parenthesis
            // within the literal
            v => {
                advance(iter);
                literal_size += if v == 0 { 1 } else { v + 1 };
            }
        }
    }

    let (suffix_start_idx, suffix_size, suffix_sliced) = try_parse_suffix::<IP>(iter, ctx)
        .map(|(start, tk)| (std::num::NonZeroUsize::new(start), tk.size, tk.sliced))
        .unwrap_or((None, 0, false));

    const EXTRA_SIZE: usize = 3; // for the parenthesis + last quote char

    Ok(CppToken {
        kind: BTK::Literal {
            kind,
            start_offset: end_delim_iter_cpp.next().map(|v| v.physical_index()).unwrap(),
            suffix_start_idx,
        },
        size: literal_size + suffix_size + prefix_size + delim_size * 2 + EXTRA_SIZE,
        sliced: offset_sliced || delim_sliced || suffix_sliced,
    })
}

/// lex a C++ char literal
pub fn lex_char_literal<IP: InternPolicy>(
    iter: &mut CppCharIt,
    ctx: &mut IP::Ctx,
    start_idx: usize,
    enc: Encoding,
) -> ILResult<IP::Symbol> {
    let (res_encoding, prefix_size) = match enc {
        Encoding::UTF8 => (BLK::UTF8Char, 3usize),     // u8'
        Encoding::UTF16 => (BLK::UTF16Char, 2usize),   // u'
        Encoding::UTF32 => (BLK::UTF32Char, 2usize),   // U'
        Encoding::Wide => (BLK::WideChar, 2usize),     // L'
        Encoding::Narrow => (BLK::NarrowChar, 1usize), // '
    };

    // skip less than the count because the first char was consumed already
    let (_, offset_sliced) = skip_n(iter, prefix_size).ok_or(ILError::EOF)?;
    let start_offset =
        peek_next(iter).map(|v| v.logical_index()).ok_or(ILError::NonTerminatedChar)?;

    // loop until a non-escaped single quote because multi byte char literals
    // are a thing
    let mut tokens: Vec<CppToken<IP::Symbol>> = vec![];
    let mut last_idx = start_idx + start_offset;
    let mut tokens_sliced = false;
    loop {
        let res = match (advance(iter), peek_next(iter)) {
            // validate we have an escape sequence
            (Some(x), Some(y)) if x.character() == '\\' => {
                last_idx += x.slice_count() as usize + 1;
                consume_esc_seq(iter, last_idx, y)
            }

            (Some(x), _) if matches!(x.character(), '\n' | '\r') => Err(ILError::NewlineInChar),

            // Are we done parsing?
            (Some(x), _) => Ok(CppToken {
                kind: BTK::Unknown(x.character()),
                size: (x.physical_index() + 1) - last_idx,
                sliced: x.sliced(),
            }),

            // Looks like we are out of characters
            _ => Err(ILError::NonTerminatedChar),
        };
        tokens_sliced |= res.as_ref().map(|tk| tk.sliced).unwrap_or(false);
        last_idx += res.as_ref().map(|tk| tk.size).unwrap_or(0);

        match res {
            // if we matched an error, report it and break
            Err(e) => return Err(e),

            // If we matched the end token (not escaped), get out
            Ok(CppToken { kind: BTK::Unknown(SINGLE_CHAR_TOKEN), .. }) => break,

            // if we matched something else stay
            Ok(res) => tokens.push(res),
        };
    }

    // Check for any possible suffix
    let (suffix_start_idx, suffix_size, suffix_sliced) = match try_parse_suffix::<IP>(iter, ctx) {
        Some((size, tk)) => (std::num::NonZeroUsize::new(size), tk.size, tk.sliced),
        None => (None, 0, false),
    };

    let sliced = offset_sliced || suffix_sliced || tokens_sliced;
    let size = last_idx - start_idx + suffix_size;

    match tokens.len() {
        0 => Err(ILError::EmptyCharConstant),
        1 => Ok(CppToken {
            kind: BTK::Literal { kind: res_encoding, start_offset, suffix_start_idx },
            size,
            sliced,
        }),
        _ => Ok(CppToken {
            kind: BTK::Literal { kind: BLK::MultiByteChar, start_offset, suffix_start_idx },
            size,
            sliced,
        }),
    }
}

/// consume escape sequences or throw an error if invalid
fn consume_esc_seq<Sym: InternedRep>(
    iter: &mut CppCharIt,
    start_idx: usize,
    c: CppChar,
) -> ILResult<Sym> {
    match c.character() {
        // Octal
        '0'..='7' => {
            let (size, sliced) = eat_while_max(iter, is_octal_digit, 3);
            Ok(CppToken { kind: BTK::PPNumber, size, sliced })
        }

        // Invalid octal
        '8' | '9' => Err(ILError::InvalidOctalEscapeSeqChar),

        // Hex?, note that the there is no max length and the representation
        // is considered "unspecified behavior"
        'x' => match eat_while(iter, is_hex_digit) {
            (_, 0, _) => Err(ILError::CharEscapeSeqExpectedHexDigits),
            (_logical_size, phys_size, sliced) => {
                Ok(CppToken { kind: BTK::PPNumber, size: phys_size, sliced })
            }
        },

        // Invalid Hex
        'X' => Err(ILError::InvalidHexEscapeSeqChar),

        // UCN
        'u' => match eat_while_max(iter, is_hex_digit, 4) {
            (4, sliced) => Ok(CppToken { kind: BTK::PPNumber, size: 4, sliced }),
            (size_found, _) => {
                Err(ILError::InvalidUniversalCharName { size_found, size_expected: 4 })
            }
        },

        // UCN
        'U' => match eat_while_max(iter, is_hex_digit, 8) {
            (8, sliced) => Ok(CppToken { kind: BTK::PPNumber, size: 8, sliced }),
            (size_found, _) => {
                Err(ILError::InvalidUniversalCharName { size_found, size_expected: 8 })
            }
        },

        // parse valid escape sequences
        v if is_ordinary_escape_seq_char(v) => {
            advance(iter);
            Ok(CppToken {
                kind: BTK::EscapedChar(c.character()),
                size: c.physical_index() - start_idx,
                sliced: c.sliced(),
            })
        }

        // otherwise this was an invalid char for escape seq
        _ => Err(ILError::InvalidEscapeSequence),
    }
}
