use super::*;

use tokens::{LiteralKind, TokenKind};

use error::InternalLexerError as ILError;

/// Helper constructor
fn str_literal(enc: Encoding, suffix_start_idx: Option<NonZeroUsize>) -> TokenKind<()> {
    let (kind, start_offset) = match enc {
        Encoding::Narrow => (LiteralKind::OrdinaryString, 1),
        Encoding::Wide => (LiteralKind::WideString, 2),
        Encoding::UTF16 => (LiteralKind::UTF16String, 2),
        Encoding::UTF32 => (LiteralKind::UTF32String, 2),
        Encoding::UTF8 => (LiteralKind::UTF8String, 3),
    };

    TokenKind::<()>::Literal { kind, start_offset, suffix_start_idx }
}

/// String literals may be empty
#[test]
pub fn test_empty() {
    test_token!(r#""""#, str_literal(Encoding::Narrow, None), 2);
    test_token!(r#"u"""#, str_literal(Encoding::UTF16, None), 3);
    test_token!(r#"L"""#, str_literal(Encoding::Wide, None), 3);
    test_token!(r#"U"""#, str_literal(Encoding::UTF32, None), 3);
    test_token!(r#"U"" "#, str_literal(Encoding::UTF32, None), 3);
}

/// String literals may be empty
#[test]
pub fn test_slice_prefix() {
    test_token!(r#"u8"""#, str_literal(Encoding::UTF8, None), 4);
    test_tokens_with_slice!(
        r#"u\
8"""#,
        (
            TokenKind::<()>::Literal {
                kind: LiteralKind::UTF8String,
                start_offset: 5,
                suffix_start_idx: None
            },
            6,
            true
        )
    );
}

/// String literals may have a suffix
#[test]
pub fn test_with_suffix() {
    test_token!(r#"" "foo"#, str_literal(Encoding::Narrow, NonZeroUsize::new(3)), 6);
    test_token!(r#"u"**()**"_bar"#, str_literal(Encoding::UTF16, NonZeroUsize::new(9)), 13);
    test_token!(r#"L"*~()*~"zoo"#, str_literal(Encoding::Wide, NonZeroUsize::new(9)), 12);
    test_token!(r#"U"zzz()zzz"baz"#, str_literal(Encoding::UTF32, NonZeroUsize::new(11)), 14);
    test_token!(r#"U"zzz(asds)zzz"baz"#, str_literal(Encoding::UTF32, NonZeroUsize::new(15)), 18);
}

/// String literals may have a sliced suffix
#[test]
pub fn test_with_sliced_suffix() {
    test_tokens_with_slice!(
        r#"U"zzz(asds)zzz"\
baz"#,
        (str_literal(Encoding::UTF32, NonZeroUsize::new(15)), 20, true)
    );
}

/// String literals dont need user defined delimiters
#[test]
pub fn test_no_delim() {
    test_token!(r#""()""#, str_literal(Encoding::Narrow, None), 4);
    test_token!(r#"u"(a)""#, str_literal(Encoding::UTF16, None), 6);
    test_token!(r#"L"(\t)""#, str_literal(Encoding::Wide, None), 7);
    test_token!(r#"u8"(\r\t)""#, str_literal(Encoding::UTF8, None), 10);
}

/// Test with parenthesis because why not
#[test]
pub fn test_parenthesis_user_defined_delim() {
    test_token!(r#""a(())a""#, str_literal(Encoding::Narrow, None), 8);
    test_token!(r#""((foo))""#, str_literal(Encoding::Narrow, None), 9);
    test_token!(r#""(())""#, str_literal(Encoding::Narrow, None), 6);
}

/// Non ordinary string literals may be empty
#[test]
pub fn test_empty_utf_user_defined_delim() {
    test_token!(r#"u"a()a""#, str_literal(Encoding::UTF16, None), 7);
    test_token!(r#"U"a()a""#, str_literal(Encoding::UTF32, None), 7);
    test_token!(r#"L"a()a""#, str_literal(Encoding::Wide, None), 7);
    test_token!(r#"u8"a()a""#, str_literal(Encoding::UTF8, None), 8);
}

/// Simple string literal with a user defined delimiter
#[test]
pub fn test_simple_user_defined_delim() {
    test_token!(r#""abc(WhatWhat)abc""#, str_literal(Encoding::Narrow, None), 18);
}

/// Test that we count escape chars as well
#[test]
pub fn test_escape_seq_user_defined_delim() {
    test_token!(r#"u8"abc(\t\t\a)abc""#, str_literal(Encoding::UTF8, None), 18);
}

// Long suffix for strings
#[test]
pub fn test_long_suffix() {
    test_token!(
        r#""abcdefghijklmopqrstuv()"abcdefghijklmopqrstuv"#,
        str_literal(Encoding::Narrow, NonZeroUsize::new(25)),
        46
    );

    test_token!(
        r#"u"abcdefghijklmopqrstuv()"abcdefghijklmopqrstuv"#,
        str_literal(Encoding::UTF16, NonZeroUsize::new(26)),
        47
    );
    test_token!(
        r#"U"abcdefghijklmopqrstuv()"abcdefghijklmopqrstuv"#,
        str_literal(Encoding::UTF32, NonZeroUsize::new(26)),
        47
    );
    test_token!(
        r#"L"abcdefghijklmopqrstuv()"abcdefghijklmopqrstuv"#,
        str_literal(Encoding::Wide, NonZeroUsize::new(26)),
        47
    );
    test_token!(
        r#"u8"abcdefghijklmopqrstuv()"abcdefghijklmopqrstuv"#,
        str_literal(Encoding::UTF8, NonZeroUsize::new(27)),
        48
    );
}

// More String Tests
#[test]
pub fn test_more_strings() {
    test_token!(r#""abcd'a()abcd'a""#, str_literal(Encoding::Narrow, None), 16);
    test_token!(r#"u"abcd'a()abcd'a""#, str_literal(Encoding::UTF16, None), 17);
    test_token!(r#"U"abcd'a()abcd'a""#, str_literal(Encoding::UTF32, None), 17);
    test_token!(r#"L"abcd'a()abcd'a""#, str_literal(Encoding::Wide, None), 17);
    test_token!(r#"u8"abcd'a()abcd'a""#, str_literal(Encoding::UTF8, None), 18);
}

// Errors

/// String Literals may not contain a new line without escaping
#[test]
pub fn test_err_newlines() {
    test_err!(
        r#""abc(What

        What)abc""#,
        ILError::NewlineInChar
    );

    test_err!(
        r#""abc(What

        What)abc""#,
        ILError::NewlineInChar
    );

    test_err!(
        r#"u8"abc(aw
br)abc""#,
        ILError::NewlineInChar
    );

    test_err!(
        r#"U"zzz(as
        ds)zzz"baz"#,
        ILError::NewlineInChar
    );

    test_err!(
        r#"U"zzz(as \t
        \z
        \0
        ds)zzz"baz"#,
        ILError::NewlineInChar
    );
}

/// Bad escape strings
#[test]
pub fn test_err_bad_escape_sequences() {
    test_err!(r#""abc(\What"#, ILError::InvalidEscapeSequence);
}
