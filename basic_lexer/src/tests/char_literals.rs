use super::*;

use tokens::{LiteralKind, TokenKind};

use error::InternalLexerError as ILError;

fn ordinary_char_seq(suffix_start_idx: Option<NonZeroUsize>) -> TokenKind<()> {
    TokenKind::<()>::Literal { kind: LiteralKind::NarrowChar, start_offset: (1), suffix_start_idx }
}

fn wide_char_seq(suffix_start_idx: Option<NonZeroUsize>) -> TokenKind<()> {
    TokenKind::<()>::Literal { kind: LiteralKind::WideChar, start_offset: (2), suffix_start_idx }
}

fn utf8_char_seq(suffix_start_idx: Option<NonZeroUsize>) -> TokenKind<()> {
    TokenKind::<()>::Literal { kind: LiteralKind::UTF8Char, start_offset: (3), suffix_start_idx }
}

fn utf16_char_seq(suffix_start_idx: Option<NonZeroUsize>) -> TokenKind<()> {
    TokenKind::<()>::Literal { kind: LiteralKind::UTF16Char, start_offset: (2), suffix_start_idx }
}

fn utf32_char_seq(suffix_start_idx: Option<NonZeroUsize>) -> TokenKind<()> {
    TokenKind::<()>::Literal { kind: LiteralKind::UTF32Char, start_offset: (2), suffix_start_idx }
}

fn multibyte_char_seq(
    start_offset: usize,
    suffix_start_idx: Option<NonZeroUsize>,
) -> TokenKind<()> {
    TokenKind::<()>::Literal { kind: LiteralKind::MultiByteChar, start_offset, suffix_start_idx }
}

// Char literals with simple escape sequences
///
/// These are the most simple escape sequences we can do
#[test]
pub fn test_simple_escape_seq() {
    // Test single quote escape sequence
    test_token!(r#"'\''"#, ordinary_char_seq(None), 4);

    // Test double quote escape sequence
    test_token!(r#"'\"'"#, ordinary_char_seq(None), 4);

    // Test question mark escape sequence
    test_token!(r#"'\?'"#, ordinary_char_seq(None), 4);

    // Test backslash escape sequence
    test_token!(r#"'\\'"#, ordinary_char_seq(None), 4);

    // Test bell escape sequence
    test_token!(r#"'\a'"#, ordinary_char_seq(None), 4);

    // Test form feed escape sequence
    test_token!(r#"'\b'"#, ordinary_char_seq(None), 4);

    // Test form feed escape sequence
    test_token!(r#"'\f'"#, ordinary_char_seq(None), 4);

    // Test new line escape sequence
    test_token!(r#"'\n'"#, ordinary_char_seq(None), 4);

    // Test carriage return escape sequence
    test_token!(r#"'\r'"#, ordinary_char_seq(None), 4);

    // Test horizontal tab escape sequence
    test_token!(r#"'\t'"#, ordinary_char_seq(None), 4);

    // Test vertical tab escape sequence
    test_token!(r#"'\v'"#, ordinary_char_seq(None), 4);
}

// Char literals with octal escape sequences
///
/// The most basic building block of numbers in C++ is the digit sequence
///
/// Examples:
/// \1
/// \12
/// \129
#[test]
pub fn test_octal_escape_seq() {
    test_token!(r#"'\0'"#, ordinary_char_seq(None), 4);
    test_token!(r#"'\01'"#, ordinary_char_seq(None), 5);
    test_token!(r#"'\012'"#, ordinary_char_seq(None), 6);
    test_token!(r#"'\0124'"#, multibyte_char_seq(1, None), 7);
}

// UTF Char literals with octal escape sequences
///
/// A char literal defined with 3 octal chars is ok
#[test]
pub fn test_octal_escape_seq_with_prefix() {
    test_token!(r#"u8'\0'"#, utf8_char_seq(None), 6);
    test_token!(r#"u8'\01'"#, utf8_char_seq(None), 7);
    test_token!(r#"u8'\012'"#, utf8_char_seq(None), 8);

    test_token!(r#"u'\3'"#, utf16_char_seq(None), 5);
    test_token!(r#"u'\34'"#, utf16_char_seq(None), 6);
    test_token!(r#"u'\345'"#, utf16_char_seq(None), 7);

    test_token!(r#"U'\6'"#, utf32_char_seq(None), 5);
    test_token!(r#"U'\67'"#, utf32_char_seq(None), 6);
    test_token!(r#"U'\670'"#, utf32_char_seq(None), 7);

    test_token!(r#"L'\1'"#, wide_char_seq(None), 5);
    test_token!(r#"L'\12'"#, wide_char_seq(None), 6);
    test_token!(r#"L'\123'"#, wide_char_seq(None), 7);
}

// UTF Char literals with octal escape sequences
///
/// A char literal defined with 3 octal chars is ok
#[test]
pub fn test_octal_escape_seq_with_suffix() {
    test_token!(r#"'\0'a"#, ordinary_char_seq(NonZeroUsize::new(4)), 5);
    test_token!(r#"'\01'_a1"#, ordinary_char_seq(NonZeroUsize::new(5)), 8);
    test_token!(r#"'\012'_12"#, ordinary_char_seq(NonZeroUsize::new(6)), 9);

    test_token!(r#"u8'\0'_to_string"#, utf8_char_seq(NonZeroUsize::new(6)), 16);
    test_token!(r#"u8'\01'minutes"#, utf8_char_seq(NonZeroUsize::new(7)), 14);
    test_token!(r#"u8'\012'seconds"#, utf8_char_seq(NonZeroUsize::new(8)), 15);

    test_token!(r#"u'\3'come"#, utf16_char_seq(NonZeroUsize::new(5)), 9);
    test_token!(r#"u'\34'on"#, utf16_char_seq(NonZeroUsize::new(6)), 8);
    test_token!(r#"u'\345'done"#, utf16_char_seq(NonZeroUsize::new(7)), 11);

    test_token!(r#"U'\6'😀"#, utf32_char_seq(NonZeroUsize::new(5)), 9);
    test_token!(r#"U'\67'huh"#, utf32_char_seq(NonZeroUsize::new(6)), 9);
    test_token!(r#"U'\670'worked"#, utf32_char_seq(NonZeroUsize::new(7)), 13);

    test_token!(r#"L'\1'a"#, wide_char_seq(NonZeroUsize::new(5)), 6);
    test_token!(r#"L'\12'ee"#, wide_char_seq(NonZeroUsize::new(6)), 8);
    test_token!(r#"L'\123'ff"#, wide_char_seq(NonZeroUsize::new(7)), 9);
}

// Non ordinary Char literals with simple escape sequences
///
/// These are the most simple escape sequences we can do
#[test]
pub fn test_utf_escape_seq() {
    test_token!(r#"u8'\''"#, utf8_char_seq(None), 6);
    test_token!(r#"u'\"'"#, utf16_char_seq(None), 5);
    test_token!(r#"L'\?'"#, wide_char_seq(None), 5);
    test_token!(r#"U'\\'"#, utf32_char_seq(None), 5);
}

// Non ordinary Char literals with simple escape sequences with suffix
///
/// These are the most simple escape sequences we can do
#[test]
pub fn test_utf_escape_seq_with_suffix() {
    test_token!(r#"u8'\''asd"#, utf8_char_seq(NonZeroUsize::new(6)), 9);
    test_token!(r#"u'\"'_foo"#, utf16_char_seq(NonZeroUsize::new(5)), 9);
    test_token!(r#"L'\?'_a12"#, wide_char_seq(NonZeroUsize::new(5)), 9);
    test_token!(r#"U'\\'bar"#, utf32_char_seq(NonZeroUsize::new(5)), 8);
    test_token!(r#"U'\\'bar "#, utf32_char_seq(NonZeroUsize::new(5)), 8);
}

// Multibyte Char
///
/// These are the most simple escape sequences we can do
#[test]
pub fn test_char_multibyte() {
    test_token!(r#"'ab'"#, multibyte_char_seq(1, None), 4);
}

// Non ordinary multibyte char
///
/// These are the most simple escape sequences we can do
#[test]
pub fn test_char_utf_multibyte() {
    test_token!(r#"'ab'"#, multibyte_char_seq(1, None), 4);
    test_token!(r#"u'a\b'"#, multibyte_char_seq(2, None), 6);
    test_token!(r#"L'ab'"#, multibyte_char_seq(2, None), 5);
    test_token!(r#"u8'a\b'"#, multibyte_char_seq(3, None), 7);
}

// Multibyte Char with suffix
///
/// These are the most simple escape sequences we can do
#[test]
pub fn test_char_multibyte_suffix() {
    test_token!(r#"'ab'a"#, multibyte_char_seq(1, NonZeroUsize::new(4)), 5);
}

// Non ordinary multibyte char with suffix
///
/// These are the most simple escape sequences we can do
#[test]
pub fn test_char_utf_multibyte_suffix() {
    test_token!(r#"u8'ab'a"#, multibyte_char_seq(3, NonZeroUsize::new(6)), 7);
    test_token!(r#"u'a\b'a"#, multibyte_char_seq(2, NonZeroUsize::new(6)), 7);
    test_token!(r#"L'abasdsad'b"#, multibyte_char_seq(2, NonZeroUsize::new(11)), 12);
    test_token!(r#"u8'a\b'ca"#, multibyte_char_seq(3, NonZeroUsize::new(7)), 9);
}

/// Test leading slices
#[test]
pub fn test_char_leading_slice() {
    test_tokens_with_slice!(
        r#"u8\
'\''asd"#,
        (
            TokenKind::<()>::Literal {
                kind: LiteralKind::UTF8Char,
                start_offset: 5,
                suffix_start_idx: std::num::NonZeroUsize::new(8),
            },
            11,
            true
        )
    );
}

/// Test suffix slices
#[test]
pub fn test_char_suffix_slice() {
    test_tokens_with_slice!(
        r#"u8'\''a\
sd"#,
        (
            TokenKind::<()>::Literal {
                kind: LiteralKind::UTF8Char,
                start_offset: 3,
                suffix_start_idx: std::num::NonZeroUsize::new(6),
            },
            11,
            true
        )
    );
}

/// Test multibyte slices
#[test]
pub fn test_char_multibyte_slice() {
    test_tokens_with_slice!(
        r#"u'a\
b'"#,
        (
            TokenKind::<()>::Literal {
                kind: LiteralKind::MultiByteChar,
                start_offset: 2,
                suffix_start_idx: None
            },
            7,
            true
        )
    );
}

/// Test end or begin quote slices
#[test]
pub fn test_char_quote_slice() {
    test_tokens_with_slice!(
        r#"u\
'ab'"#,
        (
            TokenKind::<()>::Literal {
                kind: LiteralKind::MultiByteChar,
                start_offset: 4,
                suffix_start_idx: None
            },
            7,
            true
        )
    );

    test_tokens_with_slice!(
        r#"u'ab\
'"#,
        (
            TokenKind::<()>::Literal {
                kind: LiteralKind::MultiByteChar,
                start_offset: 2,
                suffix_start_idx: None
            },
            7,
            true
        )
    );

    test_tokens_with_slice!(
        r#"u\
'ab\
'"#,
        (
            TokenKind::<()>::Literal {
                kind: LiteralKind::MultiByteChar,
                start_offset: 4,
                suffix_start_idx: None
            },
            9,
            true
        )
    );
}

// Errors

// Empty char literal
///
/// Empty char literals are not ok
#[test]
pub fn test_err_empty_char_literal() {
    let err = || ILError::EmptyCharConstant;

    test_err!("''", err());
    test_err!("''", err());
    test_err!("''", err());

    test_err!("u8''", err());
    test_err!("u8''", err());
    test_err!("u8''", err());

    test_err!("u''", err());
    test_err!("u''", err());
    test_err!("u''", err());

    test_err!("U''", err());
    test_err!("U''", err());
    test_err!("U''", err());

    test_err!("L''", err());
    test_err!("L''", err());
    test_err!("L''", err());
}

// Empty char literal
///
/// Empty char literals are not ok
#[test]
pub fn test_err_empty_with_suffix() {
    let err = || ILError::EmptyCharConstant;

    test_err!("''asd", err());
    test_err!("u8''_q3", err());
    test_err!("L''_foo", err());
}

// Unterminated Char Literal
///
/// Every char literal must end with a single quote
#[test]
pub fn test_err_non_terminated_char_literal() {
    test_err!(r#"'"#, ILError::NonTerminatedChar);
    test_err!(r#"u8'"#, ILError::NonTerminatedChar);
    test_err!(r#"L' "#, ILError::NonTerminatedChar);
    test_err!(r#"U' "#, ILError::NonTerminatedChar);
    test_err!(r#"u'asd "#, ILError::NonTerminatedChar);
}

// Newline in Char Literal
///
/// The most basic building block of numbers in C++ is the digit sequence
#[test]
pub fn test_err_newline_in_char() {
    test_err!(
        r#"'
    '"#,
        ILError::NewlineInChar
    );

    test_err!("u8'\r'", ILError::NewlineInChar);

    test_err!("u'\n\r'", ILError::NewlineInChar);

    test_err!("L'\r'", ILError::NewlineInChar);
}

// Invalid hex in Char Literal
///
/// The most basic building block of numbers in C++ is the digit sequence
#[test]
pub fn test_err_invalid_hex() {
    test_err!(r#"'\X'"#, ILError::InvalidHexEscapeSeqChar);
    test_err!(r#"u'\X'"#, ILError::InvalidHexEscapeSeqChar);
    test_err!(r#"u8'\X'"#, ILError::InvalidHexEscapeSeqChar);
    test_err!(r#"L'\X'"#, ILError::InvalidHexEscapeSeqChar);
    test_err!(r#"u'\X'"#, ILError::InvalidHexEscapeSeqChar);
}

// \x used with no following hex digits
#[test]
pub fn test_err_hex_missing_numbers() {
    test_err!(r#"'\x'"#, ILError::CharEscapeSeqExpectedHexDigits);
    test_err!(r#"u'\x'"#, ILError::CharEscapeSeqExpectedHexDigits);
    test_err!(r#"u8'\x'"#, ILError::CharEscapeSeqExpectedHexDigits);
    test_err!(r#"L'\x'"#, ILError::CharEscapeSeqExpectedHexDigits);
    test_err!(r#"u'\x'"#, ILError::CharEscapeSeqExpectedHexDigits);
}
