use super::*;

use error::InternalLexerError as ILError;
use tokens::TokenKind;

fn hex_int_token() -> TokenKind<()> {
    TokenKind::PPNumber
}

fn decimal_int_token() -> TokenKind<()> {
    TokenKind::PPNumber
}

fn octal_token() -> TokenKind<()> {
    TokenKind::PPNumber
}

fn binary_token() -> TokenKind<()> {
    TokenKind::PPNumber
}

//                            Digit Sequences

// Decimal Digit Sequences
///
/// The most basic building block of numbers in C++ is the digit sequence
/// \d+(\'?\d)*
/// Examples:
/// 1
/// 12
/// 12'9
/// 12'98
/// 12'9'8765
#[test]
pub fn test_decimal_digit_sequences() {
    test_token!(r#"1"#, decimal_int_token(), 1);
    test_token!(r#"12"#, decimal_int_token(), 2);
    test_token!(r#"12'9"#, decimal_int_token(), 4);
    test_token!(r#"12'98"#, decimal_int_token(), 5);
    test_token!(r#"12'9'8765"#, decimal_int_token(), 9);
}

// Binary Digit Sequences
///
/// A variation of the digit sequence where the digits are in binary
/// 0(B|b)(0|1)+(\'?(0|1))*
/// Examples:
/// 0b1
/// 0B0
/// 0b101
/// 0B011'1
/// 0b1'1'0'0
#[test]
pub fn test_binary_digit_sequences() {
    test_token!("0b1", decimal_int_token(), 3);
    test_token!("0B0", decimal_int_token(), 3);
    test_token!("0b101", decimal_int_token(), 5);
    test_token!("0B011'1", decimal_int_token(), 7);
    test_token!("0b1'1'0'0", decimal_int_token(), 9);
}

// Octal Digit Sequences
///
/// A variation of the digit sequence where the digits are in octal
/// 0\o+(\'?\o)*    for \o = (0-7)
/// Examples:
/// 01
/// 00
/// 01231
/// 001471'1
/// 09'3'4'7
#[test]
pub fn test_octal_digit_sequences() {
    test_token!("01", decimal_int_token(), 2);
    test_token!("00", decimal_int_token(), 2);
    test_token!("01231", decimal_int_token(), 5);
    test_token!("001471'1", decimal_int_token(), 8);
    test_token!("09'3'4'7", decimal_int_token(), 8);
}

// Hexadecimal Digit Sequences
///
/// A variation of the digit sequence where the digits are in hex
/// 0(X|x)\h+(\'?\h)*    for \h = (\d|[a-fA-F])
/// Examples:
/// 0x1
/// 0xC
/// 0x12
/// 0xF00D
/// 0xFEED'CA0
/// 0xABC'123
/// 0xAB'E'F005
/// 0xAB'E'F005with_suffix
#[test]
pub fn test_hexadecimal_digit_sequences() {
    test_token!(r#"0x1"#, decimal_int_token(), 3);
    test_token!(r#"0xC"#, decimal_int_token(), 3);
    test_token!(r#"0x12"#, decimal_int_token(), 4);
    test_token!(r#"0xF00D"#, decimal_int_token(), 6);
    test_token!(r#"0xFEED'CA0"#, decimal_int_token(), 10);
    test_token!(r#"0xABC'123"#, decimal_int_token(), 9);
    test_token!(r#"0xAB'E'F005"#, decimal_int_token(), 11);
}

// Hexadecimal Digit Sequences with suffix
///
/// A variation of the digit sequence where the digits are in hex
/// 0(X|x)\h+(\'?\h)*    for \h = (\d|[a-fA-F])
/// Examples:
///
/// 0xAB'E'F005 with_suffix    -> No suffix
/// 0xAB'E'F005with_suffix     -> with suffix
#[test]
pub fn test_hexadecimal_digit_sequences_with_suffix() {
    test_token!(r#"1 with_suffix"#, decimal_int_token(), 1);
    test_token!(r#"0xAB'E'F005 with_suffix"#, decimal_int_token(), 11);
    test_token!(r#"0xAB'E'F005with_suffix"#, decimal_int_token(), 22);
}

// Parsing Hex Floats

#[test]
pub fn test_hex_float_simple() {
    test_token!(r#"0x1p1"#, TokenKind::PPNumber, 5);

    test_token!(r#"0x1p12"#, TokenKind::PPNumber, 6);

    test_token!(r#"0x12p12"#, TokenKind::PPNumber, 7);
}
#[test]
pub fn test_hex_float_fractional() {
    test_token!(r#"0x1.2p1"#, TokenKind::PPNumber, 7);
}

#[test]
pub fn test_hex_float_trailing_fractional() {
    test_token!(r#"0x1.p1"#, TokenKind::PPNumber, 6);
}

#[test]
pub fn test_hex_float_signed_power() {
    test_token!(r#"0x1.p+1"#, TokenKind::PPNumber, 7);

    test_token!(r#"0x1.p-1"#, TokenKind::PPNumber, 7);

    test_token!(r#"0xFEED1.P-1"#, TokenKind::PPNumber, 11);
}

// Decimal Float

#[test]
pub fn test_float_simple() {
    test_token!(r#"1.1"#, TokenKind::PPNumber, 3);

    test_token!(r#"1e1"#, TokenKind::PPNumber, 3);

    test_token!(r#"1E12"#, TokenKind::PPNumber, 4);

    test_token!(r#"12.12"#, TokenKind::PPNumber, 5);
}
#[test]
pub fn test_float_fractional() {
    test_token!(r#"1.2e1"#, TokenKind::PPNumber, 5);
}

#[test]
pub fn test_float_trailing_fractional() {
    test_token!(r#"1."#, TokenKind::PPNumber, 2);

    test_token!(r#"1.e1"#, TokenKind::PPNumber, 4);
}

#[test]
pub fn test_float_signed_power() {
    test_token!(r#"1.e+1"#, TokenKind::PPNumber, 5);

    test_token!(r#"1.E-1"#, TokenKind::PPNumber, 5);

    test_token!(r#"234.e-1"#, TokenKind::PPNumber, 7);
}

// Numeric Parsing Errors

// Digit Separators cant be next to each other
///
/// 0xAB.F005
/// |       |
/// +-------+---->  This is not allowed
#[test]
pub fn test_err_hexadecimal_float_needs_power() {
    test_token!(r#"0xAB.F005"#, TokenKind::PPNumber, 9);
}

// Digit Separators cant be next to each other
///
/// 0xAB''F005
///     |
///     +---->  This is not allowed
#[test]
pub fn test_err_hexadecimal_adjacent_digit_sep() {
    test_err!(r#"0xAB''F005"#, ILError::AdjacentDigitSeparators { digit_sep_idx: 5 });
}

// There can only be one decimal point in float literals
///
/// 0xAB.123.F005
///     |  |
///     |  +---> This is the second decimal point
///     |
///     +---->  First decimal point
#[test]
pub fn test_err_too_many_decimal_pts_ok_pp_num() {
    // Hex float
    test_token!(r#"0x13.2.p123"#, TokenKind::PPNumber, 11);
    test_token!(r#"0x13..p123"#, TokenKind::PPNumber, 10);
    test_token!(r#"0x..p123"#, TokenKind::PPNumber, 8);

    // Decimal float
    test_token!(r#"1..1"#, TokenKind::PPNumber, 4);
    test_token!(r#"0..1"#, TokenKind::PPNumber, 4);
    test_token!(r#"0.2.1"#, TokenKind::PPNumber, 5);
    test_token!(r#"0.1352.1"#, TokenKind::PPNumber, 8);
    test_token!(r#"12312312.."#, TokenKind::PPNumber, 10);
}

// Numeric literals that mention a power must give an exponent, elt it pass
///
/// 0xAB.123.F005
///     |  |
///     |  +---> This is the second decimal point
///     |
///     +---->  First decimal point
#[test]
pub fn test_err_missing_exponent_digits_ok_pp_num() {
    // Hex float
    test_token!(r#"0x1p"#, TokenKind::PPNumber, 4);
    test_token!(r#"0x1P+"#, TokenKind::PPNumber, 5);
    test_token!(r#"0x1p-"#, TokenKind::PPNumber, 5);
    test_token!(r#"0x1123p"#, TokenKind::PPNumber, 7);
    test_token!(r#"0x141P+"#, TokenKind::PPNumber, 7);
    test_token!(r#"0x1123P-"#, TokenKind::PPNumber, 8);

    // Decimal float
    test_token!(r#"1e"#, TokenKind::PPNumber, 2);
    test_token!(r#"0e+"#, TokenKind::PPNumber, 3);
    test_token!(r#"1e-"#, TokenKind::PPNumber, 3);
    test_token!(r#"1123e"#, TokenKind::PPNumber, 5);
    test_token!(r#"141e+"#, TokenKind::PPNumber, 5);
    test_token!(r#"1123e-"#, TokenKind::PPNumber, 6);
}

/// Binary literals must not contain non binary digits, this must be deferred and
/// handled by the preprocessor
///
/// 0b01212
///     |
///     +---->  First non binary digit
#[test]
pub fn test_err_invalid_binary_digit_ok_pp_num() {
    test_token!(r#"0b12"#, TokenKind::PPNumber, 4);
    test_token!(r#"0b12131"#, TokenKind::PPNumber, 7);
    test_token!(r#"0b12131 "#, TokenKind::PPNumber, 7);
}
