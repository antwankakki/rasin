use super::*;

use tokens::LiteralKind as Lk;
use tokens::TokenKind as Tk;

use error::InternalLexerError as ILError;

/// Helper constructor
fn raw_str(enc: Encoding, suffix_start_idx: Option<NonZeroUsize>) -> TokenKind<()> {
    let (kind, start_offset) = match enc {
        Encoding::Narrow => (Lk::OrdinaryRawString, (2)),
        Encoding::Wide => (Lk::WideRawString, (3)),
        Encoding::UTF16 => (Lk::UTF8RawString, (3)),
        Encoding::UTF32 => (Lk::UTF16RawString, (3)),
        Encoding::UTF8 => (Lk::UTF32RawString, (4)),
    };

    Tk::Literal { kind, start_offset, suffix_start_idx }
}

///  Raw string literals may be empty
#[test]
pub fn test_empty() {
    test_token!(r#"R"a()a""#, raw_str(Encoding::Narrow, None), 7);
    test_token!(r#"uR"**()**""#, raw_str(Encoding::UTF16, None), 10);
    test_token!(r#"LR"*~()*~""#, raw_str(Encoding::Wide, None), 10);
    test_token!(r#"UR"zzz()zzz""#, raw_str(Encoding::UTF32, None), 12);
    test_token!(r#"UR"zzz()zzz" "#, raw_str(Encoding::UTF32, None), 12);
}

/// Raw string literals may have a suffix
#[test]
pub fn test_with_suffix() {
    test_token!(r#"R"a()a"foo"#, raw_str(Encoding::Narrow, NonZeroUsize::new(7)), 10);
    test_token!(r#"uR"**()**"_bar"#, raw_str(Encoding::UTF16, NonZeroUsize::new(10)), 14);
    test_token!(r#"LR"*~()*~"zoo"#, raw_str(Encoding::Wide, NonZeroUsize::new(10)), 13);
    test_token!(r#"UR"zzz()zzz"baz"#, raw_str(Encoding::UTF32, NonZeroUsize::new(12)), 15);
    test_token!(r#"UR"zzz(stop)zzz"baz"#, raw_str(Encoding::UTF32, NonZeroUsize::new(16)), 19);
    test_token!(
        r#"UR"zzz(as
        ds)zzz"baz"#,
        raw_str(Encoding::UTF32, NonZeroUsize::new(25)),
        28
    );

    test_token!(
        r#"UR"zzz(as \t
        \z
        \0
        ds)zzz"baz"#,
        raw_str(Encoding::UTF32, NonZeroUsize::new(50)),
        53
    );
}

/// Raw string literals don't need user defined delimiters
#[test]
pub fn test_no_delim() {
    test_token!(r#"R"()""#, raw_str(Encoding::Narrow, None), 5);
    test_token!(r#"uR"(a)""#, raw_str(Encoding::UTF16, None), 7);
    test_token!(r#"LR"(\t)""#, raw_str(Encoding::Wide, None), 8);
    test_token!(r#"u8R"(\r\t)""#, raw_str(Encoding::UTF8, None), 11);
}

/// Raw string literals maybe empty
#[test]
pub fn test_empty_user_defined_delim() {
    test_token!(r#"R"a()a""#, raw_str(Encoding::Narrow, None), 7);
}

/// Don't conflate parenthesis inside
#[test]
pub fn test_parenthesis_user_defined_delim() {
    test_token!(r#"R"a(())a""#, raw_str(Encoding::Narrow, None), 9);
    test_token!(r#"R"((foo))""#, raw_str(Encoding::Narrow, None), 10);
    test_token!(r#"R"(())""#, raw_str(Encoding::Narrow, None), 7);
}

/// Non ordinary raw string literals may be empty
#[test]
pub fn test_empty_utf_user_defined_delim() {
    test_token!(r#"uR"a()a""#, raw_str(Encoding::UTF16, None), 8);
    test_token!(r#"UR"a()a""#, raw_str(Encoding::UTF32, None), 8);
    test_token!(r#"LR"a()a""#, raw_str(Encoding::Wide, None), 8);
    test_token!(r#"u8R"a()a""#, raw_str(Encoding::UTF8, None), 9);
}

/// Simple raw string literal with a user defined delimiter
#[test]
pub fn test_simple_user_defined_delim() {
    test_token!(r#"R"abc(WhatWhat)abc""#, raw_str(Encoding::Narrow, None), 19);
}

/// Raw String Literals may contain a new line without escaping
#[test]
pub fn test_newline_user_defined_delim() {
    test_token!(
        r#"R"abc(What

        What)abc""#,
        raw_str(Encoding::Narrow, None),
        29
    );
}

/// Raw String Literals do not escape anything
#[test]
pub fn test_escapeseq_user_defined_delim() {
    test_token!(r#"u8R"abc(\t\t\a)abc""#, raw_str(Encoding::UTF8, None), 19);
}

/// Raw String Literals do not escape anything
#[test]
pub fn test_newline_utf_user_defined_delim() {
    test_token!(
        r#"R"abc(What

        What)abc""#,
        raw_str(Encoding::Narrow, None),
        29
    );

    test_token!(
        r#"u8R"abc(aw
br)abc""#,
        raw_str(Encoding::UTF8, None),
        18
    );
}

// Errors

// Max raw string literal reached
///
/// Do not accept any raw string literals with more than 16 delimiters
#[test]
pub fn test_err_max_string_literal() {
    let err = |count| ILError::MaxRawStringDelimiterReached { max_char_offset: count };

    test_err!(r#"R"abcdefghijklmopqrstuv()"abcdefghijklmopqrstuv"#, err(18));

    test_err!(r#"R"abcdefghijklmopqrstuv()"abcdefghijklmopqrstuv"#, err(18));
    test_err!(r#"uR"abcdefghijklmopqrstuv()"abcdefghijklmopqrstuv"#, err(19));
    test_err!(r#"UR"abcdefghijklmopqrstuv()"abcdefghijklmopqrstuv"#, err(19));
    test_err!(r#"LR"abcdefghijklmopqrstuv()"abcdefghijklmopqrstuv"#, err(19));
    test_err!(r#"u8R"abcdefghijklmopqrstuv()"abcdefghijklmopqrstuv"#, err(20));
}

// Invalid Raw String Literal Chars
///
/// Check for errors on invalid delimiter chars
#[test]
pub fn test_err_invalid_raw_delimiter_char() {
    let err = |count: usize| ILError::InvalidRawDelimiterChar { invalid_char_idx: count };

    test_err!(r#"R"abcd'a()abcd'a""#, err(6));

    test_err!(r#"uR"abcd'a()abcd'a""#, err(7));
    test_err!(r#"UR"abcd'a()abcd'a""#, err(7));
    test_err!(r#"LR"abcd'a()abcd'a""#, err(7));
    test_err!(r#"u8R"abcd'a()abcd'a""#, err(8));
}
