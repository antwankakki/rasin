use super::*;

use crate::intern::VectorInternPolicy;



macro_rules! test_interned {
    ($input:tt, $( $x:expr ),*) => {{
        let mut interner = Vec::<String>::default();
        let iter = &mut CppCharIt::new($input);

        // consume all tokens
        let _ = std::iter::from_fn(|| crate::lex::<VectorInternPolicy>(iter, &mut interner).ok()).count();

        $(
            assert_eq!(VectorInternPolicy::lookup(&interner, &$x.0), Some($x.1));
        )*
    }};
}

// test sliced atom
#[test]
pub fn test_atom() {
    test_interned!(r#"a aa aaa a"#, (0usize, "a"), (1usize, "aa"), (2usize, "aaa"));
}

// test sliced atom
#[test]
pub fn test_atom_slicing() {
    test_interned!(
        r#"\
i\
f"#,
        (0usize, "i\\\nf")
    );
}
