use super::*;

#[macro_use]
mod test_utils;

mod char_literals;
mod comments;
mod intern;
mod numeric;
mod raw_string_literals;
mod string_literals;
mod test_cppcharit;

use tokens::TokenKind;

use std::num::NonZeroUsize;

// Test normal spaces
#[test]
pub fn test_whitespace() {
    test_token!("   ", TokenKind::<()>::Space, 3);
}

// test token combinations
#[test]
pub fn test_simple_combo() {
    test_tokens!("   Hi", (TokenKind::<()>::Space, 3), (TokenKind::<()>::Atom(()), 2));
}

// test token combinations
#[test]
pub fn test_header_combo() {
    test_tokens!(
        r#"#include <Hello.h> 1+2 R"What(1\)What" "#,
        (TokenKind::<()>::Hash, 1),
        (TokenKind::<()>::Atom(()), 7),
        (TokenKind::<()>::Space, 1),
        (TokenKind::<()>::Lt, 1),
        (TokenKind::<()>::Atom(()), 5),
        (TokenKind::<()>::Dot, 1),
        (TokenKind::<()>::Atom(()), 1), // h
        (TokenKind::<()>::Gt, 1),
        (TokenKind::<()>::Space, 1)
    );
}

// Test sliced normal spaces
#[test]
pub fn test_sliced_whitespace() {
    test_tokens_with_slice!(
        r#"   \
     "#,
        (TokenKind::<()>::Space, 10, true)
    );
}

// if the next three characters are <:: and the subsequent character is neither
//  : nor >, the < is treated as a preprocessing token by itself and not as the
//  first character of the alternative token <:.
#[test]
pub fn test_angle_alternate_tokens() {
    test_tokens!(
        "<::a",
        (TokenKind::<()>::Lt, 1),
        (TokenKind::<()>::ColonColon, 2),
        (TokenKind::<()>::Atom(()), 1)
    );

    test_tokens!(
        "<:: ",
        (TokenKind::<()>::Lt, 1),
        (TokenKind::<()>::ColonColon, 2),
        (TokenKind::<()>::Space, 1)
    );

    test_tokens!("<:::", (TokenKind::<()>::OpenBracket, 2), (TokenKind::<()>::ColonColon, 2));

    test_tokens!("<::", (TokenKind::<()>::Lt, 1), (TokenKind::<()>::ColonColon, 2));

    test_tokens!("<::>", (TokenKind::<()>::OpenBracket, 2), (TokenKind::<()>::CloseBracket, 2));
}

// The program fragment x+++++y is parsed as x ++ ++ + y, which, if x and y
// have integral types, violates a constraint on increment operators, even
// though the parse x ++ + ++ y might yield a correct expression.
#[test]
pub fn test_integer_addition_tokens() {
    test_tokens!(
        "x+++++y",
        (TokenKind::<()>::Atom(()), 1), // x
        (TokenKind::<()>::PlusPlus, 2),
        (TokenKind::<()>::PlusPlus, 2),
        (TokenKind::<()>::Plus, 1),
        (TokenKind::<()>::Atom(()), 1) // y
    );
}

// Test token combinations
#[test]
pub fn test_slicing_combination() {
    test_tokens_with_slice!(
        "in\\\nt # ## #\\\n# Hell\\\no",
        (TokenKind::<()>::Atom(()), 5, true),
        (TokenKind::<()>::Space, 1, false),
        (TokenKind::<()>::Hash, 1, false),
        (TokenKind::<()>::Space, 1, false),
        (TokenKind::<()>::HashHash, 2, false),
        (TokenKind::<()>::Space, 1, false),
        (TokenKind::<()>::HashHash, 4, true),
        (TokenKind::<()>::Space, 1, false),
        (TokenKind::<()>::Atom(()), 7, true)
    );
}

// test sliced atom
#[test]
pub fn test_atom_slicing() {
    test_tokens_with_slice!(
        r#"\
i\
f"#,
        (TokenKind::<()>::Atom(()), 6, true)
    );
}
