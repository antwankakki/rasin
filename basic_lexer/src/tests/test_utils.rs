macro_rules! test_token {
    ($input:tt, $expected_token:expr, $expected_size:literal) => {{
        assert_eq!(
            crate::lex::<()>(&mut CppCharIt::new($input), &mut ()),
            Ok(CppToken { kind: $expected_token, size: $expected_size, sliced: false })
        );
    }};
}

macro_rules! test_err {
    ($input:tt, $expected_err:expr) => {{
        assert_eq!(crate::lex::<()>(&mut CppCharIt::new($input), &mut ()), Err($expected_err));
    }};
}

macro_rules! test_token_neq {
    ($input:tt, $expected_token:expr) => {{
        assert_ne!(
            crate::lex::<()>(&mut CppCharIt::new($input), &mut ()).map(|tok| tok.kind),
            Ok($expected_token)
        );
    }};
}

macro_rules! test_tokens {
    ($input:tt, $( $x:expr ),*) => {{
        let iter = &mut CppCharIt::new($input);
        $(
            assert_eq!(crate::lex::<()>(iter, &mut ()),
            Ok(CppToken {
                kind: $x.0,
                size: $x.1,
                sliced: false
            }));
        )*
    }};
}

macro_rules! test_tokens_with_slice {
    ($input:tt, $( $x:expr ),*) => {{
        let iter = &mut CppCharIt::new($input);
        $(
            assert_eq!(crate::lex::<()>(iter, &mut ()),
            Ok(CppToken {
                kind: $x.0,
                size: $x.1,
                sliced: $x.2
            }));
        )*
    }};
}
