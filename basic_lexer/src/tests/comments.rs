use super::*;

use tokens::TokenKind;

// Test Block Comments
#[test]
pub fn test_block_comment() {
    test_token!("/* * / /* */", TokenKind::<()>::BlockComment, 12);
    test_token!("/* * / /* */ ", TokenKind::<()>::BlockComment, 12);
}
#[test]
pub fn test_incomplete_block_comment() {
    test_token_neq!("/*", TokenKind::<()>::BlockComment);
    test_token_neq!("/* ", TokenKind::<()>::BlockComment);
    test_token_neq!("/* * ", TokenKind::<()>::BlockComment);
    test_token_neq!("/* /   ", TokenKind::<()>::BlockComment);
}
#[test]
pub fn test_multiline_block_comment() {
    test_token!(
        r#"/*
        Hello
    */"#,
        TokenKind::<()>::BlockComment,
        23
    );
}
#[test]
pub fn test_multiline_nested_block_comment() {
    test_token!(
        r#"/*
        /* /* Wat Wat */
    */"#,
        TokenKind::<()>::BlockComment,
        27
    );
}
#[test]
pub fn test_simple_empty_block_comment() {
    test_token!("/**/", TokenKind::<()>::BlockComment, 4);
}
#[test]
pub fn test_empty_block_comment() {
    test_token!("/**/ ", TokenKind::<()>::BlockComment, 4);
}
// Test Line Comments
#[test]
pub fn test_empty_line_comment() {
    test_token!("//", TokenKind::<()>::LineComment, 2);
}
#[test]
pub fn test_line_comment() {
    test_token!("// 💩", TokenKind::<()>::LineComment, 7);
}

// Line comments with splicing count
#[test]
pub fn test_line_comment_slicing() {
    let input = r#"// Hello\
world\
wat"#;

    test_tokens_with_slice!(input, (TokenKind::<()>::LineComment, 20, true));

    let input = r#"// Hello\
world\
wat
    asd
Hello World"#;

    test_tokens_with_slice!(
        input,
        (TokenKind::<()>::LineComment, 20, true),
        (TokenKind::<()>::NewLine, 1, false),
        (TokenKind::<()>::Space, 4, false),
        (TokenKind::<()>::Atom(()), 3, false),
        (TokenKind::<()>::NewLine, 1, false),
        (TokenKind::<()>::Atom(()), 5, false),
        (TokenKind::<()>::Space, 1, false),
        (TokenKind::<()>::Atom(()), 5, false)
    );
}

// Nesting Line comments with splicing count
#[test]
pub fn test_block_comment_nesting() {
    test_tokens_with_slice!(
        r#"/* first level /* still same comment */ text outside\
        /* first level /* still same comment */ "#,
        (TokenKind::<()>::BlockComment, 39, false),
        (TokenKind::<()>::Space, 1, false),
        (TokenKind::<()>::Atom(()), 4, false),
        (TokenKind::<()>::Space, 1, false),
        (TokenKind::<()>::Atom(()), 7, false),
        (TokenKind::<()>::Space, 10, true),
        (TokenKind::<()>::BlockComment, 39, false),
        (TokenKind::<()>::Space, 1, false)
    );
}
