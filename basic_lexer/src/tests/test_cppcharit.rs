use crate::cppchariter::{advance, eat_while, eat_while_max, skip_n, CppCharIt};

/// Test advancing the cpp char iterator
#[test]
pub fn test_cppchar_next() {
    let mut iter = CppCharIt::new("A");
    let c = advance(&mut iter);
    assert_eq!(c.map(|v| v.character()), Some('A'));
    assert_eq!(c.map(|v| v.physical_index()), Some(0));
    assert_eq!(c.map(|v| v.logical_index()), Some(0));

    let mut iter = CppCharIt::new("\\\nA");
    let c = advance(&mut iter);
    assert_eq!(c.map(|v| v.character()), Some('A'));
    assert_eq!(c.map(|v| v.physical_index()), Some(2));
    assert_eq!(c.map(|v| v.logical_index()), Some(0));

    let mut iter = CppCharIt::new("\\\n\\\nA");
    let c = advance(&mut iter);
    assert_eq!(c.map(|v| v.character()), Some('A'));
    assert_eq!(c.map(|v| v.physical_index()), Some(4));
    assert_eq!(c.map(|v| v.logical_index()), Some(0));
}

/// Test advancing the cpp char iterator with newline
#[test]
pub fn test_cppchar_next_newline() {
    let mut iter = CppCharIt::new("\\\n\n ");
    let c = advance(&mut iter);
    assert_eq!(c.map(|v| v.character()), Some('\n'));
    assert_eq!(c.map(|v| v.physical_index()), Some(2));
    assert_eq!(c.map(|v| v.logical_index()), Some(0));

    let mut iter = CppCharIt::new("\\\n\n");
    let c = advance(&mut iter);
    assert_eq!(c.map(|v| v.character()), Some('\n'));
    assert_eq!(c.map(|v| v.physical_index()), Some(2));
    assert_eq!(c.map(|v| v.logical_index()), Some(0));

    let mut iter = CppCharIt::new("\\\n\\\n\n");
    let c = advance(&mut iter);
    assert_eq!(c.map(|v| v.character()), Some('\n'));
    assert_eq!(c.map(|v| v.physical_index()), Some(4));
    assert_eq!(c.map(|v| v.logical_index()), Some(0));
}

/// Test advancing the cpp char iterator with slices
#[test]
pub fn test_cppchar_next_backslash() {
    let mut iter = CppCharIt::new("\\\n\\ ");
    let c = advance(&mut iter);
    assert_eq!(c.map(|v| v.character()), Some('\\'));
    assert_eq!(c.map(|v| v.physical_index()), Some(2));
    assert_eq!(c.map(|v| v.logical_index()), Some(0));

    let mut iter = CppCharIt::new("\\\n\\");
    let c = advance(&mut iter);
    assert_eq!(c.map(|v| v.character()), Some('\\'));
    assert_eq!(c.map(|v| v.physical_index()), Some(2));
    assert_eq!(c.map(|v| v.logical_index()), Some(0));

    let mut iter = CppCharIt::new("\\\n\\\n\\");
    let c = advance(&mut iter);
    assert_eq!(c.map(|v| v.character()), Some('\\'));
    assert_eq!(c.map(|v| v.physical_index()), Some(4));
    assert_eq!(c.map(|v| v.logical_index()), Some(0));
}

/// Test skip n, returns the index of the last token that was read
#[test]
pub fn test_skip_n() {
    let mut iter = CppCharIt::new("wat wat hello");
    assert_eq!(skip_n(&mut iter, 1), Some((0, false)));
    assert_eq!(iter.as_str(), "at wat hello");

    let mut iter = CppCharIt::new("a aaB");
    assert_eq!(skip_n(&mut iter, 4), Some((3, false)));
    assert_eq!(iter.as_str(), "B");
}

/// Test skip n, returns the index of the last token that was read
#[test]
pub fn test_skip_n_sliced() {
    let mut iter = CppCharIt::new("A\\\nB");
    assert_eq!(skip_n(&mut iter, 1), Some((0, false)));
    assert_eq!(iter.as_str(), "\\\nB");

    let mut iter = CppCharIt::new("\\\nAB");
    assert_eq!(skip_n(&mut iter, 1), Some((2, true)));
    assert_eq!(iter.as_str(), "B");

    // it doesn't nest
    let mut iter = CppCharIt::new("\\\n\\\nAB");
    assert_eq!(skip_n(&mut iter, 1), Some((4, true)));
    assert_eq!(iter.as_str(), "B");
}

/// Check that eat_while works as expected (eat as many chars matching the predicate, do not consume
/// the tokens after)
#[test]
pub fn test_eat_while() {
    let mut iter = CppCharIt::new("aaaB");
    assert_eq!(eat_while(&mut iter, |c| c == 'a'), (3, 3, false));
    assert_eq!(advance(&mut iter).map(|v| v.character()), Some('B'));

    let mut iter = CppCharIt::new("a aaB");
    assert_eq!(eat_while(&mut iter, |c| c == 'a'), (1, 1, false));
    assert_eq!(advance(&mut iter).map(|v| v.character()), Some(' '));
}

/// Eat the maximum tokens we can possible eat and return the number of chars it consumed as well as
/// whether a line slice occurred
#[test]
pub fn test_eat_while_sliced() {
    /*    let mut iter = CppCharIt::new("aa\\\naB");
        assert_eq!(eat_while(&mut iter, |c| c == 'a'), (3, 5, true));
        assert_eq!(advance(&mut iter).map(|v| v.character()), Some('B'));

        let mut iter = CppCharIt::new("\\\na aaB");
        assert_eq!(eat_while(&mut iter, |c| c == 'a'), (1, 3, true));
        assert_eq!(advance(&mut iter).map(|v| v.character()), Some(' '));
    */
    let mut iter = CppCharIt::new("\\\naaaB");
    assert_eq!(eat_while(&mut iter, |c| c == 'a'), (3, 5, true));
    assert_eq!(advance(&mut iter).map(|v| v.character()), Some('B'));
}

/// Eat the maximum tokens we can possible eat and return the number of chars it consumed as well as
/// whether a line slice occurred
#[test]
pub fn test_eat_while_max() {
    let mut iter = CppCharIt::new("aaaB");
    assert_eq!(eat_while_max(&mut iter, |c| c == 'a', 1), (1, false));
    assert_eq!(advance(&mut iter).map(|v| v.character()), Some('a'));

    let mut iter = CppCharIt::new("aaaB");
    assert_eq!(eat_while_max(&mut iter, |c| c == 'a', 2), (2, false));
    assert_eq!(advance(&mut iter).map(|v| v.character()), Some('a'));

    let mut iter = CppCharIt::new("aaaB");
    assert_eq!(eat_while_max(&mut iter, |c| c == 'a', 3), (3, false));
    assert_eq!(advance(&mut iter).map(|v| v.character()), Some('B'));

    let mut iter = CppCharIt::new("aaaB");
    assert_eq!(eat_while_max(&mut iter, |c| c == 'a', 6), (3, false));
    assert_eq!(advance(&mut iter).map(|v| v.character()), Some('B'));

    let mut iter = CppCharIt::new("a aaB");
    assert_eq!(eat_while_max(&mut iter, |c| c == 'a', 3), (1, false));
    assert_eq!(advance(&mut iter).map(|v| v.character()), Some(' '));
}

/// Eat the maximum tokens we can possible eat and return the number of chars it consumed as well as
/// whether a line slice occurred
#[test]
pub fn test_eat_while_sliced_max() {
    let mut iter = CppCharIt::new("aa\\\naB");
    assert_eq!(eat_while_max(&mut iter, |c| c == 'a', 1), (1, false));
    assert_eq!(iter.as_str(), "a\\\naB");

    let mut iter = CppCharIt::new("\\\na aaB");
    assert_eq!(eat_while_max(&mut iter, |c| c == 'a', 1), (3, true));
    assert_eq!(iter.as_str(), " aaB");

    let mut iter = CppCharIt::new("\\\naaaB");
    assert_eq!(eat_while_max(&mut iter, |c| c == 'a', 10), (5, true));
    assert_eq!(iter.as_str(), "B");
}

#[test]
pub fn test_eat_while_nothing() {
    let mut iter = CppCharIt::new("aaaa");
    assert_eq!(eat_while(&mut iter, |_| false), (0, 0, false));
    assert_eq!(iter.count(), 4);
}
