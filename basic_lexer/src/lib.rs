//! Internal Simple Lexer.
//!
//! This lexer is converts text to a sequence of basic tokens that are later
//! consumed by the preprocessor. This also handles line slicing automatically
//! to simplify the logic in the preprocessor.
#![allow(dead_code)]
pub mod cppchariter;
pub mod error;
pub mod tokens;

use cppchariter::{advance, eat_while, peek_next, peek_nth, skip_n, CppChar};
pub mod utils;

use tokens::get_whitespace_token;

use tokens::Encoding;

// lex modules
mod lex_comments;
mod lex_strings;

// validation functions
use lex_comments::lex_block_comment;
use lex_strings::{lex_char_literal, lex_raw_string_literal, lex_string_literal};
use tokens::{is_atom_continue, is_atom_start, is_digit, is_non_digit};

pub mod intern;
/// Test module
#[cfg(test)]
mod tests;

/// InternalToken
///
/// This is the basic lexer unit for the compiler. it represents a wide
/// range of tokens supported that may become preprocessor tokens or be
/// combined to make cpp tokens.
#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub struct CppToken<Sym: InternedRep> {
    /// What token was parsed
    pub kind: TokenKind<Sym>,

    /// How many characters were parsed (includes trailing line splices)
    pub size: usize,

    /// Whether this token was sliced and needs to be cleaned up
    pub sliced: bool,
}

/// Type alias representing an internal lexer error
pub type ILError = error::InternalLexerError;

/// x lexer token or an error
pub type ILResult<Sym> = Result<CppToken<Sym>, ILError>;

/// The kind of literal token (String, Number...)
pub use tokens::LiteralKind;

use crate::intern::{InternPolicy, InternedRep};
/// The lexer token kind
pub use tokens::TokenKind;

/// The iterator over char streams
pub type CppCharIt<'input> = cppchariter::CppCharIt<'input>;

/// Consume a token from the input
///
/// The main method of this module. Returns OK(Token) having consumed the
/// length that this token consists of, or Err if it encountered a lexer
/// error (or semantic errors that can be caught early).
///
/// Note that errors are generally not consumed
#[allow(clippy::cognitive_complexity)]
pub fn lex<IP: InternPolicy>(iter: &mut CppCharIt, ctx: &mut IP::Ctx) -> ILResult<IP::Symbol> {
    // Try to match actual tokens
    let (curr_char, start_idx) =
        peek_next(iter).map(|v| (v.character(), v.physical_index())).ok_or(ILError::EOF)?;

    let next_char = peek_nth(iter, 1).map(|v| v.character()).unwrap_or('\0');

    match curr_char {
        // check whitespace
        c if get_whitespace_token::<IP::Symbol>(c).is_some() => {
            let (_log_size, phys_size, sliced) = eat_while(iter, |d| d == c);

            Ok(CppToken { kind: get_whitespace_token(c).unwrap(), size: phys_size, sliced })
        }

        // match maybe a comment or maybe a slash
        '/' => match next_char {
            '/' => {
                let (_log_size, phys_size, sliced) = eat_while(iter, |c| c != '\n');
                Ok(CppToken { kind: TokenKind::LineComment, size: phys_size, sliced })
            }
            '*' => lex_block_comment(iter, start_idx),
            '=' => n_chars_token(iter, 2, TokenKind::SlashEq, start_idx),
            _ => n_chars_token(iter, 1, TokenKind::Slash, start_idx),
        },

        // Integer/Float numeric constant
        '0'..='9' => lex_numeric_constant(iter),

        // maybe atom maybe utf8 or 16 raw string, string, or char literal
        'u' => match (
            next_char,
            peek_nth(iter, 2).map(|v| v.character()),
            peek_nth(iter, 3).map(|v| v.character()),
        ) {
            ('8', Some('R'), Some('"')) => lex_raw_string_literal::<IP>(iter, ctx, Encoding::UTF8),
            ('R', Some('"'), _) => lex_raw_string_literal::<IP>(iter, ctx, Encoding::UTF16),
            ('8', Some('"'), _) => lex_string_literal::<IP>(iter, ctx, start_idx, Encoding::UTF8),
            ('8', Some('\''), _) => lex_char_literal::<IP>(iter, ctx, start_idx, Encoding::UTF8),
            ('"', _, _) => lex_string_literal::<IP>(iter, ctx, start_idx, Encoding::UTF16),
            ('\'', _, _) => lex_char_literal::<IP>(iter, ctx, start_idx, Encoding::UTF16),
            _ => lex_atom::<IP>(iter, ctx, start_idx),
        },

        // atom, or UTF32 raw string, string, or char literal
        'U' => match (next_char, peek_nth(iter, 2).map(|v| v.character())) {
            ('R', Some('"')) => lex_raw_string_literal::<IP>(iter, ctx, Encoding::UTF32),
            ('"', _) => lex_string_literal::<IP>(iter, ctx, start_idx, Encoding::UTF32),
            ('\'', _) => lex_char_literal::<IP>(iter, ctx, start_idx, Encoding::UTF32),
            _ => lex_atom::<IP>(iter, ctx, start_idx),
        },

        // atom, or narrow string literals
        'R' => match next_char {
            '"' => lex_raw_string_literal::<IP>(iter, ctx, Encoding::Narrow),
            _ => lex_atom::<IP>(iter, ctx, start_idx),
        },

        // atom, or wide string literals
        'L' => match (next_char, peek_nth(iter, 2).map(|v| v.character())) {
            ('R', Some('"')) => lex_raw_string_literal::<IP>(iter, ctx, Encoding::Wide),
            ('"', _) => lex_string_literal::<IP>(iter, ctx, start_idx, Encoding::Wide),
            ('\'', _) => lex_char_literal::<IP>(iter, ctx, start_idx, Encoding::Wide),
            _ => lex_atom::<IP>(iter, ctx, start_idx),
        },

        // narrow char literal
        '\'' => lex_char_literal::<IP>(iter, ctx, start_idx, Encoding::Narrow),

        // narrow string literal
        '"' => lex_string_literal::<IP>(iter, ctx, start_idx, Encoding::Narrow),

        // lex dot or ellipses
        '.' => match (next_char, peek_nth(iter, 2).map(|v| v.character())) {
            ('.', Some('.')) => n_chars_token(iter, 3, TokenKind::Ellipsis, start_idx),
            (c, _) if c.is_ascii_digit() => lex_numeric_constant(iter),
            _ => n_chars_token(iter, 1, TokenKind::Dot, start_idx),
        },

        // Lex Amp, AmpAmp, or AmpEq
        '&' => match next_char {
            '&' => n_chars_token(iter, 2, TokenKind::AmpAmp, start_idx),
            '=' => n_chars_token(iter, 2, TokenKind::AmpEq, start_idx),
            _ => n_chars_token(iter, 1, TokenKind::Amp, start_idx),
        },

        // Lex Star or StarEq
        '*' => match next_char {
            '=' => n_chars_token(iter, 2, TokenKind::StarEq, start_idx),
            _ => n_chars_token(iter, 1, TokenKind::Star, start_idx),
        },

        // Lex Plus or PlusEq, PlusPlus
        '+' => match next_char {
            '+' => n_chars_token(iter, 2, TokenKind::PlusPlus, start_idx),
            '=' => n_chars_token(iter, 2, TokenKind::PlusEq, start_idx),
            _ => n_chars_token(iter, 1, TokenKind::Plus, start_idx),
        },

        // Lex ArrowStar, Arrow, MinusEq, MinusMinus, or Minus
        '-' => match (next_char, peek_nth(iter, 2).map(|v| v.character())) {
            ('>', Some('*')) => n_chars_token(iter, 3, TokenKind::ArrowStar, start_idx),
            ('>', _) => n_chars_token(iter, 2, TokenKind::Arrow, start_idx),
            ('-', _) => n_chars_token(iter, 2, TokenKind::MinusMinus, start_idx),
            ('=', _) => n_chars_token(iter, 2, TokenKind::MinusEq, start_idx),
            _ => n_chars_token(iter, 1, TokenKind::Minus, start_idx),
        },

        // Lex Exclaim or ExclaimEq
        '!' => match next_char {
            '=' => n_chars_token(iter, 2, TokenKind::ExclaimEq, start_idx),
            _ => n_chars_token(iter, 1, TokenKind::Exclaim, start_idx),
        },

        // Digraphs, Percent, PercentEq
        '%' => match (
            next_char,
            peek_nth(iter, 2).map(|v| v.character()),
            peek_nth(iter, 3).map(|v| v.character()),
        ) {
            // %:%: -> ##
            (':', Some('%'), Some(':')) => n_chars_token(iter, 3, TokenKind::HashHash, start_idx),

            // %: -> #
            (':', _, _) => n_chars_token(iter, 2, TokenKind::Hash, start_idx),

            // %> -> }
            ('>', _, _) => n_chars_token(iter, 2, TokenKind::CloseBrace, start_idx),

            // %=
            ('=', _, _) => n_chars_token(iter, 2, TokenKind::PercentEq, start_idx),

            // %
            _ => n_chars_token(iter, 1, TokenKind::Percent, start_idx),
        },

        // Digraphs, Percent, PercentEq
        '<' => match (next_char, peek_nth(iter, 2).map(|v| v.character())) {
            // <<=
            ('<', Some('=')) => n_chars_token(iter, 3, TokenKind::LtLtEq, start_idx),

            // <=>
            ('=', Some('>')) => n_chars_token(iter, 3, TokenKind::Spaceship, start_idx),

            // <<
            ('<', _) => n_chars_token(iter, 2, TokenKind::LtLt, start_idx),

            // =
            ('=', _) => n_chars_token(iter, 2, TokenKind::LtEq, start_idx),

            // <% -> {
            ('%', _) => n_chars_token(iter, 2, TokenKind::OpenBrace, start_idx),

            // Per C++ Standard lex.pptoken 3.2
            (':', Some(':')) => match peek_nth(iter, 3).map(|v| v.character()) {
                // <:: -> [
                Some(':') | Some('>') => n_chars_token(iter, 2, TokenKind::OpenBracket, start_idx),
                _ => n_chars_token(iter, 1, TokenKind::Lt, start_idx),
            },

            // <: -> [
            (':', _) => n_chars_token(iter, 2, TokenKind::Lt, start_idx),

            // <
            _ => n_chars_token(iter, 1, TokenKind::Lt, start_idx),
        },

        // Lex Gt, GtGt, GtEq
        '>' => match (next_char, peek_nth(iter, 2).map(|v| v.character())) {
            // >>=
            ('>', Some('=')) => n_chars_token(iter, 3, TokenKind::GtGtEq, start_idx),

            // >>
            ('>', _) => n_chars_token(iter, 2, TokenKind::GtGt, start_idx),

            // >
            _ => n_chars_token(iter, 1, TokenKind::Gt, start_idx),
        },

        // Lex Caret, CaretEq
        '^' => match next_char {
            // ^=
            '=' => n_chars_token(iter, 2, TokenKind::CaretEq, start_idx),

            // ^
            _ => n_chars_token(iter, 1, TokenKind::Caret, start_idx),
        },

        // Lex Pipe, PipeEq
        '|' => match next_char {
            // |=
            '=' => n_chars_token(iter, 2, TokenKind::PipeEq, start_idx),

            // ||
            '|' => n_chars_token(iter, 2, TokenKind::PipePipe, start_idx),

            // |
            _ => n_chars_token(iter, 1, TokenKind::Pipe, start_idx),
        },

        // Lex Digraph, Colon, Colon Colon
        ':' => match next_char {
            // :> -> ]
            '>' => n_chars_token(iter, 2, TokenKind::CloseBracket, start_idx),

            // ::
            ':' => n_chars_token(iter, 2, TokenKind::ColonColon, start_idx),

            // :
            _ => n_chars_token(iter, 1, TokenKind::Colon, start_idx),
        },

        // Lex Eq, EqEq
        '=' => match next_char {
            // ==
            '=' => n_chars_token(iter, 2, TokenKind::EqualEqual, start_idx),

            // =
            _ => n_chars_token(iter, 1, TokenKind::Equal, start_idx),
        },

        // Lex Eq, EqEq
        '#' => match next_char {
            // ##
            '#' => n_chars_token(iter, 2, TokenKind::HashHash, start_idx),

            // #
            _ => n_chars_token(iter, 1, TokenKind::Hash, start_idx),
        },

        // Match Single Tokens
        '?' => n_chars_token(iter, 1, TokenKind::Question, start_idx),
        '[' => n_chars_token(iter, 1, TokenKind::OpenBracket, start_idx),
        ']' => n_chars_token(iter, 1, TokenKind::CloseBracket, start_idx),
        '(' => n_chars_token(iter, 1, TokenKind::OpenParen, start_idx),
        ')' => n_chars_token(iter, 1, TokenKind::CloseParen, start_idx),
        '{' => n_chars_token(iter, 1, TokenKind::OpenBrace, start_idx),
        '}' => n_chars_token(iter, 1, TokenKind::CloseBrace, start_idx),
        '~' => n_chars_token(iter, 1, TokenKind::Tilde, start_idx),
        ';' => n_chars_token(iter, 1, TokenKind::Semi, start_idx),
        ',' => n_chars_token(iter, 1, TokenKind::Comma, start_idx),
        '\n' => n_chars_token(iter, 1, TokenKind::NewLine, start_idx),

        // Check if we are to lex an atoms
        c if is_atom_start(c) => match eat_while(iter, is_atom_continue) {
            (_, 0, sliced) => Ok(CppToken { kind: TokenKind::Unknown(c), size: 1, sliced }),
            (logical_size, phys_size, sliced) => {
                let sym =
                    IP::intern(ctx, &iter.data()[start_idx..start_idx + logical_size], start_idx);
                Ok(CppToken { kind: TokenKind::Atom(sym), size: phys_size, sliced })
            }
        },

        // Saw an unknown token
        c => n_chars_token(iter, 1, TokenKind::Unknown(c), start_idx),
    }
}

fn n_chars_token<Sym: InternedRep>(
    iter: &mut CppCharIt,
    num: usize,
    kind: TokenKind<Sym>,
    start_idx: usize,
) -> ILResult<Sym> {
    skip_n(iter, num)
        .map(|v| CppToken { kind, size: v.0 - start_idx + 1, sliced: v.1 })
        .ok_or(ILError::EOF)
}

/// Lex the remainder of a numeric constant as specified by
/// The C++ standard's 5.13.2 - Integer Literals and 5.13.4 - Floating
/// Point Literals.
fn lex_numeric_constant<Sym: InternedRep>(iter: &mut CppCharIt) -> ILResult<Sym> {
    let start_idx = peek_next(iter).map(|c| c.logical_index()).unwrap_or(0);
    let mut num = 0;
    let mut cloned_iter = iter.clone();
    while let Some(c) = advance(&mut cloned_iter) {
        match c.character() {
            // if there is a number, period, or a digit separator continue
            '0'..='9' | '.' => (),

            // only accept '+' or '-' characters of they are preceded by
            // exponent chars
            'e' | 'E' | 'p' | 'P' => {
                num += 1;
                match advance(&mut cloned_iter).map(|v| v.character()) {
                    Some('+') => (),
                    Some('-') => (),
                    _ => (),
                }
            }

            '\'' => {
                let next_cx = advance(&mut cloned_iter);
                match next_cx.map(|v| v.character()) {
                    Some(c) if is_digit(c) => (),
                    Some(c) if is_non_digit(c) => (),
                    Some('\'') => {
                        return Err(ILError::AdjacentDigitSeparators {
                            digit_sep_idx: next_cx.unwrap().physical_index(),
                        });
                    }
                    _ => {
                        let last_index = skip_n(iter, num + 2);
                        return Err(ILError::UnexpectedCharAfterDigitSeparator {
                            digit_sep_idx: last_index.map(|v| v.0).unwrap_or(start_idx),
                        });
                    }
                };
                num += 1;
            }

            // if this can be a part of a suffix (i.e an atom)
            c if is_atom_continue(c) => (),

            _ => break,
        }
        num += 1;
    }
    n_chars_token(iter, num, TokenKind::PPNumber, start_idx)
}

fn lex_digit_seq<F>(iter: &mut CppCharIt, predicate: F) -> Result<usize, ILError>
where
    F: Fn(char) -> bool,
{
    #[derive(PartialEq, Eq)]
    enum DigitSeqState {
        Continue,
        AdjacentSep(usize),
        FailedPredicate(CppChar),
    }

    let valid_digit_seq = |pair: (Option<CppChar>, Option<CppChar>)| match pair {
        (Some(x), Some(y)) if x.character() == '\'' && y.character() == '\'' => {
            DigitSeqState::AdjacentSep(y.physical_index())
        }
        (Some(x), Some(y)) if x.character() == '\'' && !predicate(y.character()) => {
            DigitSeqState::FailedPredicate(y)
        }
        (Some(x), _) if !predicate(x.character()) => DigitSeqState::FailedPredicate(x),
        _ => DigitSeqState::Continue,
    };

    let stopping_token = std::iter::repeat_with(|| (advance(iter), peek_next(iter)))
        .map(valid_digit_seq)
        .find(|v| *v != DigitSeqState::Continue);

    match stopping_token {
        Some(DigitSeqState::Continue) => unreachable!(),
        Some(DigitSeqState::AdjacentSep(idx)) => {
            skip_n(iter, 2);
            Err(ILError::AdjacentDigitSeparators { digit_sep_idx: idx })
        }
        Some(DigitSeqState::FailedPredicate(c)) => Ok(c.physical_index()),
        None => Err(ILError::EOF),
    }
}

/// Returns the starting index of the suffix as well as the suffix atom token
fn try_parse_suffix<IP: InternPolicy>(
    iter: &mut CppCharIt,
    ctx: &mut IP::Ctx,
) -> Option<(usize, CppToken<IP::Symbol>)> {
    let suffix_start_idx = peek_next(iter).map(|v| v.logical_index());
    match suffix_start_idx.map(|start_idx| lex_atom::<IP>(iter, ctx, start_idx)) {
        Some(Ok(t)) => Some((suffix_start_idx.unwrap(), t)),
        _ => None,
    }
}

/// eat an atom token
fn lex_atom<IP: InternPolicy>(
    iter: &mut CppCharIt,
    ctx: &mut IP::Ctx,
    start_idx: usize,
) -> ILResult<IP::Symbol> {
    // make sure the first one is valid
    if peek_next(iter).map(|c| !is_atom_start(c.character())).unwrap_or(true) {
        return Err(ILError::InvalidAtomStartChar);
    }

    let (logical_size, phys_size, sliced) = eat_while(iter, is_atom_continue);

    if logical_size > 0 {
        let sym = IP::intern(ctx, &iter.data()[start_idx..start_idx + logical_size], start_idx);
        Ok(CppToken { kind: TokenKind::Atom(sym), size: phys_size, sliced })
    } else {
        Err(ILError::EOF)
    }
}
