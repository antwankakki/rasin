use crate::*;

/// Returns true if the given token kind is a whitespace token
pub fn is_whitespace_token<Sym: InternedRep>(kind: TokenKind<Sym>) -> bool {
    matches!(
        kind,
        TokenKind::BlockComment
            | TokenKind::LineComment
            | TokenKind::NewLine
            | TokenKind::FormFeed
            | TokenKind::Space
            | TokenKind::VerticalTab
            | TokenKind::Tab
            | TokenKind::CarriageReturn
    )
}

pub fn is_atom_token<Sym: InternedRep>(kind: TokenKind<Sym>) -> bool {
    matches!(kind, TokenKind::Atom(_))
}

pub fn is_single_char_token<Sym: InternedRep>(kind: TokenKind<Sym>) -> bool {
    matches!(kind, TokenKind::Unknown(_))
}

pub fn is_identifier_token<Sym: InternedRep>(kind: TokenKind<Sym>) -> bool {
    match kind {
        TokenKind::Unknown(c) if utils::is_atom_start(c) => true,
        TokenKind::Atom(_) => true,
        _ => false,
    }
}
