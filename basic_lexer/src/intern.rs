//! A trait defining how string interning works in Rasin's basic lexer.
//!
//! String interning is an optimization to reduce the number of copies of strings we may hold. In
//! a certain file, the same words are bound to appear many times (a class/variable/ name... etc).
//! This trait defines how to avoid keeping copies of the strings and it is made of two main types:
//!  - `Symbol`:  Used in place of a string. Think of it like a handle to get back to the String,
//!             except it is easy to copy.
//!
//!  - `Context`: Contains the mapping that turns symbols back into strings.
//!
//! There are two operations for any type implementing this trait, lookup to turn an interned
//! symbol back into its string representation, and intern to turn a string into a symbol.

use std::fmt::Debug;

/// Representation for an interned token, ideally only copy but clone is used
/// here for heavier types that dont do any real interning (like String)
pub trait InternedRep: Copy + PartialEq + Debug {}

/// Default implementation to inherit the traits for the representation
impl<T: Copy + PartialEq + Debug> InternedRep for T {}

/// The trait describing how string interning will be done
pub trait InternPolicy {
    type Symbol: InternedRep;
    type Ctx: Default;

    /// Lookup the `symbol` and map it to a str using the `context`
    fn lookup<'a>(ctx: &'a Self::Ctx, sym: &Self::Symbol) -> Option<&'a str>;

    /// Create an "interned" symbol from the string.
    fn intern(ctx: &mut Self::Ctx, val: &str, start_idx: usize) -> Self::Symbol;
}

/// Default policy is no interning, implement it for the empty tuple
impl InternPolicy for () {
    type Symbol = ();
    type Ctx = ();

    fn lookup<'a>(_: &'a Self::Ctx, _: &Self::Symbol) -> Option<&'a str> {
        None
    }

    fn intern(_: &mut Self::Ctx, _: &str, _: usize) -> Self::Symbol {}
}

/// Dumb intern policy, ties a string to its offset in a vector
pub struct VectorInternPolicy;
impl InternPolicy for VectorInternPolicy {
    /// The symbol here can be the index into the vector holding the different string copies
    type Symbol = usize;

    /// The Context is the vector holding the string copies itself
    type Ctx = Vec<String>;

    /// A lookup simply is indexing into the vector and returning a reference to the string if it
    /// is held, or None otherwise.
    fn lookup<'a>(ctx: &'a Self::Ctx, sym: &Self::Symbol) -> Option<&'a str> {
        ctx.get(*sym).map(|v| v.as_str())
    }

    /// An intern will check if a string was already inserted, if so then returning that index,
    /// otherwise push it to the end and return that iterator
    fn intern(ctx: &mut Self::Ctx, val: &str, _start_idx: usize) -> Self::Symbol {
        // check if the symbol was already interned
        if let Some(idx) = ctx.iter().position(|curr_elem| curr_elem == val) {
            idx
        } else {
            ctx.push(val.to_owned());
            ctx.len() - 1
        }
    }
}
