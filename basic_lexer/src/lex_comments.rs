use crate::intern::InternedRep;

use super::{
    cppchariter::{advance, peek_next, CppCharIt},
    CppToken, ILError, ILResult, TokenKind,
};

pub fn lex_block_comment<Sym: InternedRep>(
    iter: &mut CppCharIt,
    start_idx: usize,
) -> ILResult<Sym> {
    let slice_count = std::iter::repeat_with(|| (advance(iter), peek_next(iter)))
        .take_while(|(a, b)| match (a, b) {
            (Some(x), Some(y)) if x.character() == '*' && y.character() == '/' => false,
            (None, None) => false,
            _ => true,
        })
        .fold(0, |acc, v| v.0.map(|c| c.slice_count()).unwrap_or(0) + acc);

    // add 3 because position starts counting by 0, and it won't count the last
    // round (the last advance and peek_next) so we are off by 3 chars
    match advance(iter) {
        Some(c) => Ok(CppToken {
            kind: TokenKind::BlockComment,
            size: c.physical_index() - start_idx + 1,
            sliced: slice_count != 0,
        }),
        _ => Err(ILError::UnterminatedBlockComment),
    }
}
