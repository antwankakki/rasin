//! The entry point for the Rasin compiler.
//!
//! The driver will validate the arguments then generate a RasinArgs object containing a structured
//! cleaned up version of the arguments, this object is then used to calculate a compilation graph
//! specifying the tasks needed (preprocess, compile... etc) and then passed to the specific tasks
//! to be executed.
extern crate preprocessor;

mod cmd;

use itertools::{Either, Itertools};
use std::env;
use std::ffi::OsString;

use cmd::{
    opts::{CmdOption, FlagOpt},
    RasinArgs,
};

fn main() {
    // Get the arguments passed to the rasin, skip the invalid ones. Also skip the executable name
    let (args, bad_utf_args): (Vec<String>, Vec<(OsString, usize)>) =
        env::args_os().skip(1).enumerate().partition_map(|(idx, arg)| match arg.into_string() {
            Ok(good_utf) => Either::Left(good_utf),
            Err(bad_utf) => Either::Right((bad_utf, idx)),
        });

    for (bad_arg, idx) in bad_utf_args {
        eprint!(
            r#"note: Ignoring the argument at position {}, "{}". Invalid UTF8"#,
            idx,
            bad_arg.to_string_lossy()
        );
    }

    // Attempt to parse the arguments, print errors if any are found then exit
    match cmd::parse_args(args.iter().map(|v| v.as_str())) {
        Ok(session) => {
            //Check if the user requested to print the help message
            if session.opts.iter().any(|(_, v)| *v == CmdOption::Flag(FlagOpt::PrintHelp)) {
                println!("{}", cmd::opts::SHORT_HELP_MSG);
            } else if session.opts.iter().any(|(_, v)| *v == CmdOption::Flag(FlagOpt::PrintVersion))
            {
                println!("rasin version {}", env!("CARGO_PKG_VERSION"));
            } else {
                execute_session(session)
            }
        }
        Err(errors) => {
            eprintln!("Encountered errors:");
            for err in errors {
                eprintln!("\t{:?}", err)
            }
        }
    };
}

/// Initializes the subsystems needed to compile (diagnostics, targets... etc), build a compiler
/// instance and run it
fn execute_session(session: RasinArgs) {
    // Print group options
    println!("Source Files:             {:?}", session.source_files);

    // Print group options
    for (opt, args) in session.group_opts {
        println!("{:<26}{:?}", cmd::opts::get_group_arg_name(&opt), args);
    }

    // Print flags
    for (_, opt) in session.opts {
        if let CmdOption::Flag(flag) = opt {
            println!("{}", cmd::opts::get_flag_arg_name(&flag));
        }
    }

    // Print arguments
    for (arg, (_, opt)) in session.arguments {
        println!("{:<26}\"{}\"", cmd::opts::get_arg_name(&arg), opt);
    }
}
