#[derive(Debug, Ord, PartialOrd, Eq, PartialEq)]
pub enum CmdOption<'cmd> {
    Group(GroupOpt, &'cmd str),
    Argument(ArgOpt, &'cmd str),
    Flag(FlagOpt),
}
#[derive(Debug, Ord, PartialOrd, Eq, PartialEq)]
pub enum CmdParseError<'cmd> {
    UnknownCmd(&'cmd str),
    MissingArgument(&'cmd str),
}

#[cfg_attr(feature = "cargo-clippy", allow(non_snake_case))]
include!(concat!(env!("OUT_DIR"), "/options.rs"));
