use opts::CmdOption;
use std::collections::HashMap;

#[allow(non_snake_case, dead_code, clippy::all)]
pub mod opts;

/// List of configuration options passed to the compiler and their arguments and order.
#[derive(Debug, Eq, PartialEq)]
pub struct RasinArgs<'cmd> {
    /// List of passed source files to be compiled
    pub source_files: Vec<&'cmd str>,

    /// The list of group arguments and the index at which they appeared at
    pub group_opts: HashMap<opts::GroupOpt, Vec<(usize, &'cmd str)>>,

    /// The list of arguments that may show up only once
    pub arguments: HashMap<opts::ArgOpt, (usize, &'cmd str)>,

    /// A list of options that have no arguments
    pub opts: Vec<(usize, opts::CmdOption<'cmd>)>,
}

/// Validates the arguments passed to the iterator
///
/// # Arguments
///
/// * `args` - Iterator to a list of arguments to be parsed into valid arguments understood by the
///            the compiler.
pub fn parse_args<'cmd, CmdIter>(
    args: CmdIter,
) -> Result<RasinArgs<'cmd>, Vec<opts::CmdParseError<'cmd>>>
where
    CmdIter: Iterator<Item = &'cmd str>,
{
    let mut source_files: Vec<&'cmd str> = Vec::new();
    let mut group_opts: HashMap<opts::GroupOpt, Vec<(usize, &'cmd str)>> = HashMap::new();
    let mut arguments: HashMap<opts::ArgOpt, (usize, &'cmd str)> = HashMap::new();
    let mut cmd_parse_errors: Vec<opts::CmdParseError> = Vec::new();

    let mut raw_opts: Vec<(usize, opts::CmdOption<'cmd>)> = Vec::new();

    let mut enumerated_args = args.enumerate();
    loop {
        match enumerated_args.next() {
            Some((idx, opt)) if opt.starts_with('-') => {
                match opts::parse_opt(opt, &mut enumerated_args.by_ref().map(|(_, v)| v)) {
                    Ok(CmdOption::Group(grp, v)) => {
                        group_opts.entry(grp).or_default().push((idx, v))
                    }
                    Ok(CmdOption::Argument(arg, v)) => {
                        arguments.insert(arg, (idx, v));
                    }
                    Ok(res) => raw_opts.push((idx, res)),
                    Err(e) => cmd_parse_errors.push(e),
                }
            }
            Some((_idx, file)) => {
                source_files.push(file);
            }
            None => break,
        }
    }

    if cmd_parse_errors.is_empty() {
        Ok(RasinArgs { source_files, group_opts, arguments, opts: raw_opts })
    } else {
        // Handle errors
        Err(cmd_parse_errors)
    }
}
