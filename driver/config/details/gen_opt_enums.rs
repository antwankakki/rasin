use crate::config::Config;

/// Given a list of possible options, generate the enums that represent them
fn generate_enum<'a, I>(enum_name: &str, vals: I) -> String
where
    I: Iterator<Item = &'a str>,
{
    let mut res: Vec<String> = vec![format!(
        "#[derive(Debug, Ord, PartialOrd, Eq, Hash, PartialEq)]\npub enum {} {{",
        enum_name.to_string()
    )];
    for v in vals {
        res.push(format!("    {},", v));
    }
    res.push("}\n\n".to_string());
    res.join("\n")
}

/// process all the given options and generate corresponding enums
pub fn process(cfg: &Config) -> String {
    // translate to res
    let mut res: Vec<String> = vec![];
    if let Some(opts) = &cfg.group_opts {
        res.push(generate_enum("GroupOpt", opts.iter().map(|v| v.name.as_str())));
    }
    if let Some(opts) = &cfg.flag_opts {
        res.push(generate_enum("FlagOpt", opts.iter().map(|v| v.name.as_str())));
    }
    if let Some(opts) = &cfg.arguments {
        res.push(generate_enum("ArgOpt", opts.iter().map(|v| v.name.as_str())));
    }
    res.join("")
}
