use crate::config::{ArgOpt, Config, FlagOpt, GroupOpt};

use std::collections::BTreeMap;

/// The beginning of the matching function
const PARSE_OPT_FN_BEGIN: &str = r#"
pub fn parse_opt<'cmd, CmdIter>(opt: &'cmd str, mut args: CmdIter) -> Result<CmdOption, CmdParseError>
where
    CmdIter: Iterator<Item = &'cmd str>,
{
    match opt.as_bytes() {"#;

/// End of the matching function
const PARSE_OPT_FN_END: &str = r#"
    }
}"#;

/// Given a str, insert seperators between its chars
///   "foo", '|'   -> 'f'|'o'|'o'
fn join_str(slice: &str, sep: &str) -> String {
    let mut iter = slice.chars();
    let first = match iter.next() {
        Some(first) => first,
        None => return "".to_string(),
    };
    let size = sep.len() * slice.len() - 1;
    let mut result = Vec::with_capacity(size);
    result.push(first);

    for v in iter {
        result.extend(sep.chars());
        result.push(v);
    }
    result.iter().collect::<String>()
}

/// The differnet forms of the options that may be declared the the toml config files
pub enum OptionType<'toml> {
    /// An option that takes an argument seperate from it and may appear many times, for example -I <arg>
    SeparateFormGroup(&'toml GroupOpt),

    /// An option that takes an argument joined with it and may appear many times, for example -I<arg>
    JoinedFormGroup(&'toml GroupOpt),

    /// An option that takes an argument with both seperate and joined forms, for example -I<arg> or -I <arg>
    SeparateJoinedFormGroup(&'toml GroupOpt),

    /// A flag option that takes no arguments
    Flag(&'toml FlagOpt),

    /// An option that may take an argument and appear seperate, can only hold 1 value as opposed to group
    SeparateArgument(&'toml ArgOpt),

    /// An option that may take an argument and appear joined, can only hold 1 value as opposed to group
    JoinedArgument(&'toml ArgOpt),

    /// An option that may take an argument and appear joined or seperate, can only hold 1 value as opposed to group
    SeparateJoinedArgument(&'toml ArgOpt),
}

// Given the toml config files, generate a function that parses the argument options
pub fn process<'toml>(cfg: &'toml Config) -> String {
    // Build args
    let mut args: BTreeMap<&'toml str, OptionType> = BTreeMap::new();

    if let Some(opts) = &cfg.group_opts {
        for grp_opt in opts {
            if let Some(joined_forms) = &grp_opt.joined_forms {
                for form in joined_forms {
                    args.insert(form.as_str(), OptionType::JoinedFormGroup(grp_opt));
                }
            }
            if let Some(separate_forms) = &grp_opt.separate_forms {
                for form in separate_forms {
                    if args.contains_key(form.as_str()) {
                        args.insert(form.as_str(), OptionType::SeparateJoinedFormGroup(grp_opt));
                    } else {
                        args.insert(form.as_str(), OptionType::SeparateFormGroup(grp_opt));
                    }
                }
            }
        }
    }

    if let Some(opts) = &cfg.arguments {
        for arg_opt in opts {
            if let Some(joined_forms) = &arg_opt.joined_forms {
                for form in joined_forms {
                    args.insert(form.as_str(), OptionType::JoinedArgument(arg_opt));
                }
            }
            if let Some(separate_forms) = &arg_opt.separate_forms {
                for form in separate_forms {
                    if args.contains_key(form.as_str()) {
                        args.insert(form.as_str(), OptionType::SeparateJoinedArgument(arg_opt));
                    } else {
                        args.insert(form.as_str(), OptionType::SeparateArgument(arg_opt));
                    }
                }
            }
        }
    }

    if let Some(opts) = &cfg.flag_opts {
        for opt in opts {
            for form in &opt.forms {
                args.insert(form.as_str(), OptionType::Flag(opt));
            }
        }
    }

    // translate to res
    let mut res: Vec<String> = vec![];
    for (key, value) in args.iter() {
        let fmt = match value {
            OptionType::SeparateJoinedFormGroup(grp_opt) => {
                let match_pttrn = join_str(key, "', b'");
                let val = format!(
                    r#"
            if let Some(dir) = args.next() {{
                Ok(CmdOption::Group(GroupOpt::{}, dir))
            }} else {{
                Err(CmdParseError::MissingArgument(opt))
            }}
      "#,
                    grp_opt.name
                );

                let a = format!(
                    r#"
        [b'{}', dir @ ..] => {{
            Ok(CmdOption::Group(GroupOpt::{}, unsafe {{ std::str::from_utf8_unchecked(dir) }}))
        }}"#,
                    match_pttrn, grp_opt.name
                );
                let b = format!(
                    r#"
        b"{}" => {{  {}  }}"#,
                    key, val
                );
                format!("{},\n{}", b, a)
            }
            OptionType::SeparateFormGroup(grp_opt) => {
                let val = format!(
                    r#"
            if let Some(dir) = args.next() {{
                Ok(CmdOption::Group(GroupOpt::{}, dir))
            }} else {{
                Err(CmdParseError::MissingArgument(opt))
            }}
      "#,
                    grp_opt.name
                );

                format!(
                    r#"
        b"{}" => {{  {}  }}"#,
                    key, val
                )
            }
            OptionType::JoinedFormGroup(grp_opt) => {
                let match_pttrn = join_str(key, "', b'");
                format!(
                    r#"
        [b'{}', dir @ ..] => {{
            Ok(CmdOption::Group(GroupOpt::{}, unsafe {{ std::str::from_utf8_unchecked(dir) }}))
        }}"#,
                    match_pttrn, grp_opt.name
                )
            }

            ////////////////////////////////////////////////////////////////////////////////////////
            ////////////////////////////////////////// Arguments
            ////////////////////////////////////////////////////////////////////////////////////////
            OptionType::SeparateJoinedArgument(arg_opt) => {
                let match_pttrn = join_str(key, "', b'");
                let val = format!(
                    r#"
            if let Some(dir) = args.next() {{
                Ok(CmdOption::Argument(ArgOpt::{}, dir))
            }} else {{
                Err(CmdParseError::MissingArgument(opt))
            }}
      "#,
                    arg_opt.name
                );

                let a = format!(
                    r#"
        [b'{}', dir @ ..] => {{
            Ok(CmdOption::Group(ArgOpt::{}, unsafe {{ std::str::from_utf8_unchecked(dir) }}))
        }}"#,
                    match_pttrn, arg_opt.name
                );
                let b = format!(
                    r#"
        b"{}" => {{  {}  }}"#,
                    key, val
                );
                format!("{},\n{}", b, a)
            }
            OptionType::SeparateArgument(arg_opt) => {
                let val = format!(
                    r#"
            if let Some(dir) = args.next() {{
                Ok(CmdOption::Argument(ArgOpt::{}, dir))
            }} else {{
                Err(CmdParseError::MissingArgument(opt))
            }}
      "#,
                    arg_opt.name
                );

                format!(
                    r#"
        b"{}" => {{  {}  }}"#,
                    key, val
                )
            }
            OptionType::JoinedArgument(arg_opt) => {
                let match_pttrn = join_str(key, "', b'");
                format!(
                    r#"
        [b'{}', dir @ ..] => {{
            Ok(CmdOption::Argument(ArgOpt::{}, unsafe {{ std::str::from_utf8_unchecked(dir) }}))
        }}"#,
                    match_pttrn, arg_opt.name
                )
            }

            ////////////////////////////////////////////////////////////////////////////////////////
            ////////////////////////////////////////// Flags
            ////////////////////////////////////////////////////////////////////////////////////////
            OptionType::Flag(flag) => format!(
                r#"
        b"{}" => {{ Ok(CmdOption::Flag(FlagOpt::{})) }}"#,
                key, flag.name
            ),
        };

        res.push(format!("{},", fmt));
    }

    res.push(
        r#"
        _ => Err(CmdParseError::UnknownCmd(opt))"#
            .to_string(),
    );

    let joined_str = res.join("\n");
    [PARSE_OPT_FN_BEGIN, joined_str.as_str(), PARSE_OPT_FN_END].join("\n")
}
