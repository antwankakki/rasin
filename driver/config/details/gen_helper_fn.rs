use crate::config::Config;

/// Given a function name, enum names, and the string values that will be returned
fn generate_match_fn<'a, I>(fn_name: &str, enum_name: &str, vals: I) -> String
where
    I: Iterator<Item = (&'a str, &'a str)>,
{
    let res = vals
        .map(|s| format!("        {}::{} => \"{}\",", enum_name, s.0, s.1))
        .collect::<Vec<String>>()
        .join("\n");
    format!(
        r#"
pub fn {}(v: &{}) -> &'static str {{
    match v {{
{}
    }}
}}
"#,
        fn_name, enum_name, res
    )
}

/// Given the possible different options, generate the usage, help, and name functions to get
/// strings for the options
pub fn process(cfg: &Config) -> String {
    // translate to res
    let mut res: Vec<String> = vec![];
    if let Some(opts) = &cfg.group_opts {
        res.push(generate_match_fn(
            "get_group_help_str",
            "GroupOpt",
            opts.iter().map(|v| (v.name.as_str(), v.help.as_str())),
        ));
        res.push(generate_match_fn(
            "get_group_arg_name",
            "GroupOpt",
            opts.iter().map(|v| (v.name.as_str(), v.name.as_str())),
        ));
        res.push(generate_match_fn(
            "get_group_usage_str",
            "GroupOpt",
            opts.iter().map(|v| (v.name.as_str(), "generate usage string from forms")),
        ));
    }

    if let Some(opts) = &cfg.flag_opts {
        res.push(generate_match_fn(
            "get_flag_help_str",
            "FlagOpt",
            opts.iter().map(|v| (v.name.as_str(), v.help.as_str())),
        ));
        res.push(generate_match_fn(
            "get_flag_arg_name",
            "FlagOpt",
            opts.iter().map(|v| (v.name.as_str(), v.name.as_str())),
        ));
        res.push(generate_match_fn(
            "get_flag_usage_str",
            "FlagOpt",
            opts.iter().map(|v| (v.name.as_str(), "generate usage string from forms")),
        ));
    }

    if let Some(opts) = &cfg.arguments {
        res.push(generate_match_fn(
            "get_arg_help_str",
            "ArgOpt",
            opts.iter().map(|v| (v.name.as_str(), v.help.as_str())),
        ));
        res.push(generate_match_fn(
            "get_arg_name",
            "ArgOpt",
            opts.iter().map(|v| (v.name.as_str(), v.name.as_str())),
        ));
        res.push(generate_match_fn(
            "get_arg_usage_str",
            "ArgOpt",
            opts.iter().map(|v| (v.name.as_str(), "generate usage string from forms")),
        ));
    }

    res.join("")
}
