use crate::config::Config;
use std::borrow::{Borrow, Cow};

/// Given a list of arguments, get the first form one may use and the help message
fn gen_str<'a, I>(val_iter: I) -> String
where
    I: Iterator<Item = (&'a str, &'a str)>,
{
    let res = val_iter
        .map(|s| match s.0.len() {
            v if v < 24 => format!("  {:<24}{}", s.0, s.1),
            _ => format!("  {}\n{:^26}{}", s.0, "", s.1),
        })
        .collect::<Vec<String>>()
        .join("\n");

    format!(
        r#"pub const SHORT_HELP_MSG : &'static str = r"OVERVIEW: rasin Rasin C++ Compiler

USAGE: rasin [option] file...

OPTIONS:
{}
";"#,
        res
    )
}

/// Given the possible different options, generate a help message string to be printed by the
/// compiler
pub fn process(cfg: &Config) -> String {
    // translate to res
    let mut cmd_help = vec![];

    if let Some(opts) = &cfg.group_opts {
        for grp_opt in opts {
            if let Some(separate_forms) = &grp_opt.separate_forms {
                cmd_help.push((
                    Cow::from(format!("{}<arg>", separate_forms[0].as_str())),
                    grp_opt.help.as_str(),
                ));
            } else if let Some(joined_forms) = &grp_opt.joined_forms {
                cmd_help.push((Cow::from(joined_forms[0].as_str()), grp_opt.help.as_str()));
            }
        }
    }

    if let Some(opts) = &cfg.arguments {
        for arg_opt in opts {
            if let Some(separate_forms) = &arg_opt.separate_forms {
                cmd_help.push((
                    Cow::from(format!("{}<arg>", separate_forms[0].as_str())),
                    arg_opt.help.as_str(),
                ));
            } else if let Some(joined_forms) = &arg_opt.joined_forms {
                cmd_help.push((Cow::from(joined_forms[0].as_str()), arg_opt.help.as_str()));
            }
        }
    }

    if let Some(opts) = &cfg.flag_opts {
        for opt in opts {
            cmd_help.push((Cow::from(opt.forms[0].as_str()), opt.help.as_str()));
        }
    }

    cmd_help.sort_by(|opt_a, opt_b| {
        let trimmed = match (opt_a.0.as_bytes(), opt_b.0.as_bytes()) {
            ([b'-', b'-', a @ ..], [b'-', b'-', b @ ..]) => (a, b),
            ([b'-', b'-', a @ ..], [b'-', b @ ..]) => (a, b),
            ([b'-', a @ ..], [b'-', b'-', b @ ..]) => (a, b),
            ([b'-', a @ ..], [b'-', b @ ..]) => (a, b),
            _ => unreachable!(),
        };
        trimmed.0.partial_cmp(trimmed.1).unwrap()
    });

    gen_str(cmd_help.iter().map(|v| (v.0.borrow(), v.1)))
}
