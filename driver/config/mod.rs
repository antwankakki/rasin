//! The entry point for the Rasin compiler.
//!
//! The driver will validate the arguments then generate a RasinArgs object containing a structured
//! cleaned up version of the arguments, this object is then used to calculate a compilation graph
//! specifying the tasks needed (preprocess, compile... etc) and then passed to the specific tasks
//! to be executed.

use serde::Deserialize;

mod details;

/// Represents a group option
///
/// group options are options that can be repeated and included many times and all the values are
/// relevant. For example, there might be two interesting directories to be added to the header
/// search path, the arguments may look like this:
///
/// ```bash
/// > rasin -I<dir_a> -I<dir_b> file.cpp
/// ```
#[derive(Deserialize)]
pub struct GroupOpt {
    /// name of the option, this is how it will be accessed from rust code
    name: String,
    /// explanation of what the option is supposed to do
    help: String,
    /// The multiple joined forms this option may appear as
    joined_forms: Option<Vec<String>>,
    /// The multiple seperate forms this option may appear as
    separate_forms: Option<Vec<String>>,
}
#[derive(Deserialize)]
pub struct FlagOpt {
    /// name of the option, this is how it will be accessed from rust code
    name: String,
    /// explanation of what the option is supposed to do
    help: String,
    /// The multiple forms
    forms: Vec<String>,
}

#[derive(Deserialize)]
pub struct ArgOpt {
    /// name of the option, this is how it will be accessed from rust code
    name: String,
    /// explanation of what the option is supposed to do
    help: String,
    /// The multiple joined forms this option may appear as
    joined_forms: Option<Vec<String>>,
    /// The multiple seperate forms this option may appear as
    separate_forms: Option<Vec<String>>,
}

/// representation of the TOML options configuration file
#[derive(Deserialize)]
pub struct Config {
    #[serde(rename = "group-option")]
    group_opts: Option<Vec<GroupOpt>>,

    #[serde(rename = "flag-option")]
    flag_opts: Option<Vec<FlagOpt>>,

    #[serde(rename = "argument")]
    arguments: Option<Vec<ArgOpt>>,
}

/// given the toml file, parse it and get the raw option structures needed
pub fn process_toml(input: &str) -> Result<Config, toml::de::Error> {
    toml::from_str::<Config>(input)
}

/// generate the option parsing code
pub fn gen_src_code(cfg: &Config) -> String {
    details::gen_parse_opt::process(cfg)
}

/// generate the enums of the options
pub fn gen_enums_code(cfg: &Config) -> String {
    details::gen_opt_enums::process(cfg)
}

/// generate the helper functions that get usage, help message, or name of the options
pub fn gen_helper_fn(cfg: &Config) -> String {
    details::gen_helper_fn::process(cfg)
}

/// generat the help message to be printed by the compiler if the user asks for help
pub fn gen_help_msg_fn(cfg: &Config) -> String {
    details::gen_help_msg_fn::process(cfg)
}
