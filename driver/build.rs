extern crate serde;
extern crate toml;

use std::env;
use std::io::Write;
use std::path::PathBuf;

mod config;

#[cfg(not(target_os = "windows"))]
const MAIN_OPTS_CONFIG_TOML: &str = include_str!(concat!("config", "/main.toml"));

#[cfg(target_os = "windows")]
const MAIN_OPTS_CONFIG_TOML: &str = include_str!(concat!("config", "\\main.toml"));

fn main() -> std::io::Result<()> {
    // Tell cargo to invalidate the built crate whenever the wrapper changes
    println!("cargo:rerun-if-changed=wrapper.h");

    // Write the bindings to the $OUT_DIR/bindings.rs file.
    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap()).join("options.rs");

    let parsed_cfg = config::process_toml(MAIN_OPTS_CONFIG_TOML)?;

    let mut file = std::fs::File::create(out_path).expect("Failed to create option.rs file.");
    file.write_all(
        r#"#[allow(non_snake_case, dead_code)]
"#
        .as_bytes(),
    )?;

    file.write_all(config::gen_enums_code(&parsed_cfg).as_bytes())?;
    file.write_all(config::gen_src_code(&parsed_cfg).as_bytes())?;
    file.write_all(config::gen_helper_fn(&parsed_cfg).as_bytes())?;
    file.write_all(config::gen_help_msg_fn(&parsed_cfg).as_bytes())?;
    Ok(())
}
