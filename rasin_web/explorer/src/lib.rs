#[macro_use]
extern crate serde_derive;

use basic_lexer::lex;
use basic_lexer::CppCharIt;
use preprocessor::{PPResult, PreprocessorTokenIterator};
use wasm_bindgen::prelude::*;
use basic_lexer::intern::InternedRep;

mod result_token;
mod translate;

// Called when the wasm module is instantiated
#[wasm_bindgen(start)]
pub fn main() -> Result<(), JsValue> {
    Ok(())
}

#[wasm_bindgen]
pub fn run_basic_lexer(v: String) -> JsValue {
    if v.is_empty() {
        JsValue::from_str("Please enter some code")
    } else {
        // lex everything until we are done
        let mut lex_results = vec![];
        let mut iter = CppCharIt::new(v.as_str());
        while !iter.as_str().is_empty() {
            lex_results.push(lex::<()>(&mut iter, &mut ()));
        }

        // get the new lines:
        let new_line_indices = v
            .chars()
            .enumerate()
            .filter(|(_, c)| *c == '\n')
            .map(|(idx, _)| idx)
            .collect::<Vec<usize>>();

        // report the results
        translate::results_to_json(lex_results, new_line_indices)
    }
}

struct SizedPPResult<Sym: InternedRep> {
    pub pp_res: PPResult<Sym>,
    pub size: usize,
}

#[wasm_bindgen]
pub fn run_raw_preprocessor(v: String) -> JsValue {
    if v.is_empty() {
        JsValue::from_str("Please enter some code")
    } else {
        // lex everything until we are done
        let pp_results = PreprocessorTokenIterator::<()>::new(v.as_str())
            .sized_raw_iter()
            .map(|(pp_res, size)| SizedPPResult { pp_res, size })
            .collect::<Vec<_>>();

        // get the new lines:
        let new_line_indices = v
            .chars()
            .enumerate()
            .filter(|(_, c)| *c == '\n')
            .map(|(idx, _)| idx)
            .collect::<Vec<usize>>();

        // report the results
        translate::results_to_json(pp_results, new_line_indices)
    }
}
