#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Serialize)]
pub struct SourceLoc {
    pub line: usize,
    pub col: usize,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Serialize)]
pub struct Span {
    pub begin: SourceLoc,
    pub end: SourceLoc,
}

impl Span {
    pub fn from(source_locs: &[SourceLoc]) -> Span {
        Span { begin: source_locs[0], end: source_locs[1] }
    }
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Serialize)]
pub struct ParseResult {
    pub span: Span,
    pub msg: String,
    pub is_error: bool,
}

pub trait ResultToken {
    fn size(&self) -> usize;
    fn parse_token(&self, span: Span) -> ParseResult;
}

fn lower_bound(range: &[usize], val: usize) -> Option<usize> {
    let s = range;
    let mut size = s.len();
    if size == 0 {
        return None;
    }
    let mut base = 0usize;
    while size > 1 {
        let half = size / 2;
        let mid = base + half;
        base = if unsafe { val > *s.get_unchecked(mid) } { mid } else { base };
        size -= half;
    }
    Some(base + (unsafe { val > *s.get_unchecked(base) }) as usize)
}

impl SourceLoc {
    pub fn from(offset: usize, newlines: &[usize]) -> SourceLoc {
        let lb = lower_bound(newlines, offset);
        let last_line = newlines.iter().last();

        let (line, col) = match lb {
            None | Some(0) => (0, offset),
            Some(n) if n >= newlines.len() => (n, offset - *last_line.unwrap()),
            Some(n) if offset < newlines[n] => (n, offset - newlines.get(n - 1).unwrap_or(&0) - 1),
            Some(n) if offset == newlines[n] => (n, newlines[n]),
            Some(n) if offset > newlines[n] => (n, offset - newlines[n]),
            Some(n) if n > 0 => (n, offset - newlines[n]),
            _ => (offset, offset),
        };

        SourceLoc { line, col }
    }
}
