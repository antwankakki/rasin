use std::iter;

use crate::{
    result_token::{ParseResult, ResultToken, SourceLoc, Span},
    SizedPPResult,
};

use basic_lexer::ILResult;
use wasm_bindgen::JsValue;
use basic_lexer::intern::InternedRep;

pub fn results_to_json<T: ResultToken>(results: Vec<T>, newlines: Vec<usize>) -> JsValue {
    let source_locations = iter::once(0)
        .chain(results.iter().map(|v| v.size()))
        .scan(0_usize, |acc, x| {
            *acc += x;
            Some(*acc)
        })
        .map(|v| SourceLoc::from(v, newlines.as_slice()))
        .collect::<Vec<SourceLoc>>();

    let parse_res = results
        .iter()
        .zip(source_locations.as_slice().windows(2).map(Span::from))
        .map(|(tk, span)| tk.parse_token(span))
        .collect::<Vec<_>>();

    JsValue::from_serde(&parse_res).unwrap()
}

impl<Sym: InternedRep> ResultToken for ILResult<Sym> {
    fn size(&self) -> usize {
        self.as_ref().map(|tk| tk.size).unwrap_or(0)
    }
    fn parse_token(&self, span: Span) -> ParseResult {
        match self {
            Ok(token) => ParseResult {
                span,
                msg: format!("{:?} - {:?} - size: {:?}", token.kind, span, token.size),
                is_error: false,
            },
            Err(e) => ParseResult { span, msg: format!("{:?} - {:?}", e, span), is_error: true },
        }
    }
}

impl<Sym: InternedRep> ResultToken for SizedPPResult<Sym> {
    fn size(&self) -> usize {
        self.size
    }

    fn parse_token(&self, span: Span) -> ParseResult {
        use preprocessor::*;

        match &self.pp_res {
            Ok(PPLexOutput::Directive(dir)) => {
                ParseResult { span, msg: format!("{:?}  {:?}", dir, span), is_error: false }
            }
            Ok(PPLexOutput::Token(tk)) => ParseResult {
                span,
                msg: format!("{:?} - {:?} - size: {:?}", tk.kind, span, tk.size),
                is_error: false,
            },
            Err(e) => ParseResult { span, msg: format!("{:?} - {:?}", e, span), is_error: true },
        }
    }
}
