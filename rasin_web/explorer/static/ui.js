function setup_ui() {
    var config = {
        content: [{
            type: 'row',
            content: [
                {
                    type: 'component', componentName: 'Code'
                },
                {
                    type: 'column', content: [
                        { type: 'component', componentName: 'Internal Lexer' },
                        { type: 'component', componentName: 'Raw Preprocessor' }
                    ]
                }
            ]
        }]
    };

    var myLayout = new GoldenLayout(config);

    // Register the input component
    myLayout.registerComponent('Code', function (container, state) {

        // Create the input
        var input = $('<div id="editor" class="input_panel"> </div>');

        // Append it to the DOM
        container.getElement().append(input);
    });

    // Register the output component
    myLayout.registerComponent('Internal Lexer', function (container, componentState) {
        container.getElement().html('<div id="lexer-output" class="output_panel"></div>');
    });
    myLayout.registerComponent('Raw Preprocessor', function (container, componentState) {
        container.getElement().html('<div id="raw-pp-output" class="output_panel"></div>');
    });

    myLayout.init();
    return myLayout;
}


// Given a json version of parseResult, return a div element for it to be
// rendered
function renderParseResult(parseResult) {

    // create the element and its mouse events
    var div = document.createElement("div");

    if (parseResult.is_error === false) {
        div.className = "tokenOutput";
    }
    else {
        div.className = "tokenError";
    }

    // Create the source loc div
    var source_loc_elem = document.createElement("span");
    source_loc_elem.innerText = parseResult.span.begin.line + ':' + parseResult.span.begin.col;
    source_loc_elem.className = "tokenSourceLoc";

    // Create the data token element
    var data = document.createElement("span");
    data.innerText = parseResult.msg;
    data.className = "tokenData";

    // Add the elements to the token
    div.appendChild(source_loc_elem);
    div.appendChild(data);

    return div;
}