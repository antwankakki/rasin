#!/bin/sh

set -ex

wasm-pack build --target web
echo "Serving on 8000"
cd ./examples && cargo run --example test_server
