use std::io::Read;
use std::path::Path;

type Buffer = Vec<u8>;

// Represent the different types of buffers we have
#[allow(dead_code)]
enum BufferType {
    FileBuffer,
    ModificationBuffer,
}
// Represent an id managed by the source manager
#[allow(dead_code)]
pub struct BufferId {
    buf_type: BufferType,
    buf_idx: usize,
}
// Represents modifications that can be applied to a buffer
#[allow(dead_code)]
enum BytesModification {
    // The buffer to replace the old one with
    Replace(Vec<u8>),
    // The buffer to addTeam
    Add(Vec<u8>),
    // The number of bytes to remove
    Remove(usize),
}
// Represents a modification buffer
#[allow(dead_code)]
struct ModificationBuffer {
    // Index into the original buffer to remove the tokens
    idx: usize,
    buf_mod: BytesModification,
}
#[allow(dead_code)]
#[derive(Default)]
pub struct SourceManager {
    // The modifications applied on top of the original buffer
    modded_buffers: Vec<(BufferId, ModificationBuffer)>,
    // The original buffers read from file
    file_buffers: Vec<Buffer>,
}

impl SourceManager {
    pub fn add_file(&mut self, file_path: &Path) -> std::io::Result<BufferId> {
        // read the whole file
        let mut file = std::fs::File::open(file_path)?;
        let mut buffer: Buffer = Vec::new();
        file.read_to_end(&mut buffer)?;
        self.file_buffers.push(buffer);

        // Now we can manage it
        Ok(BufferId { buf_type: BufferType::FileBuffer, buf_idx: self.file_buffers.len() - 1 })
    }
}
