
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord)]
pub enum ParserError {
    /// Import declarations must precede all other top level declarations
    /// in the translation unit or the private module fragment.
    ImportDeclAfterTopLevelDecl {

        // The index of the previous import declaration
        preceding_decl_indx: NonZeroUsize,
    }
}