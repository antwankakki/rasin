use basic_lexer::{
    self,
    cppchariter::CppCharIt,
    intern::{InternPolicy, InternedRep},
    CppToken, ILError, ILResult, TokenKind,
};

/// Given a str into a C++ source, resolve the str into a String without any line splices
pub fn resolve_cpp_str(possibly_sliced: &str) -> String {
    CppCharIt::new(possibly_sliced).map(|v| v.character()).collect()
}

/// compare iterators
pub fn cmp_atom(input: &'static str, possibly_sliced: &str) -> bool {
    let iter = CppCharIt::new(possibly_sliced).map(|v| v.character());
    input.chars().cmp(iter) == std::cmp::Ordering::Equal
}

/// get the first non whitespace token to appear next
pub fn skip_whitespace<IP: InternPolicy>(
    iter: &mut CppCharIt,
    intern_ctx: &mut IP::Ctx,
) -> ILResult<IP::Symbol> {
    let mut lexer = std::iter::repeat_with(|| basic_lexer::lex::<IP>(iter, intern_ctx))
        .take_while(|v| v.is_ok());

    match lexer.find(|t| t.as_ref().map(|tk| !is_pp_whitespace_token(tk)).unwrap_or(false)) {
        Some(Ok(tk)) => Ok(tk),
        Some(Err(e)) => Err(e),
        None => Err(ILError::EOF),
    }
}

#[allow(clippy::type_complexity)]
pub fn tokenize_till<IP: InternPolicy>(
    limit_kind: TokenKind<IP::Symbol>,
    iter: &mut CppCharIt,
    buffer_length: usize,
    ctx: &mut IP::Ctx,
) -> Result<Vec<(usize, CppToken<IP::Symbol>)>, ILError> {
    // check if the next token
    let mut result: Vec<(usize, CppToken<IP::Symbol>)> = vec![];
    loop {
        // get the directive's start idx
        let tk_idx =
            basic_lexer::cppchariter::peek_next(iter).map_or(buffer_length, |v| v.physical_index());
        match basic_lexer::lex::<IP>(iter, ctx) {
            Ok(tk) if tk.kind == limit_kind => break,

            // we saw a token, add it to the conditional tokens
            Ok(tk) => result.push((tk_idx, tk)),

            // Encountered some other error
            Err(e) => return Err(e),
        }
    }

    Ok(result)
}

/// Returns true if the given token kind is a whitespace token, that is, whitespace
/// tokens that are not a new line
pub fn is_pp_whitespace_token<Sym: InternedRep>(tk: &CppToken<Sym>) -> bool {
    matches!(
        tk.kind,
        TokenKind::BlockComment
            | TokenKind::LineComment
            | TokenKind::FormFeed
            | TokenKind::Space
            | TokenKind::VerticalTab
            | TokenKind::Tab
            | TokenKind::CarriageReturn
    )
}

/// Checks whether a token is an atom
pub fn is_identifier_token<Sym: InternedRep>(tk: &CppToken<Sym>) -> bool {
    basic_lexer::utils::is_identifier_token(tk.kind)
}

pub fn cppcharit_to_physical_idx(iter: &CppCharIt) -> Option<usize> {
    basic_lexer::cppchariter::peek_next(iter).map(|v| v.physical_index())
}

pub fn cppcharit_to_logical_idx(iter: &CppCharIt) -> Option<usize> {
    basic_lexer::cppchariter::peek_next(iter).map(|v| v.logical_index())
}
