use crate::basic_lexer::ILError;
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord)]
pub enum PreprocessorError {
    /// The internal lexer faced an issue that was unexpected
    InternalError(ILError),

    /// An invalid token was found where a directive was expected
    /// ```C++
    /// #foo
    /// ```
    InvalidPreprocessingDirective {
        /// The invalid token idx
        invalid_token_idx: usize,
    },

    /// Expected a header name without a newline or "
    InvalidQuoteHeader,

    /// Expected a header name without a newline or >
    InvalidAngledHeader,

    /// Unterminated Conditional Block
    UnterminatedConditionalBlock {
        /// the start of the if conditional block
        if_idx: usize,

        /// the last token parsed
        last_parsed_idx: usize,
    },

    /// A conditional block was expected but non was provided (#if/#elif)
    ExpectedConditionalExpression {
        /// the start of the if conditional block
        if_idx: usize,

        /// the last token parsed
        last_parsed_idx: usize,
    },

    /// An identifier was expected for the ifdef/ifndef but none was found at this index
    MissingIfDefIdentifierAt(usize),

    /// An identifier was expected for the ifdef/ifndef but we found some other tokens
    ExpectedIfDefIdentifierAt {
        /// the index of the ifdef/ifndef directive causing the issue
        directive_idx: usize,

        /// the index of the token that was supposed to be an identifier
        bad_token_idx: usize,
    },

    /// Extra tokens were present in the ifdef/ifndef directive
    ExtraIfDefTokens {
        /// the index of the if(n)def directive causing the issue
        directive_idx: usize,

        /// the index of the extra token for the ifdef/ifndef
        bad_token_idx: usize,
    },

    /// Extra tokens were present in the else directive
    ExtraElseTokens {
        /// the index of the else directive causing the issue
        else_idx: usize,

        /// the index of the extra token for the else
        bad_token_idx: usize,
    },

    /// Extra tokens were present in the endif directive
    ExtraEndIfTokens {
        /// the index of the endif directive causing the issue
        end_idx: usize,

        /// the index of the extra token for the endif directive
        bad_token_idx: usize,
    },

    /// Expected a define identifier but none were found
    ExpectedDefineIdentifier {
        /// the index of the define directive causing the issue
        def_idx: usize,

        /// the token found where an identifier was supposed to be found
        bad_token_idx: usize,
    },

    /// If an ellipsis is found in a function macro, they must be the last one
    /// i.e  "#define F(..., X)" triggers this issue
    NonTerminalFunctionMacroEllipsis { def_idx: usize, ellipsis_idx: usize },

    /// Function macro parameters must be separated by commas
    /// i.e  "#define F(A B)" triggers this issue
    ExpectedCommaFunctionMacro {
        /// the index of the define directive causing the issue
        def_idx: usize,

        /// The index of the first identifier in the comma list
        first_ident_idx: usize,

        /// The index of the second identifier in the comma list
        second_ident_idx: usize,
    },

    /// Function macro parameters must be separated by commas
    /// i.e  "#define F(, A)" triggers this issue, so does "#define F(A, , B)"
    ExpectedIdentifierFunctionMacro {
        /// the index of the define directive causing the issue
        def_idx: usize,

        /// The index where an identifier was expected
        index_expected: usize,
    },

    /// Found a token that was not expected in the parameter list of function macros
    UnexpectedTokenFunctionMacro {
        /// the index of the define directive causing the issue
        def_idx: usize,

        /// The index of the unexpected token
        unexpected_token_idx: usize,
    },

    /// There shall be whitespace between an object like macro's identifier and its replacement list
    MissingObjectMacroIdentWhitespace {
        /// beginning of macro
        def_idx: usize,

        /// expected whitespace idx
        missing_whitespace_idx: usize,
    },

    /// __VA_ARG__ may only appear in the replacement list of a variadic function like macro
    NonVariadicMacroWithVaOpt {
        /// beginning of macro
        def_idx: usize,

        /// idx of the identifier for the macro
        ident_idx: usize,

        /// the idx of __VA_OPT__
        va_opt_idx: usize,
    },

    /// An open parenthesis must follow the __VA_OPT__ preprocessor token in a #define function
    /// macro
    VaOptExpectedOpenParen {
        /// beginning of the macro
        def_idx: usize,

        /// macro name location
        ident_idx: usize,

        /// __VA_OPT__ missing its open paren idx
        va_opt_idx: usize,

        /// Invalid token index, if no tokens were found after this will point to the end of the
        /// last token in the replacement list of the macro
        invalid_token_idx: usize,
    },

    /// __VA_OPT__ preprocessor tokens may not be nested within the token sequence inside another
    /// __VA_OPT__ preprocessor token
    NestedVaOptTokens {
        /// beginning of the macro
        def_idx: usize,

        /// macro name location
        ident_idx: usize,

        /// __VA_OPT__ missing its open paren idx
        va_opt_idx: usize,

        /// index of the token of __VA_OPT__ found within the __VA_OPT__ pointed to by va_opt_idx
        nested_va_opt_idx: usize,
    },

    /// A __VA_OPT__ must be followed by a set of balanced parenthesis, essentially, you may have
    /// parenthesis within the token sequence of __VA_OPT__ as long as we can find its end
    UnterminatedVaOptTokenSequence {
        /// beginning of the macro
        def_idx: usize,

        /// macro name location
        ident_idx: usize,

        /// __VA_OPT__ missing its close paren idx
        va_opt_idx: usize,
    },

    /// __VA_OPT__ may only appear in the replacement list of a in a variadic function like macro
    NonVariadicMacroWithVaArgs {
        /// beginning of macro
        def_idx: usize,

        /// idx of the identifier for the macro
        ident_idx: usize,

        /// the idx of __VA_ARG__
        va_arg_idx: usize,
    },

    DuplicateParameterInFunctionMacro {
        /// location of the define macro
        def_idx: usize,

        /// index to first occurrence of the macro
        first_idx: usize,

        /// index to the second occurrence of the parameter
        second_idx: usize,

        /// Number of characters to read starting from the second occurrence of the parameter
        second_ident_size: usize,
    },

    /// An **#undef** directive needs to have a name with it.
    MissingUndefDirectiveName { undef_idx: usize, line_end: usize },

    /// An **#undef** directive needs to have a name with it.
    UndefExpectedIdentifierToken {
        /// the index of the undef directive causing the issue
        undef_idx: usize,

        /// The index of the the token where an identifier was expected
        invalid_idx: usize,
    },

    /// An **#undef** directive can only have one token after it, found more
    UndefExtraTokens {
        /// the index of the undef directive causing the issue
        undef_idx: usize,

        /// The index of the the extra token
        extra_token_idx: usize,
    },

    ImportDirectiveExpectedNewline {
        /// the index of the import directive causing the issue
        directive_idx: usize,

        /// The index of the extra token, a new line was expected
        bad_token_idx: usize,
    },

    /// An export directive keyword must be followed by module or import keywords.
    ExportExpectedImportOrModule {
        /// index of the module declaration
        export_idx: usize,

        /// index of the bad token
        bad_token_idx: usize,
    },

    /// Expected to find a new line after the semi colon in a module declaration
    /// but found another token
    ModuleExpectedNewLine {
        /// index of the module declaration
        module_idx: usize,

        /// index of the bad token
        bad_token_idx: usize,
    },
}

/// Internal Lexer errors are preprocessor errors, this handy function will allow conversion from
/// the internal one to the other.
impl std::convert::From<ILError> for PreprocessorError {
    fn from(e: ILError) -> Self {
        PreprocessorError::InternalError(e)
    }
}
