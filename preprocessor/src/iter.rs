//! C++ programs are ran through a preprocessing step before compilation. Using
//! macros, one can conditionally compile sections of code, include files,
//! and reducing boilerplate.
//!
//! This macro language is defined in the specification written in the ISO/IEC
//! draft N4849. Relevant sections are 5 (Lexical Conventions) and 15
//! (Preprocessing directives).

use crate::{
    basic_lexer::{CppCharIt, ILError, TokenKind},
    error::PreprocessorError,
    ppdirectives::{lex_maybe_pp_directive, PPError, PPLexOutput, PPResult},
    utils,
};
use basic_lexer::{intern::InternPolicy, utils as ILUtils};

/// Iterator over a string of cpp constructing preprocessor tokens over the buffer
pub struct PreprocessorTokenIterator<'input, IP: InternPolicy = ()> {
    /// The iterator over cpp characters
    pub cpp_iter: CppCharIt<'input>,

    /// the main input file being processed by the
    input_buffer: &'input str,

    /// The context of the current parser
    is_beginning_of_line: bool,

    // the context for the interner
    intern_ctx: IP::Ctx,
}

impl<'input, IP: InternPolicy> PreprocessorTokenIterator<'input, IP> {
    /// construct a preprocessor iterator over the given string buffer
    pub fn new(buf: &'input str) -> PreprocessorTokenIterator<'input, IP> {
        PreprocessorTokenIterator {
            cpp_iter: CppCharIt::new(buf),
            input_buffer: buf,
            is_beginning_of_line: true,
            intern_ctx: IP::Ctx::default(),
        }
    }

    /// Lex the next preprocessor token, does not perform any preprocessing actions
    pub fn lex(&mut self) -> PPResult<IP::Symbol> {
        let next_token = basic_lexer::lex::<IP>(&mut self.cpp_iter, &mut self.intern_ctx)
            .map_err(PPError::from)?;

        // if this is the beginning of a new line, we may parse preprocessor directives
        let (result, parsed_directive) = if self.is_beginning_of_line {
            (
                lex_maybe_pp_directive::<IP>(
                    &mut self.cpp_iter,
                    next_token,
                    self.input_buffer,
                    &mut self.intern_ctx,
                ),
                true,
            )
        } else {
            // otherwise don't consider PP directives
            let _offset = utils::cppcharit_to_physical_idx(&self.cpp_iter)
                .unwrap_or(self.input_buffer.len())
                - next_token.size;
            (Ok(PPLexOutput::Token(next_token)), false)
        };

        // reset the start of line state or if we just consumed a directive
        let newline_in_next_token = match next_token.kind {
            TokenKind::NewLine => true,
            kind if ILUtils::is_whitespace_token(kind) => self.is_beginning_of_line,
            _ => false,
        };
        self.is_beginning_of_line = newline_in_next_token || parsed_directive;

        result
    }

    /// Lex the next preprocessor token, does not perform any preprocessing actions
    pub fn lex_raw_with_size(&mut self) -> (PPResult<IP::Symbol>, usize) {
        let before = utils::cppcharit_to_physical_idx(&self.cpp_iter)
            .unwrap_or(self.input_buffer.len());
        let res = self.lex();
        let after = utils::cppcharit_to_physical_idx(&self.cpp_iter)
            .unwrap_or(self.input_buffer.len());
        (res, after.saturating_sub(before))
    }

    /// Check if there are any tokens left
    pub fn is_eof(&self) -> bool {
        self.cpp_iter.clone().as_str().is_empty()
    }

    /// An iterator over the directives and tokens for the preprocessor along with their sizes
    pub fn sized_raw_iter(self) -> SizedPreprocessIterator<'input, IP> {
        SizedPreprocessIterator { pp: self }
    }
}

impl<IP: InternPolicy> Iterator for PreprocessorTokenIterator<'_, IP> {
    type Item = PPResult<IP::Symbol>;
    fn next(&mut self) -> Option<Self::Item> {
        match self.lex() {
            Ok(tk) => Some(Ok(tk)),
            Err(PreprocessorError::InternalError(ILError::EOF)) => None,
            Err(e) => Some(Err(e)),
        }
    }
}

/// An iterator of preprocessor tokens that returns raw preprocessor tokens and their sizes
pub struct SizedPreprocessIterator<'a, IP: InternPolicy> {
    pp: PreprocessorTokenIterator<'a, IP>,
}

impl<IP: InternPolicy> Iterator for SizedPreprocessIterator<'_, IP> {
    type Item = (PPResult<IP::Symbol>, usize);

    fn next(&mut self) -> Option<Self::Item> {
        match self.pp.lex_raw_with_size() {
            (Ok(tk), sz) => Some((Ok(tk), sz)),
            (Err(PreprocessorError::InternalError(ILError::EOF)), _) => None,
            (Err(e), sz) => Some((Err(e), sz)),
        }
    }
}
