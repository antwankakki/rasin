#[derive(Debug, PartialEq, Eq, PartialOrd, Ord)]
/// These don't appear in the source code or the standard, these are helper tokens to make parsing
/// The code easier to do
pub enum ControlToken {
    /// End of file was reached
    EndOfFile,

    /// End of directive was reached
    EndOfDirective,

    /// CodeCompletion marker was found
    CodeCompletionMarker,
}

/// PPTokenCategory
///
/// This is the lexer basic unit. it represents a wide range of tokens supported
/// that may become preprocessor tokens or be combined to make cpp tokens.
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord)]
pub enum PPTokenCategory {
    /// header names appear only in an include directive, a __has_include
    /// preprocessing expression, or an import preprocessing token
    HeaderName,

    /// Produced by processing an import directive
    ImportKeywordPlaceholder,

    /// Produced by processing a module directive
    ModuleKeywordPlaceholder,

    /// Produced by processing a export directive
    ExportKeywordPlaceholder,

    /// Token for the temporary place marker
    PlaceMarker,

    /// An Atom representing a __VA_ARGS__ token in preprocessing token sequence, produced when
    /// lexing replacement-lists of function-like macros.
    VaArgsKeyword,

    /// An Atom representing a __VA_ARGS__ token in preprocessing token sequence, produced when
    /// lexing replacement-lists of function-like macros.
    VaOptKeyword,

    /// An identifier may represent any of the following: a type name, variable name, a parameter
    /// to a macro, function name... etc
    Identifier,

    /// Token representing a preprocessor number
    PPNumber,

    /// Character representing an operator or a punctuator
    PPOperator,

    /// Meaningful tokens that do not of themselves specify any operation to do
    OperatorOrPunctuator,

    /// Comment token
    Comment,

    /// A whitespace char token
    WhitespaceChar,

    /// Catch all for non whitespace tokens that done fit the other categories
    NonWhitespaceChar, // except ' or ""

    /// token representing a character literal, the internal token's kind must be a literal, and
    /// the kind of the literal must be one of the character kinds
    Literals,

    /// Control tokens are tokens that dont get represented in the source but are useful for the
    /// tools processing it
    Control(ControlToken),
}
