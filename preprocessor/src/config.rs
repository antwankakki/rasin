//! Preprocessor behavior configuration module. Used to configure how the
//! the preprocessor behaves when encountering some unspecified or
//! implementation defined behavior.

/// Choose the signedness of a type (eg. when inferring extended integer type)
#[allow(dead_code)]
pub enum Signedness {
    /// Infer a signed type
    Signed,

    /// Infer an unsigned type
    Unsigned,
}

/// Choose how to truncate a value to force it to fit within a type
#[allow(dead_code)]
pub enum IntegerValueRepresentationOpts {
    /// Treating it as a byte array, drop the MSBs until the values can fit
    TruncateMostSignificant,

    /// Restrict the values to the representable range
    Clamp,

    /// Modular arithmetic
    WrapAround,
}

/// Choose how floats values are mapped to a representable value
#[allow(dead_code)]
pub enum FloatValueRepresentationOpts {
    /// Randomly choose whether to go up or down to the nearest representable
    /// value
    Random,

    /// Choose the nearest bigger value we can represent in the given type
    NearestUp,

    /// Choose the nearest smaller value we can represent in the given type
    NearestDown,
}

/// Choose how to interpret header characters
#[allow(dead_code)]
pub enum HeaderCharControl {
    /// Issue an error diagnostic upon seeing any of the characters
    Error,

    /// Treat it as if its a literal, error if it can cause a failure
    Literal,

    /// Escape the characters if possible, issue an error otherwise
    Escape,
}

/// How should line splices that appear in locations they shouldn't (raw string
/// literals) be handled
pub enum IllegalLineSliceOpts {
    /// Treat it as if there are no slices at all
    Slice,

    /// Treat it as if its part of the string
    Keep,
}

/// ImplementationBehavior
///
/// These options let the user control how the compiler behaves with respect to
/// implementation defined behavior, that is, the standard gives the
/// implementation the freedom exercise reasonable behavior as long as it
/// stays consistent with this for the lifetime of a program.
#[allow(dead_code)]
pub struct ImplementationBehavior {
    /// If a single byte char defines a value that can't be represented in a
    /// byte. For example, ''
    single_byte_char_constant_value: IntegerValueRepresentationOpts,

    /// Multibyte char representation value. For example 'AB'
    multi_char_constant_value: IntegerValueRepresentationOpts,

    /// Representation of the char in the execution character set if it
    /// can not fit in a wchar_t
    wide_16bit_char_constant_value: IntegerValueRepresentationOpts,

    /// Header names declared between angled brackets may contain any char
    /// except newline or '>'. The chars ', ", \ , /*, and // are handled in an
    /// implementation defined manager.
    angle_header_char_control: HeaderCharControl,

    /// Header names declared between quotes may contain any char
    /// except newline or '"'. The chars ', \, //, and /* are handled in an
    /// implementation defined manager.
    quote_header_char_control: HeaderCharControl,
}

/// UnspecifiedBehavior
///
/// These options let the user control how the compiler behaves with respect to
/// unspecified behavior, that is, the standard gives the implementation a
/// set of allowed behaviors it can choose as long as it stays consistent
/// with this choice for the lifetime of a program
#[allow(dead_code)]
pub struct UnspecifiedBehavior {
    /// If the integer literal can not represented in any of the allowed
    /// standard set of types and is to be represented by an extended integer
    /// type, what should the signedness of the extended type be
    integer_literal_extended_integer_type_signedness: Signedness,

    /// For a given floating point literal, its value is the nearest scaled
    /// value representable by the type. If the value is outside the range of
    /// representable values it is an ill-formed program, otherwise it is
    /// implementation defined
    float_representation_constant_value: FloatValueRepresentationOpts,

    /// If a hex digit escape sequence is found in a char literal ('\x01'),
    /// there is no limit to how many hex digits appear after it and if its
    /// value doesn't fit in the char type chosen to represent this char
    /// literal (char, char16_t...) then its result is unspecified
    hex_digit_representation_in_char: IntegerValueRepresentationOpts,
}

/// Choose how to interpret UCNs formed by the preprocessor tokenization
pub enum PPUniversalCharNameFormationOpts {
    /// Produce a multibyte char constant with the contents of the UCN
    MultibyteChar,

    /// Produce a UTF32 char constant
    UnicodeChar,
}

/// UndefinedBehavior
///
/// These options let the user control how the compiler behaves with respect to
/// undefined behavior. Which is behavior that the standard does not impose
/// any requirements on the implementations for.
#[allow(dead_code)]
pub struct UndefinedBehavior {
    /// If a character sequence that matches the syntax of a universal
    /// character name is produced by token concatenation.
    preprocessor_universal_char_name_formation: PPUniversalCharNameFormationOpts,

    /// How to treat line splices in raw string literals
    line_slice_in_raw_string_literals: IllegalLineSliceOpts,

    /// During replacement of macro invocation in macro directives, the token
    /// **defined** may be formed or an ill-formed **defined** token is formed.
    /// Replace it with this value
    defined_token_generation_from_replacement: usize,

    /// The number specified by the **#line** directive must be greater than 0 and smaller than
    /// 2147483647.
    line_control_directive_number_outside_range: IntegerValueRepresentationOpts,
}
