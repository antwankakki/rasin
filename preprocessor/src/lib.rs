//! The preprocessor is responsible for preprocessing the source code before
//! building the AST. It takes care of the first six translation phases
//! (everything up to converting the pp tokens to real tokens).
//!
//! It is separated into multiple phases as well, the basic_lexer will take raw
//! characters and convert them to basic building blocks (InternalToken) that
//! are fed to the PreprocessorTokenIterator which constructs preprocessor tokens.
//!
//! The main output of this is a vector of preprocessor tokens. Since the first
//! phases may contain implementation/unspecified behavior, such behavior is
//! exposed to the user to be configured as desired using the config module.

#![warn(missing_docs)]

// basic lexing modules
extern crate basic_lexer;
pub mod config;

mod iter;
pub use iter::PreprocessorTokenIterator;

// Preprocessor specific modules
pub(crate) mod error;

#[macro_use]
pub(crate) mod utils;

// reexport useful types
mod ppdirectives;
pub use ppdirectives::{PPDirective, PPLexOutput, PPResult};

// reexport useful types of preprocessor tokens
mod pptokens;
pub use pptokens::{ControlToken, PPTokenCategory};

/// Test module
#[cfg(test)]
mod tests;
