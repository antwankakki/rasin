use super::*;
use crate::error::PreprocessorError as PPError;
use crate::ppdirectives::{IfLine, PPDirective, PPLexOutput};

use basic_lexer::CppToken as ILToken;
use basic_lexer::TokenKind;

// Parse a simple if conditional
#[test]
pub fn test_parse_simple_conditional() {
    test_pp_token!(
        "#if HELLO\n",
        PPLexOutput::Directive(PPDirective::If(IfLine::Expression(vec!(ILToken {
            kind: TokenKind::Atom(()),
            size: 5,
            sliced: false
        }))))
    );
}

// Parse a simple if conditional
#[test]
pub fn test_parse_simple_combination() {
    test_pp_token!(
        "#if HELLO + WORLD\n",
        PPLexOutput::Directive(PPDirective::If(IfLine::Expression(vec!(
            ILToken { kind: TokenKind::Atom(()), size: 5, sliced: false },
            ILToken { kind: TokenKind::Plus, size: 1, sliced: false },
            ILToken { kind: TokenKind::Atom(()), size: 5, sliced: false }
        ))))
    );
}

// Parse a simple if conditional with spacing
#[test]
pub fn test_parse_if_leading_space_token() {
    test_pp_token!(
        "#    if HELLO\n",
        PPLexOutput::Directive(PPDirective::If(IfLine::Expression(vec!(ILToken {
            kind: TokenKind::Atom(()),
            size: 5,
            sliced: false
        }))))
    );

    test_pp_token!(
        "#    if    HELLO     \n",
        PPLexOutput::Directive(PPDirective::If(IfLine::Expression(vec!(ILToken {
            kind: TokenKind::Atom(()),
            size: 5,
            sliced: false
        }))))
    );
}

// Parse a simple ifdef
#[test]
pub fn test_parse_ifdef() {
    test_pp_token!(
        "#ifdef HELLO\n",
        PPLexOutput::Directive(PPDirective::If(IfLine::Defined(ILToken {
            kind: TokenKind::Atom(()),
            size: 5,
            sliced: false
        })))
    );

    test_pp_token!(
        "#    ifdef HELLO\n",
        PPLexOutput::Directive(PPDirective::If(IfLine::Defined(ILToken {
            kind: TokenKind::Atom(()),
            size: 5,
            sliced: false
        })))
    );

    test_pp_token!(
        "#    ifdef    HELLO     \n",
        PPLexOutput::Directive(PPDirective::If(IfLine::Defined(ILToken {
            kind: TokenKind::Atom(()),
            size: 5,
            sliced: false
        })))
    );

    test_pp_token!(
        "#    ifdef    C     \n",
        PPLexOutput::Directive(PPDirective::If(IfLine::Defined(ILToken {
            kind: TokenKind::Atom(()),
            size: 1,
            sliced: false
        })))
    );
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// Else directive

// Parse a simple else
#[test]
pub fn test_parse_else_directive() {
    test_pp_token!("#else\n", PPLexOutput::Directive(PPDirective::Else));
    test_pp_token!("#    else\n", PPLexOutput::Directive(PPDirective::Else));
    test_pp_token!(
        r#"#    else /* Hwat ehloo \
*/
"#,
        PPLexOutput::Directive(PPDirective::Else)
    );
    test_pp_token!("#    else    // HELLO     \n", PPLexOutput::Directive(PPDirective::Else));
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// Endif directive

// Parse a simple else
#[test]
pub fn test_parse_endif_directive() {
    test_pp_token!("#endif\n", PPLexOutput::Directive(PPDirective::EndIf));
    test_pp_token!("#    endif\n", PPLexOutput::Directive(PPDirective::EndIf));
    test_pp_token!(
        r#"#    endif /* ugh why am i doing this at 1 AM \
I should reconsider my life choices*/
"#,
        PPLexOutput::Directive(PPDirective::EndIf)
    );
    test_pp_token!(
        "#    endif    // LOL im so funny  \n",
        PPLexOutput::Directive(PPDirective::EndIf)
    );
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// Error Handling

// an if must be followed by a conditional expression
#[test]
pub fn test_pp_err_missing_conditional_expr() {
    test_raw_pp_err!(
        r#"#if"#,
        PPError::ExpectedConditionalExpression { if_idx: 1, last_parsed_idx: 2 }
    );

    test_raw_pp_err!(
        r#"#    if"#,
        PPError::ExpectedConditionalExpression { if_idx: 5, last_parsed_idx: 6 }
    );

    test_raw_pp_err!(
        r#"#    i\
f"#,
        PPError::ExpectedConditionalExpression { if_idx: 5, last_parsed_idx: 8 }
    );
}

// every #if must be matched by an #endif
#[test]
pub fn test_pp_err_unterminated_conditional_block() {
    test_raw_pp_err!(
        r#"#if  f"#,
        PPError::UnterminatedConditionalBlock { if_idx: 1, last_parsed_idx: 5 }
    );

    test_raw_pp_err!(
        r#"#    if  asd"#,
        PPError::UnterminatedConditionalBlock { if_idx: 5, last_parsed_idx: 11 }
    );

    test_raw_pp_err!(
        r#"#\
i\
f"#,
        PPError::ExpectedConditionalExpression { if_idx: 1, last_parsed_idx: 6 }
    );
}

// Parse ifdef with no tokens
#[test]
pub fn test_parse_ifdef_multiple_tokens() {
    test_raw_pp_err!(
        "#ifdef HELLO World\n",
        PPError::ExtraIfDefTokens { directive_idx: 1, bad_token_idx: 13 }
    );

    test_pp_token!(
        "#ifdef HELLO // World\n",
        PPLexOutput::Directive(PPDirective::If(IfLine::Defined(ILToken {
            kind: TokenKind::Atom(()),
            size: 5,
            sliced: false
        })))
    );

    test_raw_pp_err!(
        "#    ifdef World HELLO\n",
        PPError::ExtraIfDefTokens { directive_idx: 5, bad_token_idx: 17 }
    );

    test_raw_pp_err!(
        "#    ifdef    HELLO  World  \n",
        PPError::ExtraIfDefTokens { directive_idx: 5, bad_token_idx: 21 }
    );
}

// Parse ifdef with tokens
#[test]
pub fn test_parse_ifdef_no_tokens() {
    test_raw_pp_err!("#ifdef\n", PPError::MissingIfDefIdentifierAt(1));
    test_raw_pp_err!("#    ifdef\n", PPError::MissingIfDefIdentifierAt(5));
    test_raw_pp_err!("#  ifdef  \n", PPError::MissingIfDefIdentifierAt(3));
}

// Parse ifdef with tokens
#[test]
pub fn test_parse_ifdef_invalid_tokens() {
    test_raw_pp_err!(
        "#ifdef 123\n",
        PPError::ExpectedIfDefIdentifierAt { bad_token_idx: 7, directive_idx: 1 }
    );
    test_raw_pp_err!(
        "#    ifdef 0x123\n",
        PPError::ExpectedIfDefIdentifierAt { bad_token_idx: 11, directive_idx: 5 }
    );
    test_raw_pp_err!(
        "#  ifdef $ \n",
        PPError::ExpectedIfDefIdentifierAt { bad_token_idx: 9, directive_idx: 3 }
    );
}

// Parse ifdef with tokens
#[test]
pub fn test_parse_else_invalid_tokens() {
    test_raw_pp_err!("#else 123\n", PPError::ExtraElseTokens { bad_token_idx: 6, else_idx: 1 });
    test_raw_pp_err!("#else FOO\n", PPError::ExtraElseTokens { bad_token_idx: 6, else_idx: 1 });
    test_raw_pp_err!(
        "#    else 0x123\n",
        PPError::ExtraElseTokens { bad_token_idx: 10, else_idx: 5 }
    );
    test_raw_pp_err!("#  else $ \n", PPError::ExtraElseTokens { bad_token_idx: 8, else_idx: 3 });
}

// Parse ifdef with tokens
#[test]
pub fn test_parse_endif_invalid_tokens() {
    test_raw_pp_err!("#endif 123\n", PPError::ExtraEndIfTokens { bad_token_idx: 7, end_idx: 1 });
    test_raw_pp_err!("#endif FOO\n", PPError::ExtraEndIfTokens { bad_token_idx: 7, end_idx: 1 });
    test_raw_pp_err!(
        "#    endif 0x123\n",
        PPError::ExtraEndIfTokens { bad_token_idx: 11, end_idx: 5 }
    );
    test_raw_pp_err!("#  endif $ \n", PPError::ExtraEndIfTokens { bad_token_idx: 9, end_idx: 3 });
}
