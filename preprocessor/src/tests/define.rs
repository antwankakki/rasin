use super::*;
use crate::error::PreprocessorError as PPError;
use crate::ppdirectives::{DefineDirectiveMacro, PPDirective, PPLexOutput};
use basic_lexer::cppchariter::CppCharIt;

use basic_lexer::TokenKind;

////////////////////////////////////////////////////////////////////////////////////////////////////
// Object Macros

/// Instantiates a preprocessor instance with the given input and match it to the expected function
/// token
macro_rules! test_obj_macro_token {
    ($input:tt, $name:literal, $token_str:tt) => {{
        let mut pp = PreprocessorTokenIterator::<()>::new($input);

        match pp.lex() {
            Ok(PPLexOutput::Directive(PPDirective::DefineMacro(
                DefineDirectiveMacro::ObjectLike { macro_name, replacement_list },
            ))) => {
                let mut ident_iter =
                    CppCharIt::new(&$input[macro_name.0..macro_name.0 + macro_name.1.size]);
                let mut expected_iter = CppCharIt::new($name);

                loop {
                    match (ident_iter.next(), expected_iter.next()) {
                        (None, None) => break,
                        (a, b) => {
                            let equal = match (&a, &b) {
                                (&Some(ref a), &Some(ref b)) => a == b,
                                _ => false,
                            };
                            assert!(
                                equal,
                                "Mismatch {:?} == {:?}, Testing ({}) == ({})",
                                a,
                                b,
                                &$input[macro_name.0..macro_name.0 + macro_name.1.size],
                                &$name
                            );
                        }
                    }
                }

                for (idx, t) in replacement_list.iter().enumerate() {
                    assert_eq!(Some(&(t.1.size, t.1.kind)), $token_str.get(idx));
                }
            }

            Ok(v) => {
                eprintln!("{:?}", v);
                unreachable!()
            }

            Err(v) => {
                eprintln!("{:?}", v);
                unreachable!()
            }
        }
    }};
}

// Parse empty #define
#[test]
pub fn test_parse_empty_obj_macro() {
    test_obj_macro_token!("#define Foo\n", "Foo", []);
}

// Parse empty #define
#[test]
pub fn test_parse_unicode_obj_macro() {
    test_obj_macro_token!(
        r#"#de\
fine 😞 😄
"#,
        "😞",
        [(4, TokenKind::<()>::Atom(()))]
    );
}

// Parse empty #define
#[test]
pub fn test_parse_simple_obj_macro() {
    test_obj_macro_token!(
        "#define Foo X + 1\n",
        "Foo",
        [
            (1, TokenKind::<()>::Atom(())),
            (1, TokenKind::<()>::Space),
            (1, TokenKind::<()>::Plus),
            (1, TokenKind::<()>::Space),
            (1, TokenKind::<()>::PPNumber)
        ]
    );
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// Function Macros

/// Instantiates a preprocessor instance with the given input and match it to the expected function
/// token
macro_rules! test_fn_macro_token {
    ($input:tt, $fntype:tt, $name:literal, $args:tt, $tokenstr:tt) => {{
        let mut pp = PreprocessorTokenIterator::<()>::new($input);

        match pp.lex() {
            Ok(PPLexOutput::Directive(PPDirective::DefineMacro(
                DefineDirectiveMacro::$fntype { macro_name, params, replacement_list },
            ))) => {
                assert_eq!($input[macro_name.0..macro_name.0 + macro_name.1.size], *$name);

                for (idx, t) in params.iter().enumerate() {
                    assert_eq!(Some(&$input[t.0..t.0 + t.1.size]), $args.get(idx).map(|s| *s));
                }

                for (idx, t) in replacement_list.iter().enumerate() {
                    assert_eq!(Some(&t.1.kind), $tokenstr.get(idx).map(|t| t.1).as_ref());
                    assert_eq!(Some(&t.1.size), $tokenstr.get(idx).map(|t| t.0).as_ref());
                }
            }

            Ok(v) => {
                eprintln!("{:?}", v);
                unreachable!()
            }

            Err(v) => {
                eprintln!("{:?}", v);
                unreachable!()
            }
        }
    }};
}

// Parse #define without identifiers
#[test]
pub fn test_parse_empty_fn_macro() {
    test_fn_macro_token!(
        "#define Foo() X + 1\n",
        FunctionLike,
        "Foo",
        [],
        [
            (1, TokenKind::<()>::Space),
            (1, TokenKind::<()>::Atom(())),
            (1, TokenKind::<()>::Space),
            (1, TokenKind::<()>::Plus),
            (1, TokenKind::<()>::Space),
            (1, TokenKind::<()>::PPNumber)
        ]
    );
}

// Parse #define without identifiers
#[test]
pub fn test_parse_simple_fn_macro() {
    test_fn_macro_token!(
        "#define Foo(X) X + 1\n",
        FunctionLike,
        "Foo",
        ["X"],
        [
            (1, TokenKind::<()>::Space),
            (1, TokenKind::<()>::Atom(())),
            (1, TokenKind::<()>::Space),
            (1, TokenKind::<()>::Plus),
            (1, TokenKind::<()>::Space),
            (1, TokenKind::<()>::PPNumber)
        ]
    );
}

#[test]
pub fn test_parse_two_param_fn_macro() {
    test_fn_macro_token!(
        "#define BitManip(A, B) (((A) << 2) | (B))\n",
        FunctionLike,
        "BitManip",
        ["A", "B"],
        [
            (1, TokenKind::<()>::Space),
            (1, TokenKind::<()>::OpenParen),
            (1, TokenKind::<()>::OpenParen),
            (1, TokenKind::<()>::OpenParen),
            (1, TokenKind::<()>::Atom(())),
            (1, TokenKind::<()>::CloseParen),
            (1, TokenKind::<()>::Space),
            (2, TokenKind::<()>::LtLt),
            (1, TokenKind::<()>::Space),
            (1, TokenKind::<()>::PPNumber),
            (1, TokenKind::<()>::CloseParen),
            (1, TokenKind::<()>::Space),
            (1, TokenKind::<()>::Pipe),
            (1, TokenKind::<()>::Space),
            (1, TokenKind::<()>::OpenParen),
            (1, TokenKind::<()>::Atom(())),
            (1, TokenKind::<()>::CloseParen),
            (1, TokenKind::<()>::CloseParen),
        ]
    );
}

#[test]
pub fn test_parse_simple_va_args_fn_macro() {
    test_fn_macro_token!(
        "#define Hello(...) World __VA_ARGS__\n",
        VariadicFunctionLike,
        "Hello",
        ["..."],
        [
            (1, TokenKind::<()>::Space),
            (5, TokenKind::<()>::Atom(())),
            (1, TokenKind::<()>::Space),
            (11, TokenKind::<()>::Atom(())),
        ]
    );
}

#[test]
pub fn test_va_opt_spacing() {
    test_fn_macro_token!(
        "#define A(A, ...) __VA_OPT__ ()\n",
        VariadicFunctionLike,
        "A",
        ["A", "..."],
        [
            (1, TokenKind::<()>::Space),
            (10, TokenKind::<()>::Atom(())),
            (1, TokenKind::<()>::Space),
            (1, TokenKind::<()>::OpenParen),
            (1, TokenKind::<()>::CloseParen),
        ]
    );
}

#[test]
pub fn test_parse_va_args_fn_macro() {
    test_fn_macro_token!(
        "#define Hello(World, ...) World __VA_ARGS__\n",
        VariadicFunctionLike,
        "Hello",
        ["World", "..."],
        [
            (1, TokenKind::<()>::Space),
            (5, TokenKind::<()>::Atom(())),
            (1, TokenKind::<()>::Space),
            (11, TokenKind::<()>::Atom(())),
        ]
    );
}

#[test]
pub fn test_parse_fn_macro_spacing() {
    // Good function like macro
    let good_input = r#"#define FuncTest( N ) \
    return 1 + N \
}\

"#;
    test_fn_macro_token!(
        good_input,
        FunctionLike,
        "FuncTest",
        ["N"],
        [
            (7, TokenKind::<()>::Space),
            (6, TokenKind::<()>::Atom(())),
            (1, TokenKind::<()>::Space),
            (1, TokenKind::<()>::PPNumber),
            (1, TokenKind::<()>::Space),
            (1, TokenKind::<()>::Plus),
            (1, TokenKind::<()>::Space),
            (1, TokenKind::<()>::Atom(())),
            (1, TokenKind::<()>::Space),
            (1, TokenKind::<()>::CloseBrace),
            (1, TokenKind::<()>::Space)
        ]
    );

    // Invalid function like macro
    let bad_input = r#"#define FuncTest ( N ) \
    return 1 + N \
}\
"#;
    assert_ne!(
        PreprocessorTokenIterator::<()>::new(bad_input).lex(),
        PreprocessorTokenIterator::<()>::new(good_input).lex(),
    );
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// Error Handling

// Parse #define without identifiers
#[test]
pub fn test_err_parse_define_directive() {
    test_raw_pp_err!("#define", PPError::ExpectedDefineIdentifier { def_idx: 1, bad_token_idx: 6 });

    test_raw_pp_err!(
        "#define ",
        PPError::ExpectedDefineIdentifier { def_idx: 1, bad_token_idx: 7 }
    );

    test_raw_pp_err!(
        "#define 123",
        PPError::ExpectedDefineIdentifier { def_idx: 1, bad_token_idx: 8 }
    );

    test_raw_pp_err!(
        r#"#        d\
efine       #"#,
        PPError::ExpectedDefineIdentifier { def_idx: 9, bad_token_idx: 24 }
    );
}

#[test]
pub fn test_err_parse_ellipsis_fn_macro() {
    test_raw_pp_err!(
        "#define Hi(..., X) X + 1\n",
        PPError::NonTerminalFunctionMacroEllipsis { def_idx: 1, ellipsis_idx: 11 }
    );
}

#[test]
pub fn test_err_parse_sliced_ellipsis_fn_macro() {
    test_raw_pp_err!(
        r#"#define Hi(.\
.., X) X + 1\n"#,
        PPError::NonTerminalFunctionMacroEllipsis { def_idx: 1, ellipsis_idx: 11 }
    );
}

#[test]
pub fn test_err_parse_missing_comma_fn_macro() {
    test_raw_pp_err!(
        r#"#define Hi(AB YX) X + 1\n"#,
        PPError::ExpectedCommaFunctionMacro {
            def_idx: 1,
            first_ident_idx: 11,
            second_ident_idx: 14
        }
    );

    test_raw_pp_err!(
        r#"#define H\
i(A\
B X) X + 1\n"#,
        PPError::ExpectedCommaFunctionMacro {
            def_idx: 1,
            first_ident_idx: 13,
            second_ident_idx: 18
        }
    );
}

#[test]
pub fn test_err_parse_multiple_commas_fn_macro() {
    test_raw_pp_err!(
        r#"#define Hi(AB, , YX) X + 1\n"#,
        PPError::ExpectedIdentifierFunctionMacro { def_idx: 1, index_expected: 15 }
    );

    test_raw_pp_err!(
        r#"#define H\
i(A\
B,                        , X) X + 1\n"#,
        PPError::ExpectedIdentifierFunctionMacro { def_idx: 1, index_expected: 42 }
    );

    test_raw_pp_err!(
        r#"#define H\
i(, X) X + 1\n"#,
        PPError::ExpectedIdentifierFunctionMacro { def_idx: 1, index_expected: 13 }
    );

    test_raw_pp_err!(
        "#define Hi(AB, ) X + 1\n",
        PPError::ExpectedIdentifierFunctionMacro { def_idx: 1, index_expected: 13 }
    );
}

#[test]
pub fn test_err_parse_unexpected_token_fn_macro() {
    test_raw_pp_err!(
        r#"#define Hi(AB, 123, YX) X + 1\n"#,
        PPError::UnexpectedTokenFunctionMacro { def_idx: 1, unexpected_token_idx: 15 }
    );
}

#[test]
pub fn test_err_missing_undef_ident() {
    test_raw_pp_err!(
        "#undef  \n",
        PPError::MissingUndefDirectiveName { undef_idx: 1, line_end: 8 }
    );

    test_raw_pp_err!(
        "#undef  //\n",
        PPError::MissingUndefDirectiveName { undef_idx: 1, line_end: 10 }
    );

    test_raw_pp_err!(
        "#undef  /* Look Ma' I did a thing */\n",
        PPError::MissingUndefDirectiveName { undef_idx: 1, line_end: 36 }
    );
}

#[test]
pub fn test_err_undef_non_ident_tokens() {
    test_raw_pp_err!(
        "# undef 123 \n",
        PPError::UndefExpectedIdentifierToken { undef_idx: 2, invalid_idx: 8 }
    );

    test_raw_pp_err!(
        "#und\\\nef  ## //\n",
        PPError::UndefExpectedIdentifierToken { undef_idx: 1, invalid_idx: 10 }
    );

    test_raw_pp_err!(
        "#undef  /* I like to move it move it */ ^& \n",
        PPError::UndefExpectedIdentifierToken { undef_idx: 1, invalid_idx: 40 }
    );
}

#[test]
pub fn test_err_undef_extra_tokens() {
    test_raw_pp_err!(
        "# \\\nundef I\\\nIII  Should get some other text for my tests \n",
        PPError::UndefExtraTokens { undef_idx: 2, extra_token_idx: 18 }
    );

    test_raw_pp_err!(
        "#und\\\nef  Russel Brand //\n",
        PPError::UndefExtraTokens { undef_idx: 1, extra_token_idx: 17 }
    );

    test_raw_pp_err!(
        "#undef  Foo/* I like to move it move it */Bar  \n",
        PPError::UndefExtraTokens { undef_idx: 1, extra_token_idx: 42 }
    );
}

#[test]
pub fn test_err_va_opt_in_non_var_macro() {
    test_fn_macro_token!(
        "#define    Foo(A, ...)   __VA_OPT__()\n",
        VariadicFunctionLike,
        "Foo",
        ["A", "..."],
        [
            (3, TokenKind::<()>::Space),
            (10, TokenKind::<()>::Atom(())),
            (1, TokenKind::<()>::OpenParen),
            (1, TokenKind::<()>::CloseParen)
        ]
    );

    test_raw_pp_err!(
        "#define Foo(A) __VA_OPT__()\n",
        PPError::NonVariadicMacroWithVaOpt { def_idx: 1, ident_idx: 8, va_opt_idx: 15 }
    );
}

#[test]
pub fn test_err_va_arg_in_non_var_macro() {
    test_fn_macro_token!(
        "#define    Foo(A, ...)        __VA_ARGS__\n",
        VariadicFunctionLike,
        "Foo",
        ["A", "..."],
        [(8, TokenKind::<()>::Space), (11, TokenKind::<()>::Atom(()))]
    );

    test_raw_pp_err!(
        "#define    Foo(A)        __VA_ARGS__\n",
        PPError::NonVariadicMacroWithVaArgs { def_idx: 1, ident_idx: 11, va_arg_idx: 25 }
    );
}

#[test]
pub fn test_err_slice_va_arg_in_non_var_macro() {
    test_raw_pp_err!(
        "#define Foo(A)  __VA_A\\\nRGS__\n",
        PPError::NonVariadicMacroWithVaArgs { def_idx: 1, ident_idx: 8, va_arg_idx: 16 }
    );
}

#[test]
pub fn test_err_missing_obj_macro_ident_whitespace() {
    test_raw_pp_err!(
        "#define Foo)\\\n",
        PPError::MissingObjectMacroIdentWhitespace { def_idx: 1, missing_whitespace_idx: 12 }
    );
}

// a function like macro cant have duplicates in its argument
#[test]
pub fn test_err_duplicate_parameters_in_fn() {
    test_raw_pp_err!(
        "#define Foo(A, A) 1\n",
        PPError::DuplicateParameterInFunctionMacro {
            def_idx: 1,
            first_idx: 12,
            second_idx: 15,
            second_ident_size: 1
        }
    );

    test_raw_pp_err!(
        "#define Foo(AA, A\\\nA) 1\n",
        PPError::DuplicateParameterInFunctionMacro {
            def_idx: 1,
            first_idx: 12,
            second_idx: 16,
            second_ident_size: 4
        }
    );
}

// Can't allow any non-standard define directives
#[test]
pub fn test_err_invalid_pp_directive() {
    test_raw_pp_err!("#? \n", PPError::InvalidPreprocessingDirective { invalid_token_idx: 1 });

    test_raw_pp_err!("#   A \n", PPError::InvalidPreprocessingDirective { invalid_token_idx: 4 });

    test_raw_pp_err!(
        "#   \\\nA \n",
        PPError::InvalidPreprocessingDirective { invalid_token_idx: 4 }
    );
}

// Can't allow any non-standard define directives
#[test]
pub fn test_err_va_opt_missing_open_paren() {
    test_raw_pp_err!(
        "#define A(...) __VA_OPT__\n",
        PPError::VaOptExpectedOpenParen {
            def_idx: 1,
            ident_idx: 8,
            va_opt_idx: 15,
            invalid_token_idx: 25
        }
    );

    test_raw_pp_err!(
        "#define A(...) __VA_OPT__() __VA_OPT__\n",
        PPError::VaOptExpectedOpenParen {
            def_idx: 1,
            ident_idx: 8,
            va_opt_idx: 28,
            invalid_token_idx: 38
        }
    );

    test_raw_pp_err!(
        "#define A(...) __VA_OPT__() __VA_OPT__)\n",
        PPError::VaOptExpectedOpenParen {
            def_idx: 1,
            ident_idx: 8,
            va_opt_idx: 28,
            invalid_token_idx: 38
        }
    );

    test_raw_pp_err!(
        "#define A(...) __VA_OPT__() __VA_OPT__ asdasd\n",
        PPError::VaOptExpectedOpenParen {
            def_idx: 1,
            ident_idx: 8,
            va_opt_idx: 28,
            invalid_token_idx: 39
        }
    );
}

// Do not allow for unterminated VA_OPTs
#[test]
pub fn test_err_unterminated_va_opts() {
    test_raw_pp_err!(
        "#define A(...) __VA_OPT__(\n",
        PPError::UnterminatedVaOptTokenSequence { def_idx: 1, ident_idx: 8, va_opt_idx: 15 }
    );

    test_raw_pp_err!(
        "#define A(...)  __VA_OPT__ ((\n",
        PPError::UnterminatedVaOptTokenSequence { def_idx: 1, ident_idx: 8, va_opt_idx: 16 }
    );

    test_raw_pp_err!(
        "#define A(...) __VA_OPT__(()\n",
        PPError::UnterminatedVaOptTokenSequence { def_idx: 1, ident_idx: 8, va_opt_idx: 15 }
    );
}

// VA_OPTs can't nest
#[test]
pub fn test_err_nested_va_opts() {
    test_raw_pp_err!(
        "#define A(...) __VA_OPT__(__VA_OPT__())\n",
        PPError::NestedVaOptTokens {
            def_idx: 1,
            ident_idx: 8,
            va_opt_idx: 15,
            nested_va_opt_idx: 26
        }
    );

    test_raw_pp_err!(
        "#define A(...) __VA_OPT__(__VA_OPT__(abc) + a )\n",
        PPError::NestedVaOptTokens {
            def_idx: 1,
            ident_idx: 8,
            va_opt_idx: 15,
            nested_va_opt_idx: 26
        }
    );

    test_raw_pp_err!(
        "#define A(...) __VA_OPT__(A + __VA_OPT__())\n",
        PPError::NestedVaOptTokens {
            def_idx: 1,
            ident_idx: 8,
            va_opt_idx: 15,
            nested_va_opt_idx: 30
        }
    );

    test_raw_pp_err!(
        "#define A(...) __VA_OPT__(A + __VA_OPT__() + Z)\n",
        PPError::NestedVaOptTokens {
            def_idx: 1,
            ident_idx: 8,
            va_opt_idx: 15,
            nested_va_opt_idx: 30
        }
    );
}
