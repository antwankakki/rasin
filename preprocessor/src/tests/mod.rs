use super::*;

#[macro_use]
mod test_utils;
mod define;
mod if_section;
mod modules;

use crate::ppdirectives::*;

use basic_lexer::{CppToken, LiteralKind, TokenKind};

// helper to make the tests less verbose
fn internal_token(size: usize, kind: TokenKind<()>) -> PPLexOutput<()> {
    PPLexOutput::Token(CppToken { kind, size, sliced: false })
}

fn ident_token(_offset: usize, size: usize) -> PPLexOutput<()> {
    PPLexOutput::Token(CppToken { kind: TokenKind::Atom(()), sliced: false, size })
}

// ill-formed raw string, not "x" "y"
#[test]
fn test_err_define_directive_str_concat() {
    let input = r#"#define R "x"
const char* s = R"y";"#;

    test_raw_pp_tokens!(
        input,
        PPLexOutput::Directive(PPDirective::DefineMacro(
            ppdirectives::DefineDirectiveMacro::ObjectLike {
                macro_name: (8, CppToken { kind: TokenKind::Atom(()), size: 1, sliced: false }),
                replacement_list: vec![(
                    10,
                    CppToken {
                        kind: TokenKind::Literal {
                            kind: LiteralKind::OrdinaryString,
                            start_offset: 11,
                            suffix_start_idx: None
                        },
                        size: 3,
                        sliced: false
                    }
                )]
            }
        ))
    );
}

// ill-formed raw string, not "x" "y"
#[test]
fn test_define_numeric_exponent() {
    let input = r#"#define E 1
1E1 //should result in a floating-point literal"#;

    test_raw_pp_tokens!(
        input,
        PPLexOutput::Directive(PPDirective::DefineMacro(
            ppdirectives::DefineDirectiveMacro::ObjectLike {
                macro_name: (8, CppToken { kind: TokenKind::Atom(()), size: 1, sliced: false }),
                replacement_list: vec![(
                    10,
                    CppToken { kind: TokenKind::PPNumber, size: 1, sliced: false }
                )]
            }
        ))
    );
}

// Sadly, this pattern is a PPNumber, which is an invalid integer literal, even
// though it would be an ok real token as 0xP + foo
#[test]
fn test_unfortunate_pp_number() {
    test_pp_token!("0xP+foo", internal_token(7, TokenKind::PPNumber));
}

#[test]
fn test_parse_simple_combo() {
    // #define E 1
    // 1E1         should result in a floating-point literal
    let combined_input = r#"#ifndef X
int main() {
}
#endif
"#;

    test_raw_pp_tokens!(
        combined_input,
        PPLexOutput::Directive(PPDirective::If(IfLine::NotDefined(CppToken {
            kind: TokenKind::Atom(()),
            size: 1,
            sliced: false
        }))),
        ident_token(10, 3),
        internal_token(1, TokenKind::Space),
        ident_token(14, 4),
        internal_token(1, TokenKind::OpenParen),
        internal_token(1, TokenKind::CloseParen),
        internal_token(1, TokenKind::Space),
        internal_token(1, TokenKind::OpenBrace),
        internal_token(1, TokenKind::NewLine),
        internal_token(1, TokenKind::CloseBrace),
        internal_token(1, TokenKind::NewLine),
        PPLexOutput::Directive(PPDirective::EndIf)
    );
}

// Test for invalid preprocessing directives
#[test]
fn test_err_invalid_pp_directive() {
    test_raw_pp_err!("#? ", PPError::InvalidPreprocessingDirective { invalid_token_idx: 1 });

    test_raw_pp_err!("#Never", PPError::InvalidPreprocessingDirective { invalid_token_idx: 1 });

    test_raw_pp_err!("#    Gonna", PPError::InvalidPreprocessingDirective { invalid_token_idx: 5 });

    test_raw_pp_err!(
        "#    \\\n  Give\\\nYou \\\nUp",
        PPError::InvalidPreprocessingDirective { invalid_token_idx: 9 }
    );
}
