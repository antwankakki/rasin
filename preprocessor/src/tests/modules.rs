use crate::ppdirectives::{PPDirective, PPLexOutput};
use basic_lexer::{CppToken, LiteralKind, TokenKind};

// A # directive introducing token at the beginning of the line is good
#[test]
pub fn test_null_directive() {
    test_pp_token!("#\n", PPLexOutput::Directive(PPDirective::Null));
}

// A module directive introducing token at the beginning of the line is good
#[test]
pub fn test_module_directive_introducing_token() {
    test_pp_token!(
        "module ;\n",
        PPLexOutput::Directive(PPDirective::Module { is_export: false, tokens: vec![] })
    );

    test_pp_token!(
        "export module leftpad;\n",
        PPLexOutput::Directive(PPDirective::Module {
            is_export: true,
            tokens: vec![(14, CppToken { kind: TokenKind::Atom(()), size: 7, sliced: false })]
        })
    );

    let spliced = r#"module \
;
"#;
    test_pp_token!(
        spliced,
        PPLexOutput::Directive(PPDirective::Module { is_export: false, tokens: vec![] })
    );
}

// A import directive introducing token at the beginning of the line is good
#[test]
pub fn test_import_directive_introducing_token() {
    test_pp_token!(
        "import <string>;\n",
        PPLexOutput::Directive(PPDirective::Import {
            directive_idx: 0,
            is_export: false,
            tokens: vec![
                (6, CppToken { kind: TokenKind::Space, size: 1, sliced: false }),
                (7, CppToken { kind: TokenKind::Lt, size: 1, sliced: false }),
                (8, CppToken { kind: TokenKind::Atom(()), size: 6, sliced: false }),
                (14, CppToken { kind: TokenKind::Gt, size: 1, sliced: false })
            ]
        })
    );

    test_pp_token!(
        "export import \"squee\";\n",
        PPLexOutput::Directive(PPDirective::Import {
            directive_idx: 7,
            is_export: true,
            tokens: vec![
                (13, CppToken { kind: TokenKind::Space, size: 1, sliced: false }),
                (
                    14,
                    CppToken {
                        kind: TokenKind::Literal {
                            kind: LiteralKind::OrdinaryString,
                            start_offset: 15,
                            suffix_start_idx: None
                        },
                        size: 7,
                        sliced: false
                    }
                )
            ]
        })
    );
    test_pp_token!(
        "import rightpad;\n",
        PPLexOutput::Directive(PPDirective::Import {
            directive_idx: 0,
            is_export: false,
            tokens: vec![
                (6, CppToken { kind: TokenKind::Space, size: 1, sliced: false }),
                (7, CppToken { kind: TokenKind::Atom(()), size: 8, sliced: false })
            ]
        })
    );
    test_pp_token!(
        "import :part;\n",
        PPLexOutput::Directive(PPDirective::Import {
            directive_idx: 0,
            is_export: false,
            tokens: vec![
                (6, CppToken { kind: TokenKind::Space, size: 1, sliced: false }),
                (7, CppToken { kind: TokenKind::Colon, size: 1, sliced: false }),
                (8, CppToken { kind: TokenKind::Atom(()), size: 4, sliced: false })
            ]
        })
    );
}

// Not pp directives

// A import directive introducing token at the beginning of the line is good
#[test]
pub fn test_err_sliced_non_module_directive() {
    let input = r#"module
;
"#;

    test_pp_token_neq!(input, PPLexOutput::Directive(PPDirective::Null));
}

// A import directive introducing token at the beginning of the line is good
#[test]
pub fn test_err_directive_macro_expansion() {
    let input = r#"#define EMPTY
EMPTY   #   include <file.h>
"#;

    test_pp_token_neq!(input, PPLexOutput::Directive(PPDirective::Null));
}

// A import directive introducing token at the beginning of the line is good
#[test]
pub fn test_err_non_directive_export() {
    let input = r#"export
import
foo;
"#;
    test_pp_token_neq!(input, PPLexOutput::Directive(PPDirective::Null));

    let input2 = r#"export
import foo;
"#;

    test_pp_token_neq!(input2, PPLexOutput::Directive(PPDirective::Null));
}

// A import directive introducing token at the beginning of the line is good
#[test]
pub fn test_err_non_directive_import() {
    let input = "import ::\n";
    test_pp_token_neq!(input, PPLexOutput::Directive(PPDirective::Null));

    let input2 = "import ->\n";
    test_pp_token_neq!(input2, PPLexOutput::Directive(PPDirective::Null));
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// Error Handling
