/// Instantiates a preprocessor instance with the given input and lexes the
/// first token it can. The token can be a PPDirective or a PPToken.
macro_rules! test_pp_token {
    ($input:tt, $expected_token:expr) => {{
        let mut pp = crate::PreprocessorTokenIterator::<()>::new($input);
        assert_eq!(pp.lex(), Ok($expected_token));
    }};
}

/// Instantiates a preprocessor instance with the given input and lexes the
/// first token it can. Expecting a preprocessor error.
macro_rules! test_raw_pp_err {
    ($input:tt, $expected_err:expr) => {{
        let mut pp = crate::PreprocessorTokenIterator::<()>::new($input);
        assert_eq!(pp.lex(), Err($expected_err));
    }};
}

macro_rules! test_pp_token_neq {
    ($input:tt, $expected_token:expr) => {{
        let mut preprocessor = crate::PreprocessorTokenIterator::<()>::new($input);
        assert_ne!(preprocessor.lex(), Ok($expected_token));
    }};
}

macro_rules! test_raw_pp_tokens {
    ($input:tt, $( $x:expr ),*) => {{
        let mut preprocessor = crate::PreprocessorTokenIterator::<()>::new($input);
        $(
            assert_eq!(preprocessor.lex(), Ok($x));
        )*
    }};
}
