//! Directives are sequences of preprocessing tokens that start with a # at
//! the beginning of a line (or "import"/"export import") and end at a non
//! escaped newline.

use crate::{
    basic_lexer::{self, CppToken, ILError, TokenKind},
    pptokens::ControlToken,
    utils::cppcharit_to_logical_idx,
    utils::cppcharit_to_physical_idx,
    PPTokenCategory,
};
use crate::{
    error::PreprocessorError,
    utils::{
        cmp_atom, is_identifier_token, is_pp_whitespace_token, resolve_cpp_str, skip_whitespace,
        tokenize_till,
    },
};
use basic_lexer::{
    intern::{InternPolicy, InternedRep},
    CppCharIt,
};
use std::collections::HashMap;

/// Type alias representing an internal lexer error
pub type PPError = PreprocessorError;

// Special in certain contexts
const PP_DIRECTIVE_DEFINE: &str = "define";
const PP_DIRECTIVE_ELIF: &str = "elif";
const PP_DIRECTIVE_ELSE: &str = "else";
const PP_DIRECTIVE_ENDIF: &str = "endif";
const PP_DIRECTIVE_ERROR: &str = "error";
const PP_DIRECTIVE_IF: &str = "if";
const PP_DIRECTIVE_IFDEF: &str = "ifdef";
const PP_DIRECTIVE_IFNDEF: &str = "ifndef";
const PP_DIRECTIVE_INCLUDE: &str = "include";
const PP_DIRECTIVE_LINE: &str = "line";
const PP_DIRECTIVE_PRAGMA: &str = "pragma";
const PP_DIRECTIVE_UNDEF: &str = "undef";
const PP_DIRECTIVE_IMPORT: &str = "import";
const PP_DIRECTIVE_MODULE: &str = "module";
const PP_DIRECTIVE_EXPORT: &str = "export";
const VA_OPT_IDENT: &str = "__VA_OPT__";
const VA_ARGS_IDENT: &str = "__VA_ARGS__";

/// Represents the result of a preprocessor lex call without execution
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord)]
pub enum PPLexOutput<Sym: InternedRep> {
    /// The result was a directive
    Directive(PPDirective<Sym>),

    /// The result was a preprocessing token
    Token(CppToken<Sym>),
}

/// The lexer token or an error
pub type PPResult<Sym> = Result<PPLexOutput<Sym>, PPError>;

/// The result of a directive parse
pub type PPDirectiveResult<Sym> = Result<PPDirective<Sym>, PPError>;

// Parsed in this file

/// **#define** directives are the parts of the preprocessor that handle macro
/// replacement, that is, it takes an *identifier* and replaces with another
/// sequence of tokens.
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord)]
pub enum DefineDirectiveMacro<Sym: InternedRep> {
    /// Replaces the occurrence of the identifier with the (possibly empty)
    /// sequence of tokens after it. The replacement list is rescanned for
    /// more macro names as specified by [cpp.replace]
    ///
    /// Note that a macro that doesn't contain any replacement tokens simply
    /// removes the occurrence of the identifier from the token sequences (it
    /// still remains in comments and and literals), it also can be used in
    /// conditional directives.
    ///
    /// # Syntax
    /// * **# define ** *identifier* *pp-tokens* *new-line*
    ObjectLike {
        /// The identifier to be replaced
        macro_name: (usize, CppToken<Sym>),

        /// The tokens to replace it with (note that the whitespace between the
        /// identifier and the token-list, as well as the last non-whitespace
        /// token and the newline after the directive are not considered part of
        /// the replacement).
        replacement_list: Vec<(usize, CppToken<Sym>)>,
    },

    /// Accept an identifier and parameters to it and replaces the occurrence
    /// of each of the identifiers in the pp-tokens after it.
    ///
    /// # Syntax
    /// * **# define ** *identifier* *pp-tokens* *new-line*
    FunctionLike {
        /// The identifier to be replaced
        macro_name: (usize, CppToken<Sym>),

        /// The (optional) parameters to the macro
        params: Vec<(usize, CppToken<Sym>)>,

        /// The tokens to replace it with
        replacement_list: Vec<(usize, CppToken<Sym>)>,
    },

    /// Accept an identifier and parameters to it and replaces the occurrence
    /// of each of the identifiers in the pp-tokens after it. Also accepts a
    /// a variable number of arguments at the end.
    ///
    /// Note that you can pass three ellipses for **...***
    ///
    /// # Syntax
    /// * **# define ** *identifier* *pp-tokens* *new-line*
    VariadicFunctionLike {
        /// The identifier to be replaced
        macro_name: (usize, CppToken<Sym>),

        /// The (optional) parameters to the macro. The scope of the parameters
        /// extends to the next logical source line
        params: Vec<(usize, CppToken<Sym>)>,

        /// The tokens to replace it with
        replacement_list: Vec<(usize, CppToken<Sym>)>,
    },
}

/// Conditional directive may start with **#if* *, **#ifdef** (**#if defined**
/// *identifier*), and **#ifndef** (**#if !defined** *identifier*). Conditional
/// directives control whether the preprocessor will consider the following
/// lines until a terminal/alternative conditional directive is seen.
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord)]
pub enum IfLine<Sym: InternedRep> {
    /// Evaluates the constant expression formed after the **#if** that
    /// introduces it. The expression is interpreted as an *integral constant
    /// expression* with the following  happening before:
    ///  * An *identifier* (whether or not it is a keyword, enumeration
    ///    constant... etc) evaluate to 1 if its a macro name that has been
    ///    defined (with **#define** or predefined by the compiler) and has not
    ///    been undefined.
    ///  * An *identifier* that has no active definitions evaluates to 0.
    ///  * A *has-include-expression* evaluates to 1 if a search for the header
    ///    it specifies succeeds, 0 otherwise. No further processing is done
    ///  * A *has-attribute-expression* is replaced by a non-zero
    ///   *integer-literal* if supported, 0 otherwise.
    ///  * if macro invocation replacement generates **defined**, it causes
    ///    undefined behavior.
    ///  * After the preprocessor runs on the preprocessing tokens, any
    ///    identifiers and keywords (except **true** and **false**) are replaced
    ///    with 0.
    ///
    /// # Syntax
    /// * **#if** constant-expression
    Expression(Vec<CppToken<Sym>>),

    /// Equivalent to **#if defined** identifier. Evaluates to true if the
    /// identifier has been defined before with a **#define** directive.
    /// Note that if the identifier is **__has_include** or
    /// **__has_cpp_attribute**, they are treated as if they were names of
    /// defined macros.
    ///
    /// # Syntax
    /// * **#ifdef** *identifier*
    Defined(CppToken<Sym>),

    /// Equivalent to **#if !defined** *identifier*. Evaluates to true if the
    /// identifier has been defined before with a *#define* directive.
    ///
    /// # Syntax
    /// * **#ifndef** *identifier*
    NotDefined(CppToken<Sym>),
}

/// Preprocessing directives are a sequence of preprocessing tokens that follow
/// a directive-introducing token (#/import/module/export import/export module)
/// must be at the beginning of a line (skipping whitespace)
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord)]
pub enum PPDirective<Sym: InternedRep> {
    /// Represents the beginning of a conditional directive, could be
    /// **#if**, **#ifdef**, or an **#ifndef**.
    ///
    /// These may be nested and must end with a **#endif** directive. There may be
    /// **#else** or **#elif** blocks associated with these directives. They end up
    /// selecting a group part to be further processed by the preprocessor and
    /// passed along to the compiler
    If(IfLine<Sym>),

    ///
    Elif(Vec<CppToken<Sym>>),

    /// Else conditional directive. Appears following **#if**, **#ifdef**, or **#elif** and
    /// before **#endif** directive. The preprocessor enters this block if all the
    /// *constant-expression* terms are false.
    Else,

    /// **#endif** directives are the last directive parsed in a set of conditional
    /// directives starting with an **#if** or **#ifdef**.
    EndIf,

    /// A **#define** directive to help with macro replacement.
    DefineMacro(DefineDirectiveMacro<Sym>),

    /// A **#undef** directive to help with macro replacement.
    ///
    /// **Syntax**:
    ///  * **#undef** *identifier*
    UndefineMacro(CppToken<Sym>),

    /// A **defined-macro-expression** evaluates to 1 if the identifier is defined
    /// (**#define** called and no **#undef** was followed), 0 otherwise.
    ///
    /// **Syntax**:
    ///  * **defined** *identifier*
    ///  * **defined** ( *identifier* )
    DefinedTestMacro(CppToken<Sym>),

    /// Checks for the presence of an attribute named by attribute-token (after
    /// macro expansion). Evaluates to 1 if so, 0 otherwise.
    ///
    /// **Syntax**:
    ///  * **__has_cpp_attribute(** *attribute-token **)**
    ///
    /// **Notes**:
    /// * The implementation may add any attributes and assigns any value.
    /// * The following are defined by the standard and must hold these values:
    ///  | Attribute | Value |
    ///  |-|-|
    ///  | carries-dependency | 200809L
    ///  | deprecated | 201309L
    ///  | fallthrough | 201603L
    ///  | likely | 201803L
    ///  | maybe_unused | 201603L
    ///  | no_unique_address | 201803L
    ///  | nodiscard | 201907L
    ///  | noreturn | 200809L
    ///  | unlikely | 201803L
    FeatureTest(Vec<CppToken<Sym>>),

    /// A constant expression that evaluates to 1 if the filename is found and
    /// ​0​ if not.
    ///
    /// **Syntax**:
    ///  * **__has_include ( "** *filename* **" )**
    ///  * **__has_include ( <** *filename* **> )**
    ///
    /// **Notes**:
    /// * Exhibits ill-formed behavior if the filename does not exist
    HasInclude(Vec<CppToken<Sym>>),

    /// Declare the current translation unit is a module unit. From the
    /// perspective of the preprocessor, this simply gets replaced with a
    /// module declaration, the pp-tokens are preprocessed as usual. this
    /// directive does not work if module/export are defined as a macro.
    ///
    /// **Syntax**:
    ///  * **export module** *pp-tokens* **;** *newline*
    Module {
        /// whether this directive has the export keyword
        is_export: bool,

        /// the start of the module pp-tokens
        tokens: Vec<(usize, CppToken<Sym>)>,
    },

    ///
    Import {
        ///Start index of this directive
        directive_idx: usize,

        /// whether this directive has the export keyword
        is_export: bool,

        /// These tokens are processed as normal text. The format must match one of these forms,
        /// note that these tokens do not include the trailing *semi-colon* and the *newline*.
        ///  * **import** *header-name* **;** *new-line*
        ///  * **import** *header-name* *pp-tokens* **;** *new-line*
        ///  * **export import** *header-name* **;** *new-line*
        ///  * **export import** *header-name* *pp-tokens* **;** *new-line*
        tokens: Vec<(usize, CppToken<Sym>)>,
    },

    /// Request to produce a translation error and print the message as
    /// requested by the tokens (tokens are not subject to macro replacement)
    ///
    /// **Syntax**:
    ///  * **#error** *pp-tokens* **;** *newline*
    Error {
        /// The starting index for the error directive
        error_idx: usize,

        /// the list of tokens to be processed for the error message
        tokens: Vec<(usize, CppToken<Sym>)>,
    },

    /// The null directive, does not do anything.
    ///
    /// **Syntax**:
    ///  * **#**
    Null,

    /// Changes the current line number(and filename) from the perspective of the compiler.
    /// The following lines will be treated as an offset from this point in the source code. There
    /// are two forms this directive may look like after replacements, either
    /// * **#line** *digit-sequence newline* which only modifies the line number
    /// * **#line** *digit-sequence* **"** *filename* **"** *newline* which modifies both the line
    ///  number and the filename
    ///
    /// The digit sequence must be specify a number n such that 0 < n < 2147483647, otherwise it is
    /// undefined behavior.
    ///
    /// **Syntax*
    ///  *  **#line** *pp-tokens newline*
    Line(Vec<(usize, CppToken<Sym>)>),

    /// Pragma directives specify the behavior of the compiler. Everything about this is
    /// implementation defined
    ///
    /// **Syntax*
    ///  *  **#pragma** *pp-tokens newline*
    Pragma(Vec<(usize, CppToken<Sym>)>),

    /// Include the contents of the specified file as if it appeared at this point of the file.
    /// There might be two forms of this:
    ///  * **#include** **"** *char-sequence* specifying the path to file **"**
    ///  * **#include** **<** *char-sequence* specifying the path to file **>**
    ///
    /// The first form looks for files relative to the current file and after
    /// that through a set of directories from the preprocessor configuration.
    ///
    /// The second form is used generally for system files by the system
    /// search path.
    ///
    /// **Syntax*
    ///  *  **#include** *pp-tokens newline*
    Include(Vec<(usize, CppToken<Sym>)>),
}

/// Allow PPParseResult to be built from CppToken
impl<Sym: InternedRep> From<CppToken<Sym>> for PPLexOutput<Sym> {
    fn from(tk: CppToken<Sym>) -> Self {
        PPLexOutput::Token(tk)
    }
}

/// Allow PPParseResult to be built from PPDirective
impl<Sym: InternedRep> From<PPDirective<Sym>> for PPLexOutput<Sym> {
    fn from(dir: PPDirective<Sym>) -> Self {
        PPLexOutput::Directive(dir)
    }
}

/// The preprocessor is potentially expecting a preprocessing directive at this
/// state of the lexer, check for those as well
pub fn lex_maybe_pp_directive<IP: InternPolicy>(
    iter: &mut CppCharIt<'_>,
    token: CppToken<IP::Symbol>,
    buffer: &str,
    intern_ctx: &mut IP::Ctx,
) -> PPResult<IP::Symbol> {
    match token.kind {
        // If we have a hash after a new line, this may be a control line,
        // if section, or a new group part
        TokenKind::Hash => lex_hash_pp_directive::<IP>(iter, intern_ctx, buffer)
            .map(PPLexOutput::Directive),

        // check if this is the start of a non # pp directive
        TokenKind::Atom(_) => {
            lex_maybe_atom_ppdirective::<IP>(iter, token, buffer, intern_ctx)
        }

        // continue normally, this isn't a directive
        _ => Ok(PPLexOutput::Token(token)),
    }
}

/// A directive inducing token (#) was seen, proceed to parse a preprocessor directive
pub fn lex_hash_pp_directive<IP: InternPolicy>(
    iter: &mut CppCharIt<'_>,
    intern_ctx: &mut IP::Ctx,
    buffer: &str,
) -> PPDirectiveResult<IP::Symbol> {
    // get the directive token
    let directive_token = skip_whitespace::<IP>(iter, intern_ctx)?;

    // read the next token, it must be an atom
    let directive_token = match directive_token {
        tk @ CppToken { kind: TokenKind::Atom(_), .. } => tk,
        tk if tk.kind == TokenKind::NewLine => return Ok(PPDirective::Null),
        tk => {
            return Err(PPError::InvalidPreprocessingDirective {
                invalid_token_idx: cppcharit_to_physical_idx(iter)
                    .map(|v| v - tk.size)
                    .unwrap_or_else(|| buffer.len()),
            })
        }
    };

    // get the buffer for the token
    let last_idx = cppcharit_to_physical_idx(iter).unwrap_or(buffer.len());
    let directive_idx = last_idx - directive_token.size;
    let buf_len = buffer.len();

    match &buffer[directive_idx..last_idx] {
        it if cmp_atom(PP_DIRECTIVE_DEFINE, it) => {
            lex_define::<IP>(iter, buffer, directive_idx, buf_len, intern_ctx)
        }
        it if cmp_atom(PP_DIRECTIVE_ERROR, it) => {
            lex_error::<IP>(iter, directive_idx, buf_len, intern_ctx)
        }
        it if cmp_atom(PP_DIRECTIVE_UNDEF, it) => {
            lex_undef::<IP>(iter, directive_idx, buf_len, intern_ctx)
        }
        it if cmp_atom(PP_DIRECTIVE_ELIF, it) => {
            lex_elif::<IP>(iter, directive_idx, buf_len, intern_ctx)
        }
        it if cmp_atom(PP_DIRECTIVE_IFDEF, it) => {
            lex_ifdef::<IP>(iter, directive_idx, buf_len, intern_ctx)
        }
        it if cmp_atom(PP_DIRECTIVE_IF, it) => {
            lex_if::<IP>(iter, directive_idx, buf_len, intern_ctx)
        }
        it if cmp_atom(PP_DIRECTIVE_INCLUDE, it) => lex_include::<IP>(iter, buf_len, intern_ctx),
        it if cmp_atom(PP_DIRECTIVE_ELSE, it) => {
            lex_else::<IP>(iter, directive_idx, buf_len, intern_ctx)
        }
        it if cmp_atom(PP_DIRECTIVE_LINE, it) => lex_line::<IP>(iter, buf_len, intern_ctx),
        it if cmp_atom(PP_DIRECTIVE_ENDIF, it) => {
            lex_endif::<IP>(iter, directive_idx, buf_len, intern_ctx)
        }
        it if cmp_atom(PP_DIRECTIVE_IFNDEF, it) => {
            lex_ifndef::<IP>(iter, directive_idx, buf_len, intern_ctx)
        }
        it if cmp_atom(PP_DIRECTIVE_PRAGMA, it) => lex_pragma::<IP>(iter, buf_len, intern_ctx),

        _ => Err(PPError::InvalidPreprocessingDirective { invalid_token_idx: directive_idx }),
    }
}

/// Reads tokens that make up a conditional preprocessor condition and returns the token sequence
/// making up the conditional expression, Preprocessor results are returned if encountered
fn lex_conditional_expr<IP: InternPolicy>(
    iter: &mut CppCharIt,
    intern_ctx: &mut IP::Ctx,
    if_idx: usize,
    buf_len: usize,
) -> Result<Vec<CppToken<IP::Symbol>>, PPError> {
    let mut condition_tokens: Vec<CppToken<IP::Symbol>> = vec![];
    loop {
        match basic_lexer::lex::<IP>(iter, intern_ctx) {
            Ok(tk) if tk.kind == TokenKind::NewLine => break,

            // skip whitespace tokens
            Ok(tk) if is_pp_whitespace_token(&tk) => continue,

            // we saw a token, add it to the conditional tokens
            Ok(tk) => condition_tokens.push(tk),

            // encountered end of file, this is an error as an #if block must be
            // matched by an #endif block
            Err(ILError::EOF) => {
                // did we get any tokens for the expression?
                let last_parsed_idx = cppcharit_to_physical_idx(iter).unwrap_or(buf_len - 1);
                return match condition_tokens.len() {
                    0 => Err(PPError::ExpectedConditionalExpression { if_idx, last_parsed_idx }),
                    _ => Err(PPError::UnterminatedConditionalBlock { if_idx, last_parsed_idx }),
                };
            }

            // Encountered some other error
            Err(e) => return Err(PPError::from(e)),
        }
    }

    Ok(condition_tokens)
}

/// Helper function to parse #if conditional directives
fn lex_if<IP: InternPolicy>(
    iter: &mut CppCharIt,
    if_idx: usize,
    buf_len: usize,
    intern_ctx: &mut IP::Ctx,
) -> PPDirectiveResult<IP::Symbol> {
    lex_conditional_expr::<IP>(iter, intern_ctx, if_idx, buf_len)
        .map(|tokens| PPDirective::If(IfLine::Expression(tokens)))
}

/// Helper function to parse #import directives
pub fn lex_elif<IP: InternPolicy>(
    iter: &mut CppCharIt,
    if_idx: usize,
    buf_len: usize,
    intern_ctx: &mut IP::Ctx,
) -> PPDirectiveResult<IP::Symbol> {
    lex_conditional_expr::<IP>(iter, intern_ctx, if_idx, buf_len).map(PPDirective::Elif)
}

/// Helper function to parse #ifdef conditional directives
fn lex_ifdef<IP: InternPolicy>(
    iter: &mut CppCharIt,
    directive_idx: usize,
    buf_len: usize,
    intern_ctx: &mut IP::Ctx,
) -> PPDirectiveResult<IP::Symbol> {
    #[allow(clippy::type_complexity)]
    let (atom_tokens, non_atom_tokens): (
        Vec<(usize, CppToken<IP::Symbol>)>,
        Vec<(usize, CppToken<IP::Symbol>)>,
    ) = tokenize_till::<IP>(TokenKind::NewLine, iter, buf_len, intern_ctx)
        .map_err(PPError::from)?
        .iter()
        .partition(|(_, s)| is_identifier_token(s));

    // count the non whitespace non atom tokens we have
    let non_whitespace_tokens_count =
        non_atom_tokens.iter().filter(|(_, s)| !is_pp_whitespace_token(s)).count();

    // we may only have one identifier token (possibly separated by spaces)
    match (atom_tokens.len(), non_whitespace_tokens_count) {
        // happy path
        (1, 0) => Ok(PPDirective::If(IfLine::Defined(atom_tokens.first().unwrap().1))),

        // Nothing was provided
        (0, 0) => Err(PPError::MissingIfDefIdentifierAt(directive_idx)),

        // Something else was given, find the first bad token and complain
        _ => {
            let atom_idx = atom_tokens.first().map(|(v, _)| *v);

            let non_atom_idx = non_atom_tokens
                .iter()
                .filter(|(_, s)| !is_pp_whitespace_token(s))
                .map(|(v, _)| *v)
                .next();

            match (atom_idx, non_atom_idx) {
                // the atom must be first in the declaration
                (Some(a), Some(b)) if b < a => {
                    Err(PPError::ExpectedIfDefIdentifierAt { directive_idx, bad_token_idx: b })
                }

                // there were not atoms
                (None, Some(b)) => {
                    Err(PPError::ExpectedIfDefIdentifierAt { directive_idx, bad_token_idx: b })
                }

                // atom was first but there were extra tokens
                (Some(a), Some(b)) if b > a => Err(PPError::ExtraIfDefTokens {
                    directive_idx,
                    bad_token_idx: std::cmp::min(
                        b,
                        atom_tokens.get(1).map(|(i, _)| *i).unwrap_or(usize::max_value()),
                    ),
                }),

                // atom was first but there were extra tokens
                (Some(_), None) => Err(PPError::ExtraIfDefTokens {
                    directive_idx,
                    bad_token_idx: atom_tokens
                        .get(1)
                        .map(|(i, _)| *i)
                        .unwrap_or(usize::max_value()),
                }),

                _ => unreachable!(),
            }
        }
    }
}

/// helper function to parse #define macro directives
pub fn lex_define<IP: InternPolicy>(
    iter: &mut CppCharIt<'_>,
    buffer: &str,
    def_idx: usize,
    buf_len: usize,
    intern_ctx: &mut IP::Ctx,
) -> PPDirectiveResult<IP::Symbol> {
    // get the directive token
    let ident_token = match skip_whitespace::<IP>(iter, intern_ctx) {
        Ok(tk @ CppToken { kind: TokenKind::Atom(_), .. }) => Ok(tk),
        Ok(tk @ CppToken { kind: TokenKind::Unknown(_), .. }) => Ok(tk),

        Ok(tk) => Err(PPError::ExpectedDefineIdentifier {
            def_idx,
            bad_token_idx: cppcharit_to_physical_idx(iter).unwrap_or(buf_len) - tk.size,
        }),
        Err(ILError::EOF) => Err(PPError::ExpectedDefineIdentifier {
            def_idx,
            bad_token_idx: cppcharit_to_physical_idx(iter).unwrap_or(buf_len - 1),
        }),
        Err(e) => Err(PPError::from(e)),
    }?;

    let ident = (cppcharit_to_logical_idx(iter).unwrap_or(buf_len) - ident_token.size, ident_token);

    // if we got an open parenthesis token right away, this is a function macro
    match basic_lexer::lex::<IP>(iter, intern_ctx) {
        Ok(CppToken { kind: TokenKind::OpenParen, .. }) => {
            lex_fn_macro::<IP>(def_idx, ident, iter, buffer, buf_len, intern_ctx)
        }

        // empty object macros are ok
        Ok(CppToken { kind: TokenKind::NewLine, .. }) => {
            Ok(PPDirective::DefineMacro(DefineDirectiveMacro::ObjectLike {
                macro_name: ident,
                replacement_list: vec![],
            }))
        }

        // there must be whitespace between the identifier and the replacement list of an object macro
        Ok(tk) if is_pp_whitespace_token(&tk) => {
            lex_obj_macro::<IP>(ident, iter, buffer, buf_len, intern_ctx)
        }

        // raise an error if there was no whitespace between token and replacement list
        _ => Err(PPError::MissingObjectMacroIdentWhitespace {
            def_idx,
            missing_whitespace_idx: cppcharit_to_logical_idx(iter).unwrap_or(buf_len),
        }),
    }
}

/// Read the input token iterator until reaching the a token whose kind meets the limit_kind,
/// skipping the whitespace characters in between.
#[allow(clippy::type_complexity)]
fn tokenize_nonwhitespace_till<IP: InternPolicy>(
    limit_kind: TokenKind<IP::Symbol>,
    iter: &mut CppCharIt,
    buf_len: usize,
    intern_ctx: &mut IP::Ctx,
) -> Result<Vec<(usize, CppToken<IP::Symbol>)>, PPError> {
    let res = tokenize_till::<IP>(limit_kind, iter, buf_len, intern_ctx)
        .map_err(PPError::from)?
        .iter()
        .filter(|(_, s)| !is_pp_whitespace_token(s))
        .copied()
        .collect::<Vec<(usize, CppToken<IP::Symbol>)>>();
    Ok(res)
}

// helper function to make sure a set of tokens is separated by commas
fn is_valid_sep<Sym: InternedRep>(
    def_idx: usize,
    identifier_list: &[(usize, CppToken<Sym>)],
) -> Result<(), PPError> {
    let mut validation_iter = identifier_list.iter().enumerate();
    let ident_token_kind = |tk| matches!(tk, TokenKind::Atom(_) | TokenKind::Unknown(_));

    while let Some((_, &(buf_idx, ident_token))) = validation_iter.next() {
        let next_tk = validation_iter.next();

        match (ident_token.kind, next_tk.map(|(_, &tk)| tk.1.kind)) {
            // happy paths
            (TokenKind::Atom(_), Some(TokenKind::Comma)) => (),
            (TokenKind::Unknown(_), Some(TokenKind::Comma)) => (),
            (TokenKind::Atom(_), None) => (),
            (TokenKind::Unknown(_), None) => (),
            (TokenKind::Ellipsis, None) => (),

            // issue errors
            (TokenKind::Ellipsis, Some(_)) => {
                return Err(PPError::NonTerminalFunctionMacroEllipsis {
                    def_idx,
                    ellipsis_idx: buf_idx,
                })
            }

            (a, Some(b)) if ident_token_kind(a) && ident_token_kind(b) => {
                return Err(PPError::ExpectedCommaFunctionMacro {
                    def_idx,
                    first_ident_idx: buf_idx,
                    second_ident_idx: next_tk.unwrap().1.0,
                })
            }

            // if we are leading with a comma we encountered an issue
            (TokenKind::Comma, _) => {
                return Err(PPError::ExpectedIdentifierFunctionMacro {
                    def_idx,
                    index_expected: buf_idx,
                })
            }

            // found unexpected tokens here
            _ => {
                return Err(PPError::UnexpectedTokenFunctionMacro {
                    def_idx,
                    unexpected_token_idx: buf_idx,
                })
            }
        }
    }

    // Make sure the last token is not a comma
    match identifier_list.last() {
        Some(&(index_expected, tk)) if tk.kind == TokenKind::Comma => {
            Err(PPError::ExpectedIdentifierFunctionMacro { def_idx, index_expected })
        }
        _ => Ok(()),
    }
}

// helper function to finish parsing a function macro
fn lex_fn_macro<IP: InternPolicy>(
    def_idx: usize,
    ident: (usize, CppToken<IP::Symbol>),
    iter: &mut CppCharIt<'_>,
    buffer: &str,
    buf_len: usize,
    intern_ctx: &mut IP::Ctx,
) -> PPDirectiveResult<IP::Symbol> {
    // find the parameters
    let identifier_list: Vec<(usize, CppToken<IP::Symbol>)> =
        tokenize_nonwhitespace_till::<IP>(TokenKind::CloseParen, iter, buf_len, intern_ctx)?;

    is_valid_sep(def_idx, &identifier_list)?;

    let is_variadic_macro =
        identifier_list.last().map(|(_, tk)| tk.kind == TokenKind::Ellipsis).unwrap_or(false);

    let replacement_list = tokenize_till::<IP>(TokenKind::NewLine, iter, buf_len, intern_ctx)?;

    // Check if __VA_OPT__ is in a non-variadic macro, if so, make sure __VA_OPT__ and __VA_ARG__
    // arent there
    if !is_variadic_macro {
        // Given a token and an index, find if its __VA_OPT__ or __VA_ARG__
        for &(idx, tk) in replacement_list.iter() {
            match categorize_ident_token(&buffer[idx..], &tk) {
                PPTokenCategory::VaOptKeyword => {
                    return Err(PPError::NonVariadicMacroWithVaOpt {
                        def_idx,
                        ident_idx: ident.0,
                        va_opt_idx: idx,
                    })
                }
                PPTokenCategory::VaArgsKeyword => {
                    return Err(PPError::NonVariadicMacroWithVaArgs {
                        def_idx,
                        ident_idx: ident.0,
                        va_arg_idx: idx,
                    })
                }
                _ => {}
            }
        }
    } else {
        // find all instances of va_opt in the replacement list
        let va_opt_iter = replacement_list.iter().enumerate().filter(|(_, (idx, tk))| {
            categorize_ident_token(&buffer[*idx..], tk) == PPTokenCategory::VaOptKeyword
        });

        // all __VA_OPT__ instances must be part of the __VA_OPT__() token sequence, ensure all
        // __VA_OPT__ instances are followed by an open paren
        for (list_idx, &(va_opt_idx, _)) in va_opt_iter.clone() {
            // make sure its followed by a '('
            let token_after = replacement_list
                .iter()
                .skip(list_idx + 1)  // skip the VA_OPT
                .filter(|(idx, v)| categorize_ident_token(&buffer[*idx..],v) != PPTokenCategory::WhitespaceChar)
                .map(|(idx, v)| (idx, v)).next();

            match token_after {
                Some((&invalid_token_idx, tk)) if tk.kind != TokenKind::OpenParen => {
                    return Err(PPError::VaOptExpectedOpenParen {
                        def_idx,
                        ident_idx: ident.0,
                        va_opt_idx,
                        invalid_token_idx,
                    })
                }
                None => {
                    return Err(PPError::VaOptExpectedOpenParen {
                        def_idx,
                        ident_idx: ident.0,
                        va_opt_idx,
                        invalid_token_idx: replacement_list
                            .last()
                            .map(|(idx, tk)| *idx + tk.size)
                            .unwrap_or(buf_len),
                    })
                }
                _ => {}
            }
        }

        // All __VA_OPT__() pp token sequences must be followed by a balanced set of parenthesis,
        // also they may not nest
        for (list_idx, &(va_opt_idx, _)) in va_opt_iter.clone() {
            // convert tokens to numeric value to keep track of balance
            let token_to_val = |(list_idx, token): (usize, &CppToken<IP::Symbol>)| match token.kind
            {
                TokenKind::OpenParen => Some((list_idx, 1)),
                TokenKind::CloseParen => Some((list_idx, -1)),
                _ => Some((list_idx, 0)),
            };

            // sums two ints and returns val if both are positive, to keep the balancing going
            let balanced_add =
                |(balance_count, _), (list_idx, token_val)| match balance_count + token_val {
                    new_balance if new_balance > 0 => Some((new_balance, list_idx)),
                    new_balance if new_balance == 0 => None,
                    _ => unreachable!(), // reaching negative numbers is impossible in this case, as
                                         // the loop would short circuit before hitting negative
                                         // numbers
                };

            // make sure its followed by a a balanced set of parenthesis
            let mut balancing_res_iter = replacement_list
                .iter()
                .enumerate()
                .skip(list_idx + 1)  // skip the VA_OPT token
                .skip_while(|(_, (idx, v))| categorize_ident_token(&buffer[*idx..], v) == PPTokenCategory::WhitespaceChar)
                .map(|(list_idx, (_, tk))| (list_idx, tk))
                .filter_map(token_to_val)
                .skip(1); // skip the first one and keep taking until we are at 0 again

            match balancing_res_iter.try_fold((1i128, 0usize), balanced_add) {
                // couldn't find a good closing of parenthesis
                Some((balance, _)) if balance > 0i128 => {
                    return Err(PPError::UnterminatedVaOptTokenSequence {
                        def_idx,
                        ident_idx: ident.0,
                        va_opt_idx,
                    });
                }
                // this is the good case, since the try_fold will never hit 0 because we break before,
                // it short circuits so the last element it ate was the close paren, we can use the
                // sub-range of repl_list[__VA_OPT__'s list idx..next token in iter] to check for
                // nesting __VA_OPT's__
                None => {}

                // Impossible to reach because try_fold breaks when it balance hits 0 so no more
                // additions then (and it starts with 1).
                _ => unreachable!(),
            }

            // we know list+idx + 1 is valid because otherwise __VA_OPT__ wasn't followed by (, which was
            // checked above
            let last_list_idx =
                balancing_res_iter.next().map(|(idx, _tk)| idx).unwrap_or(replacement_list.len());
            let nested_va_opt =
                replacement_list[list_idx + 1..last_list_idx].iter().find(|&(idx, tk)| {
                    categorize_ident_token(&buffer[*idx..], tk) == PPTokenCategory::VaOptKeyword
                });
            if let Some(&(nested_va_idx, _)) = nested_va_opt {
                return Err(PPError::NestedVaOptTokens {
                    def_idx,
                    ident_idx: ident.0,
                    va_opt_idx,
                    nested_va_opt_idx: nested_va_idx,
                });
            }
        }
    }

    // make sure all the elements are uniq in the repl list
    {
        let mut param_to_idx: HashMap<String, usize> = HashMap::new();
        for &(ident_idx, ident) in identifier_list.iter() {
            let token_buf = resolve_cpp_str(&buffer[ident_idx..ident_idx + ident.size]);

            if param_to_idx.contains_key(&token_buf) {
                return Err(PPError::DuplicateParameterInFunctionMacro {
                    def_idx,
                    first_idx: param_to_idx[&token_buf],
                    second_idx: ident_idx,
                    second_ident_size: ident.size,
                });
            }
            param_to_idx.insert(token_buf, ident_idx);
        }
    }

    let params = identifier_list
        .into_iter()
        .filter(|(_, s)| !is_pp_whitespace_token(s))
        .filter(|(_, s)| s.kind != TokenKind::Comma)
        .collect::<Vec<_>>();

    if is_variadic_macro {
        Ok(PPDirective::DefineMacro(DefineDirectiveMacro::VariadicFunctionLike {
            macro_name: ident,
            params,
            replacement_list,
        }))
    } else {
        Ok(PPDirective::DefineMacro(DefineDirectiveMacro::FunctionLike {
            macro_name: ident,
            params,
            replacement_list,
        }))
    }
}

// Helper function parse object like macro
fn lex_obj_macro<IP: InternPolicy>(
    macro_name: (usize, CppToken<IP::Symbol>),
    iter: &mut CppCharIt<'_>,
    _buffer: &str,
    buf_len: usize,
    intern_ctx: &mut IP::Ctx,
) -> PPDirectiveResult<IP::Symbol> {
    // find the parameters
    let replacement_list: Vec<(usize, CppToken<IP::Symbol>)> =
        tokenize_till::<IP>(TokenKind::NewLine, iter, buf_len, intern_ctx)
            .map_err(PPError::from)?
            .into_iter()
            .collect::<Vec<(usize, CppToken<IP::Symbol>)>>();

    // finish the wrap up
    Ok(PPDirective::DefineMacro(DefineDirectiveMacro::ObjectLike { macro_name, replacement_list }))
}

/// Helper function to parse #error conditional directives
fn lex_error<IP: InternPolicy>(
    iter: &mut CppCharIt,
    directive_idx: usize,
    buf_len: usize,
    intern_ctx: &mut IP::Ctx,
) -> PPDirectiveResult<IP::Symbol> {
    Ok(PPDirective::Error {
        error_idx: directive_idx,
        tokens: tokenize_till::<IP>(TokenKind::NewLine, iter, buf_len, intern_ctx)
            .map_err(PPError::from)?.to_vec(),
    })
}

/// Helper function to parse #ifndef conditional directives
pub fn lex_ifndef<IP: InternPolicy>(
    iter: &mut CppCharIt,
    directive_idx: usize,
    buf_len: usize,
    intern_ctx: &mut IP::Ctx,
) -> PPDirectiveResult<IP::Symbol> {
    match lex_ifdef::<IP>(iter, directive_idx, buf_len, intern_ctx) {
        Ok(PPDirective::If(IfLine::Defined(tk))) => Ok(PPDirective::If(IfLine::NotDefined(tk))),
        Err(e) => Err(e),
        _ => unreachable!(),
    }
}

/// Helper function to parse #else conditional directives
pub fn lex_else<IP: InternPolicy>(
    iter: &mut CppCharIt,
    else_idx: usize,
    buf_len: usize,
    intern_ctx: &mut IP::Ctx,
) -> PPDirectiveResult<IP::Symbol> {
    // make sure there are no tokens until a new line
    let atom_tokens: Vec<(usize, CppToken<IP::Symbol>)> =
        tokenize_nonwhitespace_till::<IP>(TokenKind::NewLine, iter, buf_len, intern_ctx)?;
    match atom_tokens.len() {
        0 => Ok(PPDirective::Else),
        _ => {
            Err(PPError::ExtraElseTokens { else_idx, bad_token_idx: atom_tokens.first().unwrap().0 })
        }
    }
}

/// Helper function to parse #endif conditional directives
pub fn lex_endif<IP: InternPolicy>(
    iter: &mut CppCharIt,
    end_idx: usize,
    buf_len: usize,
    intern_ctx: &mut IP::Ctx,
) -> PPDirectiveResult<IP::Symbol> {
    let atom_tokens: Vec<(usize, CppToken<IP::Symbol>)> =
        tokenize_nonwhitespace_till::<IP>(TokenKind::NewLine, iter, buf_len, intern_ctx)?;
    match atom_tokens.len() {
        0 => Ok(PPDirective::EndIf),
        _ => {
            Err(PPError::ExtraEndIfTokens { end_idx, bad_token_idx: atom_tokens.first().unwrap().0 })
        }
    }
}

/// Helper function to parse #undef directives
pub fn lex_undef<IP: InternPolicy>(
    iter: &mut CppCharIt,
    undef_idx: usize,
    buf_len: usize,
    intern_ctx: &mut IP::Ctx,
) -> PPDirectiveResult<IP::Symbol> {
    // read the next token, it must be an atom
    let ident_token = match skip_whitespace::<IP>(iter, intern_ctx) {
        // acceptable tokens
        Ok(tk) if matches!(tk.kind, TokenKind::Atom(_)) => Ok(tk),
        Ok(tk @ CppToken { kind: TokenKind::Unknown(_), .. }) => Ok(tk),

        // error handling

        // missing an identifier
        Ok(tk) if tk.kind == TokenKind::NewLine => {
            return Err(PPError::MissingUndefDirectiveName {
                undef_idx,
                line_end: cppcharit_to_physical_idx(iter).unwrap_or(buf_len - 1),
            })
        }

        // found some other token
        Ok(tk) => Err(PPError::UndefExpectedIdentifierToken {
            undef_idx,
            invalid_idx: cppcharit_to_logical_idx(iter).unwrap_or(buf_len) - tk.size,
        }),

        Err(e) => Err(PPError::from(e)),
    }?;

    // if we got here then make sure there aren't any more tokens
    let tokens =
        tokenize_nonwhitespace_till::<IP>(TokenKind::NewLine, iter, buf_len, intern_ctx)?;

    match tokens.len() {
        0 => (),
        _ => {
            return Err(PPError::UndefExtraTokens {
                undef_idx,
                extra_token_idx: tokens.first().unwrap().0,
            })
        }
    };

    Ok(PPDirective::UndefineMacro(ident_token))
}

/// Helper function to parse #line macro directives
pub fn lex_line<IP: InternPolicy>(
    iter: &mut CppCharIt,
    buf_len: usize,
    intern_ctx: &mut IP::Ctx,
) -> PPDirectiveResult<IP::Symbol> {
    tokenize_till::<IP>(TokenKind::NewLine, iter, buf_len, intern_ctx)
        .map(PPDirective::Line)
        .map_err(PPError::from)
}

/// Helper function to parse #pragma directives
pub fn lex_pragma<IP: InternPolicy>(
    iter: &mut CppCharIt,
    buf_len: usize,
    intern_ctx: &mut IP::Ctx,
) -> PPDirectiveResult<IP::Symbol> {
    tokenize_till::<IP>(TokenKind::NewLine, iter, buf_len, intern_ctx)
        .map(PPDirective::Pragma)
        .map_err(PPError::from)
}

/// Helper function to parse #include directives
pub fn lex_include<IP: InternPolicy>(
    iter: &mut CppCharIt,
    buf_len: usize,
    intern_ctx: &mut IP::Ctx,
) -> PPDirectiveResult<IP::Symbol> {
    tokenize_till::<IP>(TokenKind::NewLine, iter, buf_len, intern_ctx)
        .map(PPDirective::Include)
        .map_err(PPError::from)
}

///. Helper function to parse import directive
fn lex_import<IP: InternPolicy>(
    iter: &mut CppCharIt,
    is_export: bool,
    directive_idx: usize,
    buf_len: usize,
    intern_ctx: &mut IP::Ctx,
) -> PPDirectiveResult<IP::Symbol> {
    // read tokens till semicolon
    let tokens = tokenize_till::<IP>(TokenKind::Semi, iter, buf_len, intern_ctx)
        .map_err(PPError::from)?;

    // now that the semi colon was matched, time to
    let bad_tokens =
        tokenize_nonwhitespace_till::<IP>(TokenKind::NewLine, iter, buf_len, intern_ctx)?;

    match bad_tokens.len() {
        0 => (),
        _ => {
            return Err(PPError::ImportDirectiveExpectedNewline {
                directive_idx,
                bad_token_idx: bad_tokens.first().unwrap().0,
            })
        }
    };

    Ok(PPDirective::Import { directive_idx, is_export, tokens })
}

///. Helper function to parse import directive
pub fn lex_module<IP: InternPolicy>(
    iter: &mut CppCharIt,
    is_export: bool,
    module_idx: usize,
    buf_len: usize,
    intern_ctx: &mut IP::Ctx,
) -> PPDirectiveResult<IP::Symbol> {
    #[allow(clippy::type_complexity)]
    let tokens: Vec<(usize, CppToken<IP::Symbol>)> =
        tokenize_till::<IP>(TokenKind::Semi, iter, buf_len, intern_ctx)
            .map_err(PPError::from)?
            .iter()
            .filter(|(_, s)| !is_pp_whitespace_token(s))
            .copied()
            .collect::<Vec<_>>();

    // make sure the next token is new line
    // get the directive token
    let directive_token = skip_whitespace::<IP>(iter, intern_ctx)?;

    // read the next token, it must be an atom
    let bad_token_idx = cppcharit_to_logical_idx(iter).unwrap_or(buf_len);
    match directive_token {
        tk if tk.kind == TokenKind::NewLine => Ok(()),
        _ => Err(PPError::ModuleExpectedNewLine { module_idx, bad_token_idx }),
    }?;

    Ok(PPDirective::Module { is_export, tokens })
}

///. Helper function to parse import directive
pub fn lex_export<IP: InternPolicy>(
    iter: &mut CppCharIt<'_>,
    buffer: &str,
    export_idx: usize,
    intern_ctx: &mut IP::Ctx,
) -> PPDirectiveResult<IP::Symbol> {
    // the next token must be import or module
    let next_token = match skip_whitespace::<IP>(iter, intern_ctx) {
        Ok(tk) if matches!(tk.kind, TokenKind::Atom(_)) => Ok(tk),
        Ok(tk) => Err(PPError::ExportExpectedImportOrModule {
            export_idx,
            bad_token_idx: cppcharit_to_logical_idx(iter).unwrap_or(buffer.len())
                - tk.size,
        }),
        _ => Err(PPError::ExportExpectedImportOrModule {
            export_idx,
            bad_token_idx: cppcharit_to_logical_idx(iter).unwrap_or(buffer.len()),
        }),
    }?;

    let end_idx = cppcharit_to_logical_idx(iter).unwrap_or(buffer.len());
    let start_idx = end_idx - next_token.size;

    match &buffer[start_idx..end_idx] {
        it if cmp_atom(PP_DIRECTIVE_IMPORT, it) => {
            lex_import::<IP>(iter, true, start_idx, buffer.len(), intern_ctx)
        }
        it if cmp_atom(PP_DIRECTIVE_MODULE, it) => {
            lex_module::<IP>(iter, true, start_idx, buffer.len(), intern_ctx)
        }
        _ => Err(PPError::ExportExpectedImportOrModule {
            export_idx,
            bad_token_idx: cppcharit_to_logical_idx(iter).unwrap_or(buffer.len()),
        }),
    }
}

/// Helper function to parse directives that do not start with #
pub fn lex_maybe_atom_ppdirective<IP: InternPolicy>(
    iter: &mut CppCharIt<'_>,
    token: CppToken<IP::Symbol>,
    buffer: &str,
    intern_ctx: &mut IP::Ctx,
) -> PPResult<IP::Symbol> {
    let directive_idx =
        cppcharit_to_logical_idx(iter).unwrap_or(buffer.len()) - token.size;

    match &buffer[..token.size] {
        it if cmp_atom(PP_DIRECTIVE_IMPORT, it) => {
            lex_import::<IP>(iter, false, directive_idx, buffer.len(), intern_ctx)
                .map(PPLexOutput::Directive)
        }
        it if cmp_atom(PP_DIRECTIVE_MODULE, it) => {
            lex_module::<IP>(iter, false, directive_idx, buffer.len(), intern_ctx)
                .map(PPLexOutput::Directive)
        }
        it if cmp_atom(PP_DIRECTIVE_EXPORT, it) => {
            lex_export::<IP>(iter, buffer, directive_idx, intern_ctx)
                .map(PPLexOutput::Directive)
        }
        _ => Ok(PPLexOutput::Token(token)),
    }
}

/// consume an internal token and convert it to a preprocessor token, this is a lossy conversion as
/// it converts it to a standard preprocessor token (no interning), therefore it is not possible for
/// this function to work without being given a buffer, this specific build function is useful
/// in the function below and tests only
pub(crate) fn categorize_token<Sym: InternedRep>(token: &CppToken<Sym>) -> PPTokenCategory {
    use TokenKind as BTK;
    match token.kind {
        // Control tokens
        BTK::EndOfFile => PPTokenCategory::Control(ControlToken::EndOfFile),
        BTK::EndOfPPDirective => PPTokenCategory::Control(ControlToken::EndOfDirective),
        BTK::CodeCompletionMarker => PPTokenCategory::Control(ControlToken::EndOfDirective),
        // Comment tokens
        BTK::LineComment => PPTokenCategory::Comment,
        BTK::BlockComment => PPTokenCategory::Comment,

        // Note this represents more than a single space char and so it may splice to the next line,
        // this applies to most whitespace
        BTK::Space => PPTokenCategory::WhitespaceChar,
        BTK::Tab => PPTokenCategory::WhitespaceChar,
        BTK::VerticalTab => PPTokenCategory::WhitespaceChar,
        BTK::NewLine => PPTokenCategory::WhitespaceChar,
        BTK::FormFeed => PPTokenCategory::WhitespaceChar,
        BTK::CarriageReturn => PPTokenCategory::WhitespaceChar,

        // Tokens that dont fit anywhere
        BTK::EscapedChar(_) => PPTokenCategory::NonWhitespaceChar,
        BTK::Unknown(_) => PPTokenCategory::NonWhitespaceChar,

        // Preprocessing Operators, though they are only operators in function like macros
        BTK::Hash => PPTokenCategory::PPOperator,
        BTK::HashHash => PPTokenCategory::PPOperator,

        // Non identifier atoms can only be produced by knowing the context, that's done inside
        // the preprocessor directives lex_* functions
        BTK::Atom(_) => {
            print!("Oh no");
            unreachable!()
        }

        // Preprocessing Number tokens
        BTK::PPNumber => PPTokenCategory::PPNumber,

        // Literals
        BTK::Literal { .. } => PPTokenCategory::Literals,

        // Operators and Punctuators
        BTK::Amp => PPTokenCategory::OperatorOrPunctuator,
        BTK::AmpAmp => PPTokenCategory::OperatorOrPunctuator,
        BTK::AmpEq => PPTokenCategory::OperatorOrPunctuator,
        BTK::Arrow => PPTokenCategory::OperatorOrPunctuator,
        BTK::ArrowStar => PPTokenCategory::OperatorOrPunctuator,
        BTK::Caret => PPTokenCategory::OperatorOrPunctuator,
        BTK::CaretEq => PPTokenCategory::OperatorOrPunctuator,
        BTK::Colon => PPTokenCategory::OperatorOrPunctuator,
        BTK::ColonColon => PPTokenCategory::OperatorOrPunctuator,
        BTK::Comma => PPTokenCategory::OperatorOrPunctuator,
        BTK::Dot => PPTokenCategory::OperatorOrPunctuator,
        BTK::DotStar => PPTokenCategory::OperatorOrPunctuator,
        BTK::Ellipsis => PPTokenCategory::OperatorOrPunctuator,
        BTK::Equal => PPTokenCategory::OperatorOrPunctuator,
        BTK::EqualEqual => PPTokenCategory::OperatorOrPunctuator,
        BTK::Exclaim => PPTokenCategory::OperatorOrPunctuator,
        BTK::ExclaimEq => PPTokenCategory::OperatorOrPunctuator,
        BTK::Gt => PPTokenCategory::OperatorOrPunctuator,
        BTK::GtEq => PPTokenCategory::OperatorOrPunctuator,
        BTK::GtGt => PPTokenCategory::OperatorOrPunctuator,
        BTK::GtGtEq => PPTokenCategory::OperatorOrPunctuator,
        BTK::Lt => PPTokenCategory::OperatorOrPunctuator,
        BTK::LtEq => PPTokenCategory::OperatorOrPunctuator,
        BTK::LtLt => PPTokenCategory::OperatorOrPunctuator,
        BTK::LtLtEq => PPTokenCategory::OperatorOrPunctuator,
        BTK::Minus => PPTokenCategory::OperatorOrPunctuator,
        BTK::MinusEq => PPTokenCategory::OperatorOrPunctuator,
        BTK::MinusMinus => PPTokenCategory::OperatorOrPunctuator,
        BTK::Percent => PPTokenCategory::OperatorOrPunctuator,
        BTK::PercentEq => PPTokenCategory::OperatorOrPunctuator,
        BTK::Pipe => PPTokenCategory::OperatorOrPunctuator,
        BTK::PipeEq => PPTokenCategory::OperatorOrPunctuator,
        BTK::PipePipe => PPTokenCategory::OperatorOrPunctuator,
        BTK::Plus => PPTokenCategory::OperatorOrPunctuator,
        BTK::PlusEq => PPTokenCategory::OperatorOrPunctuator,
        BTK::PlusPlus => PPTokenCategory::OperatorOrPunctuator,
        BTK::Question => PPTokenCategory::OperatorOrPunctuator,
        BTK::Semi => PPTokenCategory::OperatorOrPunctuator,
        BTK::Slash => PPTokenCategory::OperatorOrPunctuator,
        BTK::SlashEq => PPTokenCategory::OperatorOrPunctuator,
        BTK::Spaceship => PPTokenCategory::OperatorOrPunctuator,
        BTK::Star => PPTokenCategory::OperatorOrPunctuator,
        BTK::StarEq => PPTokenCategory::OperatorOrPunctuator,
        BTK::Tilde => PPTokenCategory::OperatorOrPunctuator,
        BTK::OpenParen => PPTokenCategory::OperatorOrPunctuator,
        BTK::CloseParen => PPTokenCategory::OperatorOrPunctuator,
        BTK::OpenBrace => PPTokenCategory::OperatorOrPunctuator,
        BTK::CloseBrace => PPTokenCategory::OperatorOrPunctuator,
        BTK::OpenBracket => PPTokenCategory::OperatorOrPunctuator,
        BTK::CloseBracket => PPTokenCategory::OperatorOrPunctuator,
    }
}

/// Take a buffer, offset, and internal token and convert it to a preprocessor token, this function
/// converts the InternalToken to a non-interned PPToken.
pub fn categorize_ident_token<Sym: InternedRep>(
    buffer: &str,
    token: &CppToken<Sym>,
) -> PPTokenCategory {
    use TokenKind as BTK;
    match token.kind {
        BTK::Atom(_) if cmp_atom(VA_ARGS_IDENT, &buffer[..token.size]) => {
            PPTokenCategory::VaArgsKeyword
        }
        BTK::Atom(_) if cmp_atom(VA_OPT_IDENT, &buffer[..token.size]) => {
            PPTokenCategory::VaOptKeyword
        }
        BTK::Atom(_) => PPTokenCategory::Identifier,
        _ => categorize_token(token),
    }
}
