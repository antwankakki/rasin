use criterion::{criterion_group, criterion_main, Criterion};
use preprocessor::PreprocessorTokenIterator;

fn lex_file(buf: &str) {
    let mut pp = PreprocessorTokenIterator::<()>::new(buf);
    while !pp.is_eof() {
        let _ = pp.lex();
    }
}

pub fn preprocessor_lex_raw(c: &mut Criterion) {
    let input = include_str!("./res/simple_file.cpp");
    c.bench_function("preproc_lexraw__simple_file", |b| b.iter(|| lex_file(input)));

    let input = include_str!("./res/clang_10.cpp");
    c.bench_function("preproc_lexraw_clang_10", |b| b.iter(|| lex_file(input)));
}

criterion_group!(benches, preprocessor_lex_raw);
criterion_main!(benches);
