use basic_lexer::CppCharIt;
use criterion::{criterion_group, criterion_main, Criterion};

fn lex_file(buf: &str) {
    let mut cppcharit = CppCharIt::new(buf);
    while !cppcharit.as_str().is_empty() {
        let _ = basic_lexer::lex::<()>(&mut cppcharit, &mut ());
    }
}

pub fn basic_lexer_lex_file(c: &mut Criterion) {
    let input = include_str!("./res/simple_file.cpp");
    c.bench_function("lex_simple_file", |b| b.iter(|| lex_file(input)));

    let input = include_str!("./res/clang_10.cpp");
    c.bench_function("lex_clang_10", |b| b.iter(|| lex_file(input)));
}

criterion_group!(benches, basic_lexer_lex_file);
criterion_main!(benches);
