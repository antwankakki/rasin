A C++ compiler implementation based on the C++20 standard with a focus on simplicity and ease of use.


# Credits
* A lot of the design here came from studying the sources of the rust compiler as well as
clang. Thanks a lot to everyone who contributes to these!

* For documentation/standard compliance, cppreference and MSDN's documentation were really useful as well
